/** 
*	\file BGLIB_Module_Cfg.h
*	\brief [Description] - Configuration File.
*	\author Luca Bortolotti
*	\date   16-May-2017
*	\copyright Copyright (c) 2017 E-Novia Srl. All Rights Reserved.
* 
* E-NOVIA CONFIDENTIAL
* __________________
* 
* ALL INFORMATION CONTAINED HEREIN WAS PRODUCED BY E-NOVIA SRL AND NO THIRD PARTY MAY CLAIM ANY RIGHT OR PATERNITY ON IT.\n 
* THE INTELLECTUAL AND TECHNICAL CONCEPTS CONTAINED HEREIN ARE AND REMAIN
* THE PROPERTY OF E-NOVIA SRL AND ARE PROTECTED BY TRADE SECRET OR COPYRIGHT LAW.
* REPRODUCTION, DISTRIBUTION OR DISCLOSURE OF THIS MATERIAL TO ANY OTHER PARTY
* IS STRICTLY FORBIDDEN UNLESS PRIOR WRITTEN PERMISSION IS OBTAINED FROM E-NOVIA SRL
*/ 

#ifndef USE_OLD_BLE_IF

#ifndef __BGLIB_MODULE_CFG_H_
#define __BGLIB_MODULE_CFG_H_

//#include "os_api.h"
#include "main.h"
/** 
*	\defgroup BGLIB_Module_CONFIGURATION CONFIGURATIONS
*	\ingroup BGLIB_Module_MODULE
* 	\brief The BGLIB Module Module Configuration.
*   \{
*/

#define BGLIB_RAW_PACKET_PRINT_ENABLE	1																	/**< 1 to enable the raw packet ptint \warning Use only for deep debug purpose, it's called on interrupts */
#define BGLIB_WAKEUP_PIN_ENABLED		1																	/**< Set to 1 if WakeUp Pin is present */
#define BGLIB_RESET_PIN_ENABLED			1																	/**< Set to 1 if Reset Pin is present */
                                                                                                            /**< To initialize the uart with standard configuration*/


#define BGLIB_UART_INIT()               {MX_UART4_Init();\
                                        HAL_UART_Receive_IT(&huart4, &UART4_RxData, sizeof(uint8_t));}

#define BGLIB_UART_DEINIT()                                                                         /**< To initialize the uart with standard configuration*/

//#define BGLIB_TX(len, data)				(UART_Send(UART_ID_1, (uint8_t*)data, len, 1000U) == TRUE)	        /**< Modify to customize the UART transmisison method */

#define BGLIB_TX(len, data)             ((HAL_UART_Transmit(&huart4, (uint8_t*)data, len, 1000U) == HAL_OK))                                            


#define BGLIB_RX_TIMER_ID				/* OS_TIMER_ID_BGLIB_RX	*/	/* CC Capire che uso ne viene fatto */  /**< Set the Timer ID for BGLIB Rx operations*/
#define BGLIB_WAIT_RX_TIMEOUT			300																	/**< Set the timeout to Rx messages from module */


#if BGLIB_WAKEUP_PIN_ENABLED
#define BGLIB_WAKEUP_PORT				BLE_WAKE_GPIO_Port
#define BGLIB_WAKEUP_PIN				BLE_WAKE_Pin
#else
#warning "WakeUp pin Not enabled, use the right firmware to program the BLUEGIGA module"
#endif

#if BGLIB_RESET_PIN_ENABLED
#define BGLIB_RESET_PORT				UART4_RESET_GPIO_Port
#define BGLIB_RESET_PIN					UART4_RESET_Pin
#endif





#if BGLIB_WAKEUP_PIN_ENABLED
#define BGLIB_INIT_PIN_WAKEUP()               {HAL_GPIO_WritePin(BGLIB_WAKEUP_PORT, BGLIB_WAKEUP_PIN, GPIO_PIN_RESET);\
                                               GPIO_InitTypeDef GPIO_InitStruct;\
                                               GPIO_InitStruct.Pin   = BGLIB_WAKEUP_PIN;\
                                               GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;\
                                               GPIO_InitStruct.Pull  = GPIO_NOPULL;\
                                               GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;\
                                               HAL_GPIO_Init(BGLIB_WAKEUP_PORT, &GPIO_InitStruct);} 
#endif
                                               
#if BGLIB_RESET_PIN_ENABLED
#define BGLIB_INIT_PIN_RESET()                {HAL_GPIO_WritePin(BGLIB_RESET_PORT, BGLIB_RESET_PIN, GPIO_PIN_SET);\
                                               GPIO_InitTypeDef GPIO_InitStruct;\
                                               GPIO_InitStruct.Pin   = BGLIB_RESET_PIN;\
                                               GPIO_InitStruct.Mode  = GPIO_MODE_OUTPUT_PP;\
                                               GPIO_InitStruct.Pull  = GPIO_NOPULL;\
                                               GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;\
                                               HAL_GPIO_Init(BGLIB_RESET_PORT, &GPIO_InitStruct);} 
#endif
                                               

/**
*	\}
*/ 

#endif /*__BGLIB_MODULE_CFG_H_ */

#endif /*USE_OLD_BLE_IF*/

/* EOF */
