#ifndef __TAGMANAGER_H
#define __TAGMANAGER_H
#include <stdint.h>
#include <stdbool.h>
#include <error.h>

error_e InitTagManager();// set tag id 

uint16_t GetTagId();// get tag id 

#endif /* __TAGMANAGER_H */


