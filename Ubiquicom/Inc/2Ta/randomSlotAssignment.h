#ifndef __randomSlotAssignment_H
#define __randomSlotAssignment_H

#include "tagManager.h"

typedef struct randomSlotsAssignmentInit {        
    uint8_t minNumOfEmptySlotsBeetweenTwoPolls;     
    uint8_t numOfSlotsInSframe;     
    uint32_t slotDuration_ns;     
} randomSlotsAssignmentInit;

typedef struct randomSlotsResult {    
    uint32_t first_of_two_tau_ns;     
    uint32_t second_of_Two_tau_ns;     
    uint32_t single_tau_ns;     
} randomSlotsResult;

randomSlotsResult GetTwoRandomPollsTimes();
randomSlotsResult GetOtherRandomPollSlot(uint32_t fixedSlotTau_ns);


void InitRandomSlotAssignment(uint32_t seed);
#endif /* __randomSlotAssignment_H */


