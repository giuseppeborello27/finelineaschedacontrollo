#ifndef __2TA_H
#define __2TA_H

#include "tagManager.h"
#include "2taStateManager.h"
#include "frameConfiguration.h"


typedef struct pollsSlotCorrections {    
    bool isNextFirstPollIdentified;     
    bool isNextSecondPollIdentified;     
    int32_t firstPollCorrection_ns;     
    int32_t secondPollCorrection_ns;     
} pollsSlotCorrections;

typedef struct twota_waitFor_params {
    bool subtractElapsedTime;
    bool isAfterSecondCommunication;
    uint32_t tau_ns;     
} twota_waitFor_params;


int32_t GetNextIdentifiedPollCorrection (pollsSlotCorrections *slotCorrections);
targetsContainer Init2Ta(frameConfig *frameConfiguration);
void Twr_with_target(targetInfo *target);
pollsSlotCorrections Prepare_next_interval_communications(targetsContainer *targets);
void Twota_waitFor(bool isAfterSecond, int32_t delay);
uint32_t getCurrentTime_ticks();
int32_t getNextWaitingTime_ns(uint32_t startSuperFrame_ticks, uint32_t timeOffset);
int32_t getRemainingSuperFrameTime(uint32_t startSuperFrame_ticks);
bool ComputeIfWeShouldCorrectWaitAtTheEndOfSuperFrame(int32_t identifiedCorrection_ns, uint32_t startSuperFrame_ticks);
uint32_t shiftStartSuperframeTick(uint32_t startSuperFrame_ticks, int32_t timeToShift_ns);
void AdjustAnonymousPollCorrection(\
  pollsSlotCorrections *slotCorrections, targetInfo *nextFirstarget, targetInfo *nextSecondtarget);
  uint8_t updateNonReceptionTimeout(uint8_t ReceptionTimeout);
#endif /* __2TA_H */


