#ifndef __frameConfiguration_H
#define __frameConfiguration_H

#include <stdint.h>
#include <stdbool.h>
#include <error.h>

typedef struct frameConfig{
  uint32_t timeSuperFrame_ns;
  int32_t minTimeAllocatedForASlot_ns;
  int32_t slotDuration_ns;
  uint8_t numSlot;
  uint8_t minNumOfEmptySlotsBeetweenTwoSentPolls;
} frameConfig;

frameConfig *GetFrameConfiguration();

#endif /* __frameConfiguration_H */


