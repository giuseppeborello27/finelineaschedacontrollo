#ifndef __targetmanager_H
#define __targetmanager_H

#include <stdint.h>
#include <stdbool.h>
#include <error.h>

typedef enum receivedPacketStatus {  
  UNDEFINED = 0,
  // the followings are setted by 2ta
  RECEIVED_EXPECTED_RESPONSE_PACKET = 1,
  PACKET_FROM_NOT_EXPECTED_SOURCE_ID = 2, 

  // the followings are setted by DW1000
  RECEIVED_A_RESPONSE_FOR_THIS_TAG = 4, 
  UNKNOWN_PACKET = 8, // from DW1000
  RESPONSE_ADDRESSED_TO_ANOTHER_TAG_ID = 16, 
  ERROR_OR_TIMEOUT_IN_RECEPTION = 32 
} receivedPacketStatus;

typedef struct receivedPacketInfo {
  receivedPacketStatus status;
  uint16_t sourceId;
  uint16_t dist_cm;
  int32_t slotCorr_ns;
} receivedPacketInfo;

typedef struct targetInfo {
  uint16_t targetInfoId;
  uint16_t talkingSlotNumber; // 0, 1, ..., N
  uint32_t slot_tau_ns; // beginning of this communication in ns, respect to current superframe start time i.e. (talkingSlotNumber)*slotTime_ns
  struct receivedPacketInfo receivedPacket;
  uint16_t numberOfReceptionFailsFromThisSourceId;
  uint16_t numberOfAnonymousReceptionFailsInThisTalkingSlot;
  uint16_t numberOfIdentifiedPacketsSentToSameTarget;
  uint16_t lastValidIdentifiedDistance;
} targetInfo;

void WaitForFinishingComunication();

void SetCurrentTargetInfo(targetInfo *current);
void SetCurrentTargetResponseStatus(uint16_t dist_cm, uint16_t targetInfoId, receivedPacketStatus status);
void UpdateNumberOfFails(targetInfo *target);// current
void SetCurreSlotCorrection(int32_t slotCorr_ns);
void SignalFinishedSecondPollCommunication();

targetInfo *GetCurrentTargetInfo();
#endif /* __targetmanager_H */


