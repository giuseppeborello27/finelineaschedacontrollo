#ifndef __2taStateManager_H
#define __2taStateManager_H

#include "targetManager.h"

#define MAX_NUMBER_OF_IDENTIFIED_FAILS 5

#define MAX_NUMBER_OF_ANONYMOUS_FAILS 3

#define MIN_NUMBER_OF_IDENTIFIED_PACKETS_SENT_TO_SAME_TARGET 3

typedef struct targetsContainer {  
  struct targetInfo firstTargetInfo;
  struct targetInfo secondTargetInfo;  
} targetsContainer;

void UpdateTargetsForNextTrasmission(targetsContainer *targets);

#endif /* __2taStateManager_H */


