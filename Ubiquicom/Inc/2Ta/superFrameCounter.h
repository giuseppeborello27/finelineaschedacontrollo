#ifndef __superFrameCounter_H
#define __superFrameCounter_H
#include <stdint.h>
#include <stdbool.h>
#include <error.h>

uint8_t GetSuperFrameNumber();
void IncrementSuperFrameCounter();

#endif /* __superFrameCounter_H */


