/**
 * @project       Project
 *
 * @Component     Component
 *
 * @file          File
 *
 * @brief         Brief
 *
 * @author        Author 
 *
 * @date          Date
 *
 * @copyright     Copyright 2019 Kalpa All rights reserved.
 *
 *                This file is copyrighted and the property of Kalpa. It contains confidential and proprietary
 *                information and may not be distributed or reproduced in whole or in part without express written
 *                permission from Kalpa.
 *                Any copies of this file (in whole or in part) made by any method must also include a copy of this legend.
 *
 * Developed by:  Kalpa
 ***********************************************************************************************************************/
#include <string.h>

#include "appConfig.h"
//#include "externFlashPartition.h"
#include "config.h"


#define CFG_HEADER_FLASH_ADDRESS  0x00
#define CFG_SERIAL_NUMBER_ADDRESS 0x1000

#define CFG_SECTOR_BLOCK		 CFG_HEADER_FLASH_ADDRESS
#define CFG_SECTOR_SERIAL_BLOCK	 ( CFG_HEADER_FLASH_ADDRESS + 1 )


static ConfigParam_t configParam = { 0 };
static uint8_t serialNumber[20] = {0};//"UPA-201014B-P000001";

///*Static function */
//static void readConfigFromFlash(ConfigParam_t * cfg)
//{
//
//	static ConfigParamHeader_t cfgheader;
//
//	static uint8_t isFlashEmpty = 1;
//
//	EFLASH_Init();
//
//
//
///*
//	memcpy(serialNumber, "UPA-201014B-P000001", sizeof(serialNumber));
//	EFP_WriteData(EFP_SN_TYPE, (uint8_t *)serialNumber, sizeof(serialNumber), EFP_SN_DATA_START_ADDR);
//*/
//
//	if(EFP_ReadData(EFP_SN_TYPE, (uint8_t *)serialNumber, sizeof(serialNumber),EFP_SN_DATA_START_ADDR))
//	{
//		for( register uint32_t i = 0 ; i < sizeof(serialNumber) ; i++)
//		{
//			if(0xFF != serialNumber[i])
//			{
//				isFlashEmpty = 0;
//				break;
//			}
//		}
//		if ( isFlashEmpty )
//		{
//
//		/*	memcpy(serialNumber, "UPA-201014B-P000001", sizeof(serialNumber)); */
//			//EFLASH_EraseSector(CFG_SECTOR_SERIAL_BLOCK);
//
//			EFP_WriteData(EFP_SN_TYPE, (uint8_t *)serialNumber, sizeof(serialNumber), EFP_SN_DATA_START_ADDR);
//			//EFLASH_Write(CFG_SERIAL_NUMBER_ADDRESS, (uint8_t *)serialNumber, sizeof(serialNumber));
//
//			EFP_ReadData(EFP_SN_TYPE, (uint8_t *)serialNumber, sizeof(serialNumber),EFP_SN_DATA_START_ADDR);
//		}
//
//	}
//
//	isFlashEmpty = 1;
//	/* EFLASH_Read(CFG_HEADER_FLASH_ADDRESS, (uint8_t *)&cfgheader, sizeof(ConfigParamHeader_t))*/
//
//	if( EFP_ReadData(EFP_CONF_TYPE, (uint8_t *)&cfgheader, sizeof(ConfigParamHeader_t), EFP_CONF_DATA_START_ADDR) )
//	{
//		for( register uint32_t i = 0 ; i < sizeof(ConfigParamHeader_t) ; i++)
//		{
//			if(0xFFFF != cfgheader.version)
//			{
//				isFlashEmpty = 0;
//				break;
//			}
//		}
//		/* check if flash is empty and write default parameters. */
//		if( !isFlashEmpty && (cfgheader.version > 0 && cfgheader.version <= cfg->confHeader.version)
//				&& cfgheader.size != 0 && cfgheader.maxParam != 0)
//		{
//			EFP_ReadData(EFP_CONF_TYPE, (uint8_t *)&cfg->preAlarmThreshold, (cfgheader.size - sizeof(ConfigParamHeader_t)), (EFP_CONF_DATA_START_ADDR + sizeof(ConfigParamHeader_t)));
//			//EFLASH_Read((CFG_HEADER_FLASH_ADDRESS + sizeof(ConfigParamHeader_t)), (uint8_t *)&cfg->preAlarmThreshold, (cfgheader.size - sizeof(ConfigParamHeader_t)));
//		}
//	}
//
//}
//
//
//void writeConfigIntoFlash(ConfigParam_t * cfg)
//{
//    EFP_WriteData(EFP_CONF_TYPE, (uint8_t *)cfg, sizeof(ConfigParam_t), EFP_CONF_DATA_START_ADDR);
//}
//
///*Global function*/
//
//void configInit(void)
//{
//	uint8_t *dummy = 0;
//	restoreConfig();
//	writeConfigIntoFlash(getConfigParam());
//	for( uint8_t idx = 1; idx < MAX_ParamID; idx ++)
//	{
//		configManager(dummy, 0, idx , copy);
//	}
//}
//
//
//ConfigParamItem_u buildConfigItem(ConfigParamId_t id, ConfigParamType_e type, uint16_t size, uint8_t * data, uint32_t * buildSize)
//{
//   ConfigParamItem_u tmpItem = { 0 };
//
//   tmpItem.header.id = id;
//   tmpItem.header.type = type;
//
//   switch(type)
//   {
//      case typeU8:
//      {
//         tmpItem.header.size = sizeof(uint8_t);
//         tmpItem.typeU8.data = *(uint8_t *)data;
//         break;
//      }
//      case typeS8:
//      {
//         tmpItem.header.size = sizeof(int8_t);
//         tmpItem.typeS8.data = *(int8_t *)data;
//         break;
//      }
//      case typeU16:
//      {
//         tmpItem.header.size = sizeof(uint16_t);
//         tmpItem.typeU16.data = *(uint16_t *)data;
//         break;
//      }
//      case typeS16:
//      {
//         tmpItem.header.size = sizeof(int16_t);
//         tmpItem.typeS16.data = *(int16_t *)data;
//         break;
//      }
//      case typeU32:
//      {
//         tmpItem.header.size = sizeof(uint32_t);
//         tmpItem.typeU32.data = *(uint32_t *)data;
//         break;
//      }
//      case typeS32:
//      {
//         tmpItem.header.size = sizeof(int32_t);
//         tmpItem.typeS32.data = *(int32_t *)data;
//         break;
//      }
//      case typeString:
//      {
//         tmpItem.header.size = size;
//         memcpy(&tmpItem.rawData[sizeof(ConfigParamItemHeader_t)], data, size);
//         break;
//      }
//      default:
//         break;
//   }
//
//   *buildSize += (sizeof(ConfigParamItemHeader_t) + tmpItem.header.size);
//
//   return tmpItem;
//}
//
//void restoreConfig(void)
//{
//	restoreConfigDefault(getConfigParam());
//
//	readConfigFromFlash(getConfigParam());
//}
//
//
//
//
//void restoreConfigDefault(ConfigParam_t * cfg)
//{
//   uint16_t value = 0;
//
//   cfg->confHeader.version = CONFIG_PARAM_VERSION;
//   cfg->confHeader.size = sizeof(ConfigParamHeader_t) ;
//   cfg->confHeader.maxParam = (uint16_t)MAX_ParamID-1;
//
//   value = DEFAULT_PRE_ALRM_THR;
//   cfg->preAlarmThreshold = buildConfigItem(ParamPreAlarmThreshold, typeU16, 2, (uint8_t *)&value, &cfg->confHeader.size);
//
//   value = DEFAULT_ALRM_THR;
//   cfg->alarmThreshold = buildConfigItem(ParamAlarmThreshold, typeU16, 2, (uint8_t *)&value, &cfg->confHeader.size);
//
//   value = DEFAULT_PRE_ALRM_HYS;
//   cfg->preAlarmHysteresis = buildConfigItem(ParamPreAlarmHysteresis, typeU16, 2, (uint8_t *)&value, &cfg->confHeader.size);
//
//   value = DEFAULT_ALRM_HYS;
//   cfg->alarmHysteresis = buildConfigItem(ParamAlarmHysteresis, typeU16, 2, (uint8_t *)&value, &cfg->confHeader.size);
//
//   value = DEFAULT_TIMEOUT;
//   cfg->timeout = buildConfigItem(ParamTimeout, typeU16, 2, (uint8_t *)&value, &cfg->confHeader.size);
//
//   value = DEFAULT_PRE_ALRM_REL;
//   cfg->preAlarmReleActivation = buildConfigItem(ParamPreAlarmReleActivation, typeU16, 2, (uint8_t *)&value, &cfg->confHeader.size);
//
//   value = DEFAULT_ALRM_REL;
//   cfg->alarmReleActivation = buildConfigItem(ParamAlarmReleActivation, typeU16, 2, (uint8_t *)&value, &cfg->confHeader.size);
//
//   value = DEFAULT_AVG_WIN_SAMP;
//   cfg->averageWindowSamples = buildConfigItem(ParamAverageWindowSamples, typeU16, 2, (uint8_t *)&value, &cfg->confHeader.size);
//
//   value = DEFAULT_DRIVE_EXC_TIMEOUT;
//   cfg->driverExclusionTimeout = buildConfigItem(ParamDriverExclusionTimeout, typeU16, 2, (uint8_t *)&value, &cfg->confHeader.size);
//
//   value = DEFAULT_DETEC_FREQ;
//   cfg->detectionFrequency = buildConfigItem(ParamDetectionFrequency, typeU16, 2, (uint8_t *)&value, &cfg->confHeader.size);
//
//}
//
///* Get */
//
//void configManager(uint8_t * pBuffer, uint8_t size, ConfigParamId_t id, paramAction_e action)
//{
//	static uint8_t pCpyBuff[4] = {0};
//   if( ( NULL != pBuffer ) && ( sizeof(uint32_t) <= size ) )
//   {
//      switch(id)
//      {
//         case ParamPreAlarmThreshold:
//         {
//            if(write == action)
//            {
//               setPreAlarmThreshold( (uint16_t) ( ( ( pBuffer[0] << 8 ) & 0xFF00 ) | (  pBuffer[1] & 0xFF ) ) );
//            }
//            else if (read == action)
//            {
//               *(uint16_t *)pBuffer = getPreAlarmThreshold();
//            }
//            else
//            {
//            	*(uint16_t *)pCpyBuff = getPreAlarmThreshold();
//            	setPreAlarmThreshold( (uint16_t) ( ( ( pCpyBuff[0] << 8 ) & 0xFF00 ) | (  pCpyBuff[1] & 0xFF ) ) );
//            }
//
//            break;
//         }
//         case ParamAlarmThreshold:
//         {
//            if(write == action)
//            {
//               setAlarmThreshold( (uint16_t) ( ( ( pBuffer[0] << 8 ) & 0xFF00 ) | (  pBuffer[1] & 0xFF ) ) );
//            }
//            else if( read == action)
//            {
//               *(uint16_t *)pBuffer = getAlarmThreshold();
//            }
//            else
//			{
//				*(uint16_t *)pCpyBuff = getAlarmThreshold();
//				setAlarmThreshold( (uint16_t) ( ( ( pCpyBuff[0] << 8 ) & 0xFF00 ) | (  pCpyBuff[1] & 0xFF ) ) );
//			}
//
//            break;
//         }
//         case ParamPreAlarmHysteresis:
//         {
//            if(write == action)
//            {
//               setPreAlarmHysteresis( (uint16_t) ( ( ( pBuffer[0] << 8 ) & 0xFF00 ) | (  pBuffer[1] & 0xFF ) ) );
//            }
//            else if( read == action)
//            {
//               *(uint16_t *)pBuffer = getPreAlarmHysteresis();
//            }
//            else
//			{
//				*(uint16_t *)pCpyBuff = getPreAlarmHysteresis();
//				setPreAlarmHysteresis( (uint16_t) ( ( ( pCpyBuff[0] << 8 ) & 0xFF00 ) | (  pCpyBuff[1] & 0xFF ) ) );
//			}
//            break;
//         }
//         case ParamAlarmHysteresis:
//         {
//            if(write == action)
//            {
//               setAlarmHysteresis( (uint16_t) ( ( ( pBuffer[0] << 8 ) & 0xFF00 ) | (  pBuffer[1] & 0xFF ) ) );
//            }
//            else if( read == action)
//            {
//               *(uint16_t *)pBuffer = getAlarmHysteresis();
//            }
//            else
//			{
//				*(uint16_t *)pCpyBuff = getAlarmHysteresis();
//				setAlarmHysteresis( (uint16_t) ( ( ( pCpyBuff[0] << 8 ) & 0xFF00 ) | (  pCpyBuff[1] & 0xFF ) ) );
//			}
//            break;
//         }
//         case ParamTimeout:
//         {
//            if(write == action)
//            {
//               setTimeout( (uint16_t) ( ( ( pBuffer[0] << 8 ) & 0xFF00 ) | (  pBuffer[1] & 0xFF ) ) );
//            }
//            else if( read == action)
//            {
//               *(uint16_t *)pBuffer = getTimeout();
//            }
//            else
//			{
//				*(uint16_t *)pCpyBuff = getTimeout();
//				setTimeout( (uint16_t) ( ( ( pCpyBuff[0] << 8 ) & 0xFF00 ) | (  pCpyBuff[1] & 0xFF ) ) );
//			}
//            break;
//         }
//         case ParamPreAlarmReleActivation:
//         {
//            if(write == action)
//            {
//               setPreAlarmReleActivation( (bool) ( ( ( pBuffer[0] << 8 ) & 0xFF00 ) | (  pBuffer[1] & 0xFF ) ) );
//            }
//            else if( read == action)
//            {
//               *(uint16_t *)pBuffer = (uint16_t) getPreAlarmReleActivation();
//            }
//            else
//			{
//				*(uint16_t *)pCpyBuff = getPreAlarmReleActivation();
//				setPreAlarmReleActivation( (uint16_t) ( ( ( pCpyBuff[0] << 8 ) & 0xFF00 ) | (  pCpyBuff[1] & 0xFF ) ) );
//			}
//            break;
//         }
//         case ParamAlarmReleActivation:
//         {
//            if(write == action)
//            {
//               setAlarmReleActivation( (bool) ( ( ( pBuffer[0] << 8 ) & 0xFF00 ) | (  pBuffer[1] & 0xFF ) ) );
//            }
//            else if( read == action)
//            {
//               *(uint16_t *)pBuffer = (uint16_t) getAlarmReleActivation();
//            }
//            else
//			{
//				*(uint16_t *)pCpyBuff = getAlarmReleActivation();
//				setAlarmReleActivation( (uint16_t) ( ( ( pCpyBuff[0] << 8 ) & 0xFF00 ) | (  pCpyBuff[1] & 0xFF ) ) );
//			}
//            break;
//         }
//         case ParamAverageWindowSamples:
//         {
//            if(write == action)
//            {
//               setAverageWindowSamples( (uint16_t) ( ( ( pBuffer[0] << 8 ) & 0xFF00 ) | (  pBuffer[1] & 0xFF ) ) );
//            }
//            else if( read == action)
//            {
//               *(uint16_t *)pBuffer = getAverageWindowSamples();
//            }
//            else
//			{
//				*(uint16_t *)pCpyBuff = getAverageWindowSamples();
//				setAverageWindowSamples( (uint16_t) ( ( ( pCpyBuff[0] << 8 ) & 0xFF00 ) | (  pCpyBuff[1] & 0xFF ) ) );
//			}
//            break;
//         }
//         case ParamDriverExclusionTimeout:
//         {
//            if(write == action)
//            {
//               setDriverExclusionTimeout( (uint16_t) ( ( ( pBuffer[0] << 8 ) & 0xFF00 ) | (  pBuffer[1] & 0xFF ) ) );
//            }
//            else if( read == action)
//            {
//               *(uint16_t *)pBuffer = getDriverExclusionTimeout();
//            }
//            else
//			{
//				*(uint16_t *)pCpyBuff = getDriverExclusionTimeout();
//				setDriverExclusionTimeout( (uint16_t) ( ( ( pCpyBuff[0] << 8 ) & 0xFF00 ) | (  pCpyBuff[1] & 0xFF ) ) );
//			}
//            break;
//         }
//         case ParamDetectionFrequency:
//         {
//            if(write == action)
//            {
//               setDetectionFrequency( (uint16_t) ( ( ( pBuffer[0] << 8 ) & 0xFF00 ) | (  pBuffer[1] & 0xFF ) ) );
//            }
//            else if( read == action)
//            {
//               *(uint16_t *)pBuffer = getDetectionFrequency();
//            }
//            else
//			{
//				*(uint16_t *)pCpyBuff = getDetectionFrequency();
//				setDetectionFrequency( (uint16_t) ( ( ( pCpyBuff[0] << 8 ) & 0xFF00 ) | (  pCpyBuff[1] & 0xFF ) ) );
//			}
//            break;
//         }
//         case ParamNone:
//         {
//            *(uint16_t *)pBuffer = 0xFFFF;
//         }
//         default:
//         {
//            break;
//         }
//      }
//   }
//}
//
//ConfigParam_t * getConfigParam(void)
//{
//   return &configParam;
//}
//
//uint16_t getPreAlarmThreshold(void)
//{
//   return FIX_ENDIANNESS(configParam.preAlarmThreshold.typeU16.data);
//}
//
//uint16_t getAlarmThreshold(void)
//{
//   return FIX_ENDIANNESS(configParam.alarmThreshold.typeU16.data);
//}
//
//uint16_t getPreAlarmHysteresis(void)
//{
//   return FIX_ENDIANNESS(configParam.preAlarmHysteresis.typeU16.data);
//}
//
//uint16_t getAlarmHysteresis(void)
//{
//   return FIX_ENDIANNESS(configParam.alarmHysteresis.typeU16.data);
//}
//
//uint16_t getTimeout(void)
//{
//   return FIX_ENDIANNESS(configParam.timeout.typeU16.data);
//}
//
//uint16_t getPreAlarmReleActivation(void)
//{
//   return FIX_ENDIANNESS(configParam.preAlarmReleActivation.typeU16.data);
//}
//
//uint16_t getAlarmReleActivation(void)
//{
//   return FIX_ENDIANNESS(configParam.alarmReleActivation.typeU16.data);
//}
//
//uint16_t getAverageWindowSamples(void)
//{
//   return FIX_ENDIANNESS(configParam.averageWindowSamples.typeU16.data);
//}
//
//uint16_t getDriverExclusionTimeout(void)
//{
//   return FIX_ENDIANNESS(configParam.driverExclusionTimeout.typeU16.data);
//}
//
//uint16_t getDetectionFrequency(void)
//{
//   return FIX_ENDIANNESS(configParam.detectionFrequency.typeU16.data);
//}
//
///* Set */
//
//void setPreAlarmThreshold(uint16_t value)
//{
//   param_block_t *cfg = get_pbssConfig();
//   cfg->config.preAlarmThreshold_v = value;
//   configParam.preAlarmThreshold.typeU16.data = value;
//}
//
//void setAlarmThreshold(uint16_t value)
//{
//   param_block_t *cfg = get_pbssConfig();
//   cfg->config.alarmThreshold_v = value;
//   configParam.alarmThreshold.typeU16.data = value;
//}
//
//void setPreAlarmHysteresis(uint16_t value)
//{
//   param_block_t *cfg = get_pbssConfig();
//   cfg->config.preAlarmHysteresis_v = value;
//   configParam.preAlarmHysteresis.typeU16.data = value;
//}
//
//void setAlarmHysteresis(uint16_t value)
//{
//   param_block_t *cfg = get_pbssConfig();
//   cfg->config.alarmHysteresis_v = value;
//   configParam.alarmHysteresis.typeU16.data = value;
//}
//
//void setTimeout(uint16_t value)
//{
//   param_block_t *cfg = get_pbssConfig();
//   cfg->config.timeout_v = value;
//   configParam.timeout.typeU16.data = value;
//}
//
//void setPreAlarmReleActivation(uint16_t value)
//{
//   param_block_t *cfg = get_pbssConfig();
//   cfg->config.preAlarmReleActivation_v = value;
//   configParam.preAlarmReleActivation.typeU16.data = value;
//}
//
//void setAlarmReleActivation(uint16_t value)
//{
//   param_block_t *cfg = get_pbssConfig();
//   cfg->config.alarmReleActivation_v = value;
//   configParam.alarmReleActivation.typeU16.data = value;
//}
//
//void setAverageWindowSamples(uint16_t value)
//{
//   param_block_t *cfg = get_pbssConfig();
//   cfg->config.averageWindowSamples_v = value;
//   configParam.averageWindowSamples.typeU16.data = value;
//}
//
//void setDriverExclusionTimeout(uint16_t value)
//{
//   param_block_t *cfg = get_pbssConfig();
//   cfg->config.driverExclusionTimeout_v = value;
//   configParam.driverExclusionTimeout.typeU16.data = value;
//}
//
//void setDetectionFrequency(uint16_t value)
//{
//   param_block_t *cfg = get_pbssConfig();
//   cfg->config.detectionFrequency_v = value;
//   configParam.detectionFrequency.typeU16.data = value;
//}

uint8_t * getSerialNumber(void)
{
	 return serialNumber;
}
