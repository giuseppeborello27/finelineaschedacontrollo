#include "superFrameCounter.h"

static uint8_t localSuperFrameCounter = 0;

uint8_t GetSuperFrameNumber() {
  return localSuperFrameCounter;
}

void IncrementSuperFrameCounter() {
  localSuperFrameCounter++;  
  if (localSuperFrameCounter== 0) localSuperFrameCounter = 1;
}

