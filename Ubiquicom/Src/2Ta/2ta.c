#include "2ta.h"
#include "tag.h"
#include "main.h"

#define RECEPTIONTIMEOUT  10
/* ************** private data  ***************************** */
static twr_info_t     *p;
static uint32_t tStartFrame_ticks;

static const frameConfig * frameConfigurations;
static uint32_t minimumAllowedWaitDelay_ns = 1000000;
/* ************** private functions  ***************************** */



/* ************** Public functions  ***************************** */
uint32_t getCurrentTime_ticks() {
  return  (uint32_t)((hrtc.Instance->SSR) & RTC_SSR_SS);  
} 
void Twr_with_target(targetInfo *target) {

  SetCurrentTargetInfo(target);
  error_e res = initialize_secondCommunication(p, target->targetInfoId);

  initiator_wake_and_send_poll(p);
}

targetsContainer Init2Ta(frameConfig *frameConfiguration) {
  frameConfigurations = frameConfiguration;

  //SetCurrentTargetInfo(NULL);

  p = getTwrInfoPtr();

  do{
	 Sleep(1);
  }while(!(p = getTwrInfoPtr()));    //wait for initialisation of pTwrInfo



 error_e res = initialize_pollBasedResgistration(p);

  targetsContainer container = {};

  targetInfo targetA = {};
  targetA.slot_tau_ns = 15*1000000; //15 ms

  targetInfo targetB = {};
  targetB.slot_tau_ns = 95*1000000; //95 ms

  container.firstTargetInfo = targetA;
  container.secondTargetInfo = targetB;

  tStartFrame_ticks = getCurrentTime_ticks();

  return container;
}

void Twota_waitFor(bool isAfterSecondCommunication, int32_t delay) {  
   //printf("delay 1 %d\r\n",delay);

  if ( (delay < 0) || (delay < minimumAllowedWaitDelay_ns)) {
     //printf("delay 2 %d\r\n",delay);
     delay = 100000000;
    return;
  }
  twr_configure_rtc_wakeup_ns(delay);

  if (isAfterSecondCommunication)
  {
      osSignalWait(app.pollTask.Signal, osWaitForever);
  }
  else
  {
    osMutexRelease(app.pollTask.MutexId);

    osSignalWait(app.pollTask.Signal, osWaitForever);

    osMutexWait(app.pollTask.MutexId, 0);            
  }    
}

int32_t getRemainingSuperFrameTime(uint32_t startSuperFrame_ticks) {
  int32_t superFrameDuration = frameConfigurations->timeSuperFrame_ns;
  uint32_t currentTime = getCurrentTime_ticks();
  uint32_t tmpNow = (startSuperFrame_ticks > currentTime)?
                      (startSuperFrame_ticks - currentTime):
                      (startSuperFrame_ticks - currentTime + 32768);      
  int32_t remainingTime_ns  = superFrameDuration -(tmpNow*WKUP_RESOLUTION_NS);  

  return remainingTime_ns;        
}

int32_t getNextWaitingTime_ns(uint32_t startSuperFrame_ticks, uint32_t timeOffset) {
  uint32_t currentTime = getCurrentTime_ticks();
  uint32_t tmpNow = (startSuperFrame_ticks > currentTime)?
                      (startSuperFrame_ticks - currentTime):
                      (startSuperFrame_ticks - currentTime + 32768);      
  int32_t nextWakeUpPeriod_ns  = timeOffset -(tmpNow*WKUP_RESOLUTION_NS);  

  return nextWakeUpPeriod_ns;        
}

uint32_t shiftStartSuperframeTick(uint32_t startSuperFrame_ticks, int32_t timeToShift_ns) {
  int32_t timeToShift_ticks = timeToShift_ns/WKUP_RESOLUTION_NS;
    uint32_t tmpNow = (startSuperFrame_ticks + timeToShift_ticks);
                      
  // uint32_t tmpNow = (startSuperFrame_ticks > timeToShift_ticks)?
  //                     (startSuperFrame_ticks - timeToShift_ticks):
  //                     (startSuperFrame_ticks - timeToShift_ticks + 32768);      
  //printf("timeToShift_ns %d timeToShift_ticks %d startSuperFrame_ticks %d tmpNow %d\r\n",timeToShift_ns, timeToShift_ticks, startSuperFrame_ticks,tmpNow);
  return tmpNow;        
}


void static resetPacketReception(targetInfo *target){
  if(target != NULL) {
    target->receivedPacket.dist_cm = 0;
    target->receivedPacket.slotCorr_ns = 0;
    target->receivedPacket.sourceId = 0;
    target->receivedPacket.status = UNDEFINED;
  }
}

pollsSlotCorrections Prepare_next_interval_communications(targetsContainer *targets) {  
  UpdateNumberOfFails(&targets->firstTargetInfo);
  UpdateNumberOfFails(&targets->secondTargetInfo);
  UpdateTargetsForNextTrasmission(targets);

  targetInfo *firstTarget = &targets->firstTargetInfo;
  targetInfo *secondTarget = &targets->secondTargetInfo;
  
  // switch order if slots are changed
  if(firstTarget->slot_tau_ns > secondTarget->slot_tau_ns ) {
    targetInfo temp = *firstTarget; // swap
    targets->firstTargetInfo = *secondTarget;
    targets->secondTargetInfo =  temp;
  }

  // return first and second slot corrections
  //TODO:
  pollsSlotCorrections pollCorrections = {
    .firstPollCorrection_ns =  targets->firstTargetInfo.receivedPacket.slotCorr_ns,\
    .secondPollCorrection_ns =  targets->secondTargetInfo.receivedPacket.slotCorr_ns,\
    .isNextFirstPollIdentified = (targets->firstTargetInfo.targetInfoId > 0),\
    .isNextSecondPollIdentified = (targets->secondTargetInfo.targetInfoId > 0)
  };
  // reset firstTarget and secondTarget receptions (receivedPacketInfo)
  resetPacketReception(firstTarget);
  resetPacketReception(secondTarget);
  
  // return sfSlotCorrection_ns;
  return pollCorrections;
}


int32_t GetNextIdentifiedPollCorrection (pollsSlotCorrections *slotCorrections) {
  int32_t correction =0;
  if (slotCorrections != NULL) {
    if(slotCorrections->isNextFirstPollIdentified) {correction=slotCorrections->firstPollCorrection_ns;}
    else if((slotCorrections->isNextSecondPollIdentified)) {correction = slotCorrections->secondPollCorrection_ns;}
  }  
  return correction;
}


static bool CheckIfEnoughTimeAtTheEnd(int32_t identifiedCorrection_ns,uint32_t startSuperFrame_ticks ) {
  bool enoughTimeAtTheEnd = false;
  int32_t remainingSfTime_ns = getRemainingSuperFrameTime(startSuperFrame_ticks);
  // if(remainingSfTime_ns<0) {
  //   printf("remainingSfTime %d\r\n", remainingSfTime_ns);
  // }
  if(remainingSfTime_ns > 0) {
    int32_t neededTimeToCorrect = remainingSfTime_ns-identifiedCorrection_ns;
    if (neededTimeToCorrect > 0 ){
      enoughTimeAtTheEnd =  true;
    }
  }
  
  // if (!enoughTimeAtTheEnd) {
  //   printf("remainingSfTime_ns %d identifiedCorrection_ns %d\r\n", remainingSfTime_ns, identifiedCorrection_ns);
  // }
  
  return enoughTimeAtTheEnd;  
}

bool ComputeIfWeShouldCorrectWaitAtTheEndOfSuperFrame(int32_t identifiedCorrection_ns, uint32_t startSuperFrame_ticks) {
  bool shouldWaitAtEnd = true;
  if(identifiedCorrection_ns > 0) {// send identified earlier
    	shouldWaitAtEnd = CheckIfEnoughTimeAtTheEnd(identifiedCorrection_ns, startSuperFrame_ticks);
  }  
	return shouldWaitAtEnd;        
}

static int32_t getMaxAbsoluteAnonymousLeftTauCorrection(bool isNextFirstPollIdentified, int32_t *anonymousTau_ns, int32_t *identifiedTau_ns) {
  int32_t maxCorrection = 0;  
  int32_t sFrameDuration_ns = frameConfigurations->timeSuperFrame_ns;
  int32_t min2taPollsDistance_ns = frameConfigurations->slotDuration_ns * frameConfigurations->minNumOfEmptySlotsBeetweenTwoSentPolls;

  if(isNextFirstPollIdentified){
    maxCorrection= (*anonymousTau_ns)- (*identifiedTau_ns)-min2taPollsDistance_ns;
    maxCorrection = (maxCorrection<0)? 0:maxCorrection;
  }
  else {
    maxCorrection =  (*anonymousTau_ns) + (sFrameDuration_ns -  (*identifiedTau_ns))-min2taPollsDistance_ns ;
    maxCorrection = (maxCorrection<0)? 0:maxCorrection;
    maxCorrection = MIN( (maxCorrection), (*anonymousTau_ns));
  }
  return maxCorrection;
}
static int32_t getMaxAbsoluteAnonymousRightTauCorrection(bool isNextFirstPollIdentified, int32_t *anonymousTau_ns, int32_t *identifiedTau_ns) {
  int32_t maxCorrection = 0;
  int32_t sFrameDuration_ns = frameConfigurations->timeSuperFrame_ns;
  int32_t min2taPollsDistance_ns = frameConfigurations->slotDuration_ns * frameConfigurations->minNumOfEmptySlotsBeetweenTwoSentPolls;

  if(isNextFirstPollIdentified){
    maxCorrection =  (*identifiedTau_ns) + (sFrameDuration_ns - (*anonymousTau_ns)) - min2taPollsDistance_ns ;
    maxCorrection = (maxCorrection<0)? 0:maxCorrection;
    maxCorrection = MIN( (maxCorrection), (sFrameDuration_ns-(*anonymousTau_ns)) );    
  }
  else {
    maxCorrection= (*identifiedTau_ns)- (*anonymousTau_ns)-min2taPollsDistance_ns;
    maxCorrection = (maxCorrection<0)? 0:maxCorrection;
  }
  return maxCorrection;
}

void AdjustAnonymousPollCorrection(\
  pollsSlotCorrections *slotCorrections,targetInfo *nextFirstarget, targetInfo *nextSecondtarget){
  if(slotCorrections == NULL) {
    return;
  }
  //printf("apc. 1\r\n");
  int32_t identifiedCorrection_ns = 0;
  int32_t anonymousCorrection_ns = 0;

  targetInfo *nextAnonymousTarget, *nextIdentifiedTarget;
  bool isNextFirstPollIdentified = slotCorrections->isNextFirstPollIdentified;
  if(isNextFirstPollIdentified) {
    //printf("apc. 2\r\n");
    nextIdentifiedTarget = nextFirstarget;
    identifiedCorrection_ns = slotCorrections->firstPollCorrection_ns;
    
    nextAnonymousTarget = nextSecondtarget;    
    anonymousCorrection_ns = slotCorrections->secondPollCorrection_ns;
  }
  else {
    //printf("apc. 3\r\n");
    nextIdentifiedTarget = nextSecondtarget;
    identifiedCorrection_ns = slotCorrections->secondPollCorrection_ns;

    nextAnonymousTarget = nextFirstarget;    
    anonymousCorrection_ns = slotCorrections->firstPollCorrection_ns;
  }  
  if (anonymousCorrection_ns == 0) { // todo: here we should return in case only one poll get answered 
    // printf("zero anonymous correction\r\n");
    return;
  }

  int32_t correctionShift_ns  = -(anonymousCorrection_ns-identifiedCorrection_ns);
  int32_t boundedCorrectionShift_ns = 0;
  //printf("identifiedCorrection_ns %d anonymousCorrection_ns: %d\r\n",identifiedCorrection_ns, anonymousCorrection_ns);
  if(correctionShift_ns<0) { // move anonymous tau to the left
    //printf("apc. 4\r\n");
    int32_t maxAbsoluteLeftShift_ns = getMaxAbsoluteAnonymousLeftTauCorrection(isNextFirstPollIdentified,  &nextAnonymousTarget->slot_tau_ns, &nextIdentifiedTarget->slot_tau_ns);
    //printf("maxAbsoluteLeftShift_ns %d\r\n",maxAbsoluteLeftShift_ns);    
    boundedCorrectionShift_ns = -MIN((-correctionShift_ns),(maxAbsoluteLeftShift_ns));
    // printf("boundedCorrectionShift_ns %d\r\n",boundedCorrectionShift_ns);    
  } 
  else { // move anonymous tau to the rigth
    //printf("apc. 5\r\n");
    int32_t maxAbsoluteRightShift_ns = getMaxAbsoluteAnonymousRightTauCorrection(isNextFirstPollIdentified,  &nextAnonymousTarget->slot_tau_ns, &nextIdentifiedTarget->slot_tau_ns);
    //printf("maxAbsoluteRightShift_ns %d\r\n",maxAbsoluteRightShift_ns);
    boundedCorrectionShift_ns = MIN((correctionShift_ns),(maxAbsoluteRightShift_ns));
    // printf("boundedCorrectionShift_ns %d \r\n",boundedCorrectionShift_ns);    
  } 
  //printf("apc. 6\r\n");
  nextAnonymousTarget->slot_tau_ns = nextAnonymousTarget->slot_tau_ns + boundedCorrectionShift_ns;
  nextAnonymousTarget->slot_tau_ns = MIN( nextAnonymousTarget->slot_tau_ns, (frameConfigurations->timeSuperFrame_ns-frameConfigurations->slotDuration_ns));
}

uint8_t updateNonReceptionTimeout(uint8_t ReceptionTimeout)
{
  if (ReceptionTimeout >= RECEPTIONTIMEOUT)
  {
//    HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin,GPIO_PIN_RESET); TODO: Configurare la segnalazione con il nuovo HW
    ReceptionTimeout = 0;
  }
  else
  {
    ReceptionTimeout ++;
  }
  return ReceptionTimeout;
  
}
