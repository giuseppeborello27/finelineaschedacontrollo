#include "targetmanager.h"
#include "tag.h"


static targetInfo *currentTarget;
static targetInfo dummyTarget = {};

void SetCurrentTargetInfo(targetInfo *target) {
  if (target != NULL) {
    currentTarget = target;  
  }
  else {
    currentTarget = &dummyTarget;  
  }
}

void SetCurreSlotCorrection(int32_t slotCorr_ns) {
  currentTarget->receivedPacket.slotCorr_ns = slotCorr_ns;  
}
void SetCurrentTargetResponseStatus(uint16_t dist_cm, uint16_t sourceId,receivedPacketStatus status) {
    currentTarget->receivedPacket.sourceId = sourceId;
    currentTarget->receivedPacket.dist_cm = dist_cm;
    //TODO: save the distance 

    receivedPacketStatus *currentStatus = &currentTarget->receivedPacket.status;
    if(status == RECEIVED_A_RESPONSE_FOR_THIS_TAG ) {
      // get expected target id
      uint16_t expectedTargetId = currentTarget->targetInfoId;      
      if( (expectedTargetId !=0) && \
          (expectedTargetId!=sourceId)) {*currentStatus = PACKET_FROM_NOT_EXPECTED_SOURCE_ID;}
      else{*currentStatus = RECEIVED_EXPECTED_RESPONSE_PACKET;}      
    }
    else {
      *currentStatus = status;      
    }

}


void WaitForFinishingComunication() {
  osSignalWait(2, osWaitForever);  
}

void SignalFinishedSecondPollCommunication() {
   if(osSignalSet(app.pollTask.Handle, 2) != osOK)
    {
        error_handler(1, _Err_Signal_Bad);
   }    
}

void UpdateNumberOfFails(targetInfo *target) {
    receivedPacketStatus status = target->receivedPacket.status;
    uint16_t *numOfFails;
    
    if(target->targetInfoId == 0 )  {
      numOfFails = &target->numberOfAnonymousReceptionFailsInThisTalkingSlot;
    }else {
      numOfFails = &target->numberOfReceptionFailsFromThisSourceId;        
    }
    
    if( status &&
      ( status & (UNKNOWN_PACKET | PACKET_FROM_NOT_EXPECTED_SOURCE_ID \
        | RESPONSE_ADDRESSED_TO_ANOTHER_TAG_ID | ERROR_OR_TIMEOUT_IN_RECEPTION))\
      ) {        
      (*numOfFails)++;
    }                  
}

targetInfo *GetCurrentTargetInfo() {
  return currentTarget;
}