#include "randomSlotAssignment.h"

#include "math.h"
#include <stdlib.h>

/* ************** private data  ***************************** */
static randomSlotsAssignmentInit initConfig = 
  {
  .minNumOfEmptySlotsBeetweenTwoPolls = 2,
  .numOfSlotsInSframe = 20,
  .slotDuration_ns=(5000 * 1000)
  };

/* ************** private functions  ***************************** */

// Assumes 0 <= max <= RAND_MAX
// Returns in the closed interval [0, max]
// acap: adapted from https://stackoverflow.com/questions/2509679/how-to-generate-a-random-integer-number-from-within-a-range
static uint32_t random_at_most(uint32_t max) {
  uint32_t 
    // max <= RAND_MAX < ULONG_MAX, so this is okay.
    num_bins = (uint32_t) max + 1,
    num_rand = (uint32_t) RAND_MAX + 1,
    bin_size = num_rand / num_bins,
    defect   = num_rand % num_bins;

  int32_t x;
  do {
   x = rand();
  }
  // This is carefully written not to overflow
  while (num_rand - defect <= (uint32_t)x);

  // Truncated division is intentional
  return x/bin_size;
}

static uint8_t getRandomSlot(uint8_t maxSlotNumber ){
    uint32_t rndRange = random_at_most(maxSlotNumber-1) + 1;
    uint8_t slotNumber = (uint8_t) rndRange;   
    return slotNumber;
}

// todo: if slotNumber==0 => ERROR
static uint32_t slotNumberToSlotStartTime(uint8_t slotNumber) {    
    uint32_t slotStartTime_ns = (slotNumber -1) * initConfig.slotDuration_ns;
    return slotStartTime_ns;
}

static uint8_t slotStartTimeToSlotNumber(uint32_t slotStartTime_ns) {  
  uint8_t nSlot = slotStartTime_ns/initConfig.slotDuration_ns + 1; 
  return nSlot; 
}

/* ************** Public functions  ***************************** */
void InitRandomSlotAssignment(uint32_t seed) {
      srand(seed);
}



randomSlotsResult GetTwoRandomPollsTimes() {
  randomSlotsResult res = {};
  
  uint8_t nSlot = initConfig.numOfSlotsInSframe;
  uint8_t minBucketDist = initConfig.minNumOfEmptySlotsBeetweenTwoPolls;

  uint8_t firstRandomSlot = getRandomSlot(nSlot);
  
  uint8_t rndOffset = getRandomSlot(nSlot-(1+2*minBucketDist));
  uint8_t eta1 = (firstRandomSlot + minBucketDist) % nSlot;
  uint8_t secondRandomSlot = (eta1 + rndOffset) % nSlot;
  if (secondRandomSlot == 0) { secondRandomSlot = nSlot;}
  
  res.first_of_two_tau_ns = slotNumberToSlotStartTime(firstRandomSlot);
  res.second_of_Two_tau_ns = slotNumberToSlotStartTime(secondRandomSlot);

   return res;
}

randomSlotsResult GetOtherRandomPollSlot(uint32_t fixedSlotTau_ns) {
  randomSlotsResult res = {};

  uint8_t nSlot = initConfig.numOfSlotsInSframe;
  uint8_t minBucketDist = initConfig.minNumOfEmptySlotsBeetweenTwoPolls;  
  uint8_t fixedSlotNumber = slotStartTimeToSlotNumber(fixedSlotTau_ns);


  uint8_t rndOffset = getRandomSlot(nSlot-(1+2*minBucketDist));
  uint8_t eta1 = (fixedSlotNumber + minBucketDist) % nSlot;
  uint8_t otherRandomSlot = (eta1 + rndOffset) % nSlot;
  if (otherRandomSlot == 0) { otherRandomSlot = nSlot;}

  res.single_tau_ns = slotNumberToSlotStartTime(otherRandomSlot);

   return res;
}
