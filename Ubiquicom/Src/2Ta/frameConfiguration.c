#include "frameConfiguration.h"


//TODO: we should get this values from app structure
static frameConfig frameConfiguration = \
  { .timeSuperFrame_ns =100*1000000, //100 ms\
    .numSlot = 20,
    .minTimeAllocatedForASlot_ns = 5*1000000, // 5 ms
    .slotDuration_ns=(5000 * 1000),
    .minNumOfEmptySlotsBeetweenTwoSentPolls = 2
  };


frameConfig *GetFrameConfiguration() {
  return &frameConfiguration;
}

