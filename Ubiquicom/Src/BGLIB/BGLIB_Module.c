/** 
*	\file BGLIB_Module.c
*	\brief [Description] - Source File.
*	\author Luca Bortolotti
*	\date   16-May-2017
*	\copyright Copyright (c) 2017 E-Novia Srl. All Rights Reserved.
* 
* E-NOVIA CONFIDENTIAL
* __________________
* 
* ALL INFORMATION CONTAINED HEREIN WAS PRODUCED BY E-NOVIA SRL AND NO THIRD PARTY MAY CLAIM ANY RIGHT OR PATERNITY ON IT.\n 
* THE INTELLECTUAL AND TECHNICAL CONCEPTS CONTAINED HEREIN ARE AND REMAIN
* THE PROPERTY OF E-NOVIA SRL AND ARE PROTECTED BY TRADE SECRET OR COPYRIGHT LAW.
* REPRODUCTION, DISTRIBUTION OR DISCLOSURE OF THIS MATERIAL TO ANY OTHER PARTY
* IS STRICTLY FORBIDDEN UNLESS PRIOR WRITTEN PERMISSION IS OBTAINED FROM E-NOVIA SRL
*/ 

#ifndef USE_OLD_BLE_IF

#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "main.h"
#include "BGLIB_Module.h"
//#include "appConfig.h"


#include "cmsis_os.h"

//#include "debug_log.h"
#include "cmd_def.h"
#include "version.h"


/** 
*	\defgroup BGLIB_Module_PRIVATE PRIVATE
*	\ingroup BGLIB_Module_MODULE
* 	\brief BGLIB Module Private Definitions and Functions.
*	\{
*/

#define BGLIB_RX_BUFFER_SIZE	256     /**< The Size of the buffer to receive messages from the BLE module */

#define DBG_INFO(x)

/** \enum BGLIB_RxPhase_t
*	\brief Descriptor of Rx messages phases
*
*/
typedef enum
{
	RX_PHASE_HEADER = 0,	/**< The MCU is receiving the message header from the BLE module */
	RX_PHASE_DATA,			/**< The MCU is receiving the message data from the BLE module */
	
	RX_PHASE_INVALID		/**< The invalid Rx Phase */
}BGLIB_RxPhase_t;

/** \struct BGLIB_RxCtx_t
*	\brief The struct containing all the receiving context variables
*
*/
typedef struct
{
	struct ble_header hdr;
	uint8_t hdrOffset;
	uint8_t data[BGLIB_RX_BUFFER_SIZE];
	uint16_t dataOffset;
	BGLIB_RxPhase_t rxPhase;
}BGLIB_RxCtx_t;

uint32_t heartbeat_cnt;

extern UART_HandleTypeDef huart4;
extern UART_HandleTypeDef huart5;
extern uint8_t UART4_RxData;
extern uint8_t UART5_RxData;

static BGLIB_RxCtx_t rxCtx;		/**< The Local Rx Messages from module context */

static osTimerId BGLIB_RxTimerID = 0;

osSemaphoreId OS_EventID_BGLIB_Handler = 0;

osSemaphoreId OS_EventID_BGLIB_FWUP_Handler = 0;

/*

static uint8_t flagsData[3] = {};
//static uint8_t scanData[16] = {0x0F, 0x09, 0x53, 0x49, 0x4D, 0x2D, 0x43, 0x4c, 0x41, 0x5F, 0x5F, 0x20, 0x54, 0x45, 0x53, 0x54};

//static uint8_t nameData[15] = {0x0E, 0x09, 'K', 'A', 'L', 'P', 'A', '-', 'P', 'E', 'R' , 'S', 'O', 'N', 'A'};

static uint8_t nameData[100] = {0x02, gap_ad_type_flags, 0x06,\
								0x011, gap_ad_type_services_128bit_all, 0x7c, 0xeb, 0x55, 0x61, 0xcc, 0x6c, 0x43, 0xeb, 0xa5, 0x5b, 0x7c, 0x38, 0x0a, 0x1a, 0x3d, 0xb5
								};
								//0x14, gap_ad_type_localname_complete,'U','P','A','-','2','0','1','0','1','4','B','-', 'V', '0', '0', '0', '0', '0', '1', \
								};
                                //0x011, gap_ad_type_services_128bit_all, 0x7c, 0xeb, 0x55, 0x61, 0xcc, 0x6c, 0x43, 0xeb, 0xa5, 0x5b, 0x7c, 0x38, 0x0a, 0x1a, 0x3d, 0xb5};

static uint8_t tmpManData [] = {0x14, gap_ad_type_localname_complete,'U','P','A','-','2','0','1','0','1','4','B','-', 'V', '0', '0', '0', '0', '0', '1',};
//static uint8_t manufacturerData[11] = {0x0A, 0xFF, 'U', 'B', 'I', 'Q', 'U', 'I', 'C', 'O', 'M'};

*/

/**/

static void BGLIB_buildAdvScanMessageString(uint8_t * pAdvMsg, uint8_t *pAdvMsgLen, enum gap_ad_types typeMsg, const char * pStringMsg );
static void BGLIB_buildAdvScanMessageByteArray(uint8_t * pAdvMsg, uint8_t *pAdvMsgLen, enum gap_ad_types typeMsg, uint8_t * pArrayMsg, uint8_t arrayMsgLen );

/**
*	\brief Function to send messages to BGAPI from BGLIB throught UART
*	\param[in] len1 length of data1
*	\param[in] data1 command to send
*	\param[in] len2 length of data2
*	\param[in] data2 data to send
*/
static void BGLIB_output(uint8 len1, uint8* data1, uint16 len2, uint8* data2);

/**
*	\brief Function to Reset the Rx context
*/
static void BGLIB_ResetRxCtx(void);

/**
*	\brief Function to Init the Rx context
*/
static void BGLIB_RxInit(void);

/**
*	\brief Function to DeInit the Rx context
*/
static void BGLIB_RxDeInit(void);

/**
*	\brief Function called on Rx timeout
*/
static void BGLIB_RxTimeout(void);

/**
*	\brief Function to manage received data from module
*/
static void BGLIB_ManageReceivedData(void);

/**
*	\brief Function to send messages to BGAPI from BGLIB throught UART.
*			This function works only if \ref BGLIB_RAW_PACKET_PRINT_ENABLE is set to 1.
*	\param[in] hdr The Packet Header
*	\param[in] data The Packet Data
*/
static void BGLIB_PrintRawPacket(struct ble_header *hdr, unsigned char *data);



/**
* 	\}
*/


/** 
*	\addtogroup BGLIB_Module_PUBLIC
*	\{
*/

uint8_t bglib_rx_byte = 0U;

extern uint8_t UpdateDataBuffer[2][50];
extern uint8_t ToggleBuffer;
uint8_t pValueBuffer[5] = {0};
bool isFWRecordValid = true;
 
//void BGLIB_RxHandler(void)
//{
//    BGLIB_RxCallback(bglib_rx_byte);
//    HAL_UART_Receive_IT(&huart4, &UART4_RxData, sizeof(uint8_t));
//}
extern uint8_t isDeviceDisconnected;
void BGLIB_RxHandler(void * arguments)
{

//   static uint8_t flag = 0;
   BGLIB_Init();

//   BGLIB_DBG_AdvertisingStop();
//   osDelay(500);

   BGLIB_DBG_AdvertisingStart();

    //BGLIB_DBG_ScanStart();
    //ble_cmd_gap_discover(gap_discover_observation);

   for(;;)
   {
      if( OS_EventID_BGLIB_Handler )
      {
         if ( osSemaphoreWait(OS_EventID_BGLIB_Handler, osWaitForever) == osOK)
         {
            BGLIB_RxCallback(bglib_rx_byte);
            HAL_UART_Receive_IT(&huart4, &UART4_RxData, sizeof(uint8_t));
         }
         if( true == getRowComplete() )
         {
        	 setRowComplete (false);
//        	 if ( FALSE == FWREC_SplitIntoRecords(UpdateDataBuffer[ToggleBuffer], 50) )
//        	 {
//        		 isFWRecordValid = false;
//        	 }
         }

//            BGLIB_DBG_AdvertisingStop();
//            osDelay(250);
//         if(flag < 1)
//         {
//
//            BGLIB_DBG_AdvertisingStart();
//
//            flag++;
//         }

         //osDelay(100);
      }

      else
      {
         osDelay(100);
      }

   }
}

void BGLIB_Init(void)
{	
    /*Enable the uart 1 with standard configuration*/
    BGLIB_UART_INIT();
    /*Configure GPIO*/
    BGLIB_INIT_PIN_RESET();
    BGLIB_INIT_PIN_WAKEUP();
    
    BGLIB_ResetModule();
    
    if( !OS_EventID_BGLIB_Handler )
    {
		osSemaphoreDef(OS_EventID_BGLIB_Name);
		OS_EventID_BGLIB_Handler = osSemaphoreCreate(osSemaphore(OS_EventID_BGLIB_Name), 1);
    }
    if ( !OS_EventID_BGLIB_FWUP_Handler )
    {
		osSemaphoreDef(OS_EventID_BGLIB_FWUP_Name);
		OS_EventID_BGLIB_FWUP_Handler = osSemaphoreCreate(osSemaphore(OS_EventID_BGLIB_FWUP_Name), 1);
    }
	/* Set the Output Function to send UART MESSAGES */
	bglib_output = BGLIB_output;

	/* Init Receiving */
	BGLIB_RxInit();
    
	osDelay(3000);

    DBG_INFO("BGLIB_Init");
}

void BGLIB_DeInit(void)
{	
	/* UnSet the Output Function to send UART MESSAGES */
	bglib_output = NULL;

	/* DeInit Receiving */
	BGLIB_RxDeInit();
}

void BGLIB_ResetModule(void)
{
#if BGLIB_RESET_PIN_ENABLED
	HAL_GPIO_WritePin(BGLIB_RESET_PORT, BGLIB_RESET_PIN, GPIO_PIN_RESET);
	osDelay(100);

    HAL_GPIO_WritePin(BGLIB_RESET_PORT, BGLIB_RESET_PIN, GPIO_PIN_SET);	
#else
#warning "Reset Pin Not Configured, call BGLIB_ResetModule() has no effect"
#endif
}

void BGLIB_RxCallback(uint8_t rxByte)
{

    HAL_NVIC_DisableIRQ(UART4_IRQn);

    if( !BGLIB_RxTimerID )
    {
       osTimerDef(BGLIB_Rx_TimerName, BGLIB_RxTimeout);
       BGLIB_RxTimerID = osTimerCreate(osTimer(BGLIB_Rx_TimerName), osTimerOnce, NULL);
    }
    osTimerStart(BGLIB_RxTimerID, BGLIB_WAIT_RX_TIMEOUT);

	switch(rxCtx.rxPhase)
	{
		case RX_PHASE_HEADER:
		{
			if(rxCtx.hdrOffset < sizeof(rxCtx.hdr))
			{
				((uint8_t *)&rxCtx.hdr)[rxCtx.hdrOffset] = rxByte;
				rxCtx.hdrOffset++;
				
				if(rxCtx.hdrOffset == sizeof(rxCtx.hdr))
				{
					if(rxCtx.hdr.lolen)
					{
						rxCtx.rxPhase = RX_PHASE_DATA;
						osTimerStart(BGLIB_RxTimerID, BGLIB_WAIT_RX_TIMEOUT);
					}
					else
					{
						BGLIB_ResetRxCtx();
					}
				}				
			}
			else
			{
				BGLIB_ResetRxCtx();
			}				
		}
		break;
		
		case RX_PHASE_DATA:
		{
			if(rxCtx.dataOffset < rxCtx.hdr.lolen)
			{
				rxCtx.data[rxCtx.dataOffset] = rxByte;
				rxCtx.dataOffset++;
				
				if(rxCtx.dataOffset == rxCtx.hdr.lolen)
				{
                    BGLIB_ManageReceivedData(); 
                    
				}				
			}
			else
			{
//				DBG_INFO("Ignoring data [0x%02] to wait received message management.", rxByte); ##CC
			}			
		}
		break;
		
		case RX_PHASE_INVALID:
		default:
			BGLIB_ResetRxCtx();
			break;
	}	
    HAL_NVIC_EnableIRQ(UART4_IRQn);
}

//#ifdef BLE_DBG
void BGLIB_DBG_ScanStart(void)
{
    ble_cmd_gap_discover(2);
}

void BGLIB_DBG_ScanStop(void)
{
    ble_cmd_gap_end_procedure();
}

void BGLIB_DBG_AdvertisingStart(void)
{
    /* Set Adv Data */
	static uint8_t ubiquicomMan[15] ={0xA0, 0xA1};

    static uint8_t manufacturerData[] = FULL_VERSION;
    static uint8_t uuidsValue[] = { 0x7c, 0xeb, 0x55, 0x61, 0xcc, 0x6c, 0x43, 0xeb, 0xa5, 0x5b, 0x7c, 0x38, 0x0a, 0x1a, 0x3d, 0xb5};
    uint8_t advMesgString[60] = {0};
	uint8_t advMsgLen = 0;

    BGLIB_buildAdvScanMessageString(advMesgString, &advMsgLen, gap_ad_type_flags, "6");
    BGLIB_buildAdvScanMessageByteArray(&advMesgString[advMsgLen], &advMsgLen, gap_ad_type_services_128bit_all, uuidsValue, 16);

    osDelay(1000);

    ble_cmd_gap_set_adv_data(0, advMsgLen, advMesgString);

    advMsgLen = 0;


    BGLIB_buildAdvScanMessageString(advMesgString, &advMsgLen,  gap_ad_type_localname_complete, getSerialNumber());
    memcpy(&ubiquicomMan[2],manufacturerData, sizeof(manufacturerData));
    BGLIB_buildAdvScanMessageByteArray(&advMesgString[advMsgLen], &advMsgLen,  custom_gap_adv_type_manufact, ubiquicomMan, sizeof(manufacturerData)+2);

    ble_cmd_gap_set_adv_data(1, advMsgLen, advMesgString);

    osDelay(1000);

    /* Set Adv Params (320ms min and max on all three channels) */
    ble_cmd_gap_set_adv_parameters(0x200, 0x200, 0x07);
    
    /* Set Adv Mode */
//    ble_cmd_gap_set_mode(gap_user_data, gap_scannable_non_connectable);
    ble_cmd_gap_set_mode(gap_user_data, gap_directed_connectable);
    
}

static void BGLIB_buildAdvScanMessageString(uint8_t * pAdvMsg, uint8_t *pAdvMsgLen, enum gap_ad_types typeMsg, const char * pStringMsg )
{
	if(NULL != pAdvMsg)
	{
		pAdvMsg[0] = (1+ strlen(pStringMsg));
		pAdvMsg[1] = typeMsg;
		memcpy(&pAdvMsg[2], pStringMsg, strlen(pStringMsg));

		*pAdvMsgLen += pAdvMsg[0] + 1 ;
	}
}

static void BGLIB_buildAdvScanMessageByteArray(uint8_t * pAdvMsg, uint8_t *pAdvMsgLen, enum gap_ad_types typeMsg, uint8_t * pArrayMsg, uint8_t arrayMsgLen )
{
	if(NULL != pAdvMsg)
	{
		pAdvMsg[0] = (1+ arrayMsgLen);
		pAdvMsg[1] = typeMsg;
		memcpy(&pAdvMsg[2], pArrayMsg, arrayMsgLen);

		*pAdvMsgLen += pAdvMsg[0] + 1 ;
	}
}


void BGLIB_DBG_AdvertisingStop(void)
{
    ble_cmd_gap_set_mode(gap_non_discoverable, gap_non_connectable);
}
//#endif

/**
*	\}
*/ 

/** 
*	\addtogroup BGLIB_Module_PRIVATE
*	\{
*/

static void BGLIB_ResetRxCtx(void)
{
   if( BGLIB_RxTimerID )
   {
      osTimerStop(BGLIB_RxTimerID);
   }

	memset(&rxCtx, 0x00, sizeof(BGLIB_RxCtx_t));
}

static void BGLIB_output(uint8 len1, uint8* data1, uint16 len2, uint8* data2)
{    
    bool res  = true;
    
    if(data1 && len1 > 0)
        res = BGLIB_TX(len1, data1);
    
    if(data2 && len2 > 0)
        res =res && BGLIB_TX(len2, data2);
    
    if (!res)
	{
//        DBG_ERROR("Writing to serial port failed"); ##CC
    }    
    else
    {
    HAL_UART_Receive_IT(&huart4, &UART4_RxData, sizeof(uint8_t));
    }
}

static void BGLIB_RxInit(void)
{
	/* Reset Rx Ctx */
	BGLIB_ResetRxCtx();	
	
#if BGLIB_WAKEUP_PIN_ENABLED	
	/* Enable BLE Module Wake-Up Pin */
	HAL_GPIO_WritePin(BGLIB_WAKEUP_PORT, BGLIB_WAKEUP_PIN, GPIO_PIN_SET);
#endif
    HAL_UART_Receive_IT(&huart4, &UART4_RxData, sizeof(uint8_t));
	
		
}

static void BGLIB_RxDeInit(void)
{

#if BGLIB_WAKEUP_PIN_ENABLED	
	/* Disable BLE Module Wake-Up Pin */
	HAL_GPIO_WritePin(BGLIB_WAKEUP_PORT, BGLIB_WAKEUP_PIN, GPIO_PIN_RESET);
#endif
	
	/* Reset Rx Ctx */
	BGLIB_ResetRxCtx();	
}

static void BGLIB_RxTimeout(void)
{
	BGLIB_ResetRxCtx();	
}

static void BGLIB_ManageReceivedData(void)
{
    const struct ble_msg *msg = ble_get_msg_hdr(rxCtx.hdr);	
    BGLIB_PrintRawPacket(&rxCtx.hdr, rxCtx.data);

    if(msg)
    {
        msg->handler(rxCtx.data);
    }
    else
    {
//        ##CC DBG_ERROR("Unknown Message Received");
    }
    
    BGLIB_ResetRxCtx();	
}

static void BGLIB_PrintRawPacket(struct ble_header *hdr, unsigned char *data)
{
#if BGLIB_RAW_PACKET_PRINT_ENABLE
    heartbeat_cnt = HAL_GetTick();
//    ##CC DBG_ENTRY("Incoming packet: ");
    int i;
    for (i = 0; i < sizeof(*hdr); i++) {
//        DBG_INFO("%02x ", ((unsigned char *)hdr)[i]); ##CC
    }
    for (i = 0; i < hdr->lolen; i++) {
//        DBG_INFO("%02x ", data[i]); ##CC
    }
//    ##CC DBG_EXIT("\n");
#endif
}

/**
* 	\}
*/

#endif /*USE_OLD_BLE_IF*/

/* EOF */
