/*
 * BLE_Protocol.c
 *
 *  Created on: Nov 4, 2020
 *      Author: CCagninelli
 */

#include "BLE_Protocol.h"
//#include "appConfig.h"


typedef struct
{
	uint8_t len;
	uint8_t reqNum;
	uint8_t cmdType;
	uint8_t paramId;
	uint8_t payload[BLE_PACKET_MAX_SIZE];
	uint16_t crc;

}__attribute__((__packed__))BLE_payloadMsg_t;


/**
 * return INVALID se pacchetto corrotto;
 * return VALID se pacchetto ok,
 */




/*
		 * Da inserire parte relativa al CRC se richiesto!
		 *
		 * LEN
		 * CMD_TYPE non trovato;
		 * PARAM_ID non trovato;
		 * CRC sbagliato.
		 *
		 * */
bleMsgIntegrity_e BLE_verifyPacket(const struct ble_msg_attributes_value_evt_t *msg)
{
	bleMsgIntegrity_e retVal = VALID;
//	BLE_payloadMsg_t * blePayload  = (BLE_payloadMsg_t *) msg->value.data;
//
//	if(  msg->value.len != blePayload->len )
//	{
//		retVal = INVALID;
//	}
//	if( blePayload->cmdType >= MAX_cmd)
//	{
//		retVal = INVALID;
//	}
//	else
//	{
//		if( blePayload->cmdType < cmdStartUpgradeFwReq )
//		{
//			if(blePayload->paramId >= MAX_ParamID)
//			{
//				retVal = INVALID;
//			}
//		}
//	}

	/*if(blePayload.crc != BLE_verifyCRC(blePayload))
	 * {
	 * retVal = INVALID;
	 * }*/

	return retVal;
}

/*
 * output variable: uint8_t *pMsgToBLE, uint8_t *msgLen
 *
 * input uint8_t *msgPayload
 * */
void BLE_buildPacket( uint8_t *pMsgToBLE, uint8_t *pMsgLen, uint8_t *pMsgData , cmdType_e cmdType, uint8_t *pMsgPayload, uint8_t msgSize)
{
	pMsgToBLE[CMD_REQ_IDX]   = pMsgData[CMD_REQ_IDX];
    pMsgToBLE[CMD_TYPE_IDX]  = cmdType;
	pMsgToBLE[CMD_PARAM_IDX] = pMsgData[CMD_PARAM_IDX];

	memcpy(&pMsgToBLE[CMD_FIRST_BYTE_IDX], pMsgPayload, msgSize );

	/* aggiungere un indomani IL CRC */

	 pMsgToBLE[CMD_LEN_IDX] = CALCULATE_MSG_SIZE(msgSize);

	 *pMsgLen = pMsgToBLE[CMD_LEN_IDX];
}

