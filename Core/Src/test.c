#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <port.h>
#include <config.h>
#include <task_ctrl.h>
#include <task_flush.h>
#include <task_tag.h>
#include "stm32l4xx_hal.h"

//MY_MOD
//#include <tagManager.h>
//#include "randomSlotAssignment.h"
#include "stdio.h"
//#include "i2c.h"
//#include "nxp_pcal6416a.h"
//#include "typeDefs.h"
//#include "nxp_pcal6416a.h"
//#include "uart_command.h"
//#include "inertial_sensing.h"
#include "test.h"
//#include "cmd_def.h"

CAN_TxHeaderTypeDef   TxHeader;
uint8_t               TxData[8];

CAN_RxHeaderTypeDef   RxHeader;
uint8_t               RxData[8];
uint32_t              TxMailbox;

extern CAN_HandleTypeDef hcan1;

static volatile uint8_t arrived=0;


/**
  * @brief  Rx Fifo 0 message pending callback
  * @param  hcan: pointer to a CAN_HandleTypeDef structure that contains
  *         the configuration information for the specified CAN.
  * @retval None
  */
//void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)
//{
//	uint8_t ubKeyNumber = 0x0;
//
//  /* Get RX message */
//  if (HAL_CAN_GetRxMessage(hcan, CAN_RX_FIFO0, &RxHeader, RxData) != HAL_OK)
//  {
//    /* Reception Error */
//    Error_Handler();
//  }
//
//  if ((RxHeader.StdId == 0x321) && (RxHeader.IDE == CAN_ID_STD) && (RxHeader.DLC == 4))
//  {
//	  if ((RxData[0]=='P') && (RxData[1]=='O') && (RxData[2]=='N') && (RxData[3]=='G'))
//		  ubKeyNumber=1;
//  }
//}

void CAN_RxManage(void)
{
	//ENABLE CAN TRANSCEIVER
	HAL_GPIO_WritePin(GPIOA, CAN_SHDN_Pin, GPIO_PIN_RESET);

	/*##-5- Configure Transmission process #####################################*/
	  TxHeader.StdId = 0x321;
	  TxHeader.ExtId = 0x01;
	  TxHeader.RTR = CAN_RTR_DATA;
	  TxHeader.IDE = CAN_ID_STD;
	  TxHeader.DLC = 4;
	  TxHeader.TransmitGlobalTime = DISABLE;

	/* If Got RX message */
	 if (HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &RxHeader, RxData) == HAL_OK)
	 {

		// risposta arrivata - verifico
		 if ((RxHeader.StdId == 0x321) && (RxHeader.IDE == CAN_ID_STD) && (RxHeader.DLC == 4))
		 {
			 // se arriva un PING rispondo con PONG
			 if ((RxData[0]=='P') && (RxData[1]=='I') && (RxData[2]=='N') && (RxData[3]=='G'))
			 {

				 /* Set the data to be transmitted */
				  TxData[0] = 'P';
				  TxData[1] = 'O';
				  TxData[2] = 'N';
				  TxData[3] = 'G';

				  /* Start the Transmission process */
				  if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &TxMailbox) != HAL_OK)
				  {
					  /* Transmission request Error */
					  Error_Handler();
					  //return FALSE;
				  }
			 }
		 }
	 }
}

//CAN TASK
//void CAN_RxHandler(void * arguments)
//{
//	//ENABLE CAN TRANSCEIVER
//	HAL_GPIO_WritePin(GPIOA, CAN_SHDN_Pin, GPIO_PIN_RESET);
//
//	/*##-5- Configure Transmission process #####################################*/
//	  TxHeader.StdId = 0x321;
//	  TxHeader.ExtId = 0x01;
//	  TxHeader.RTR = CAN_RTR_DATA;
//	  TxHeader.IDE = CAN_ID_STD;
//	  TxHeader.DLC = 4;
//	  TxHeader.TransmitGlobalTime = DISABLE;
//
//	 for(;;)
//	 {
//		 taskENTER_CRITICAL();
//
//		/* If Got RX message */
//		 if (HAL_CAN_GetRxMessage(&hcan1, CAN_RX_FIFO0, &RxHeader, RxData) == HAL_OK)
//		 {
//
//			// risposta arrivata - verifico
//			 if ((RxHeader.StdId == 0x321) && (RxHeader.IDE == CAN_ID_STD) && (RxHeader.DLC == 4))
//			 {
//				 // se arriva un PING rispondo con PONG
//				 if ((RxData[0]=='P') && (RxData[1]=='I') && (RxData[2]=='N') && (RxData[3]=='G'))
//				 {
//
//					 /* Set the data to be transmitted */
//					  TxData[0] = 'P';
//					  TxData[1] = 'O';
//					  TxData[2] = 'N';
//					  TxData[3] = 'G';
//
//					  /* Start the Transmission process */
//					  if (HAL_CAN_AddTxMessage(&hcan1, &TxHeader, TxData, &TxMailbox) != HAL_OK)
//					  {
//						  /* Transmission request Error */
//						  Error_Handler();
//						  //return FALSE;
//					  }
//				 }
//			 }
//		 }
//
//		  taskEXIT_CRITICAL();
//
//		  HAL_Delay(100);
//		  //osDelay(1000);
//		  osThreadYield();
//	  }
//}


//void testLATLED()
//{
//	//printf ("INIT TEST LATLED\n ");
//	for (char count=0; count<3;count++)
//	{
//		 //HAL_Delay(500);
//		osDelay (500);
//
//		//LATLED: accendo alternativamente i colori Rosso, Verde e Blu ripetendo ciclicamente la sequenza
//		 HAL_GPIO_WritePin(GPIOC, LLED_BLUE_Pin, GPIO_PIN_SET);
//		 HAL_GPIO_WritePin(GPIOC, LLED_RED_Pin, GPIO_PIN_RESET);
//
//		 //HAL_Delay(500);
//		osDelay (500);
//
//		 HAL_GPIO_WritePin(GPIOC, LLED_RED_Pin, GPIO_PIN_SET);
//		 HAL_GPIO_WritePin(GPIOC, LLED_GREEN_Pin, GPIO_PIN_RESET);
//
//		 //HAL_Delay(500);
//		osDelay (500);
//
//		 HAL_GPIO_WritePin(GPIOC, LLED_GREEN_Pin, GPIO_PIN_SET);
//		 HAL_GPIO_WritePin(GPIOC, LLED_BLUE_Pin, GPIO_PIN_RESET);
//
//	}
//}
//
//void testRELE1()
//{
//	//printf ("INIT TEST RELE1\n ");
//
//	 //HAL_Delay(1000);
//	 osDelay (1000);
//
//	 //ATTIVO RELE1:
//	 HAL_GPIO_WritePin(GPIOA, RELAY_K1_Pin, GPIO_PIN_SET);
//
//	 //HAL_Delay(1000);
//	 osDelay (1000);
//
//	 //DISATTIVO RELE1:
//	 HAL_GPIO_WritePin(GPIOA, RELAY_K1_Pin, GPIO_PIN_RESET);
//
//}
//
//void testRELE2()
//{
//	//printf ("INIT TEST RELE2\n ");
//
//	//HAL_Delay(1000);
//	osDelay (1000);
//
//	 //ATTIVO RELE2:
//	 HAL_GPIO_WritePin(GPIOA, RELAY_K2_Pin, GPIO_PIN_SET);
//
//	 //HAL_Delay(1000);
//	 osDelay (1000);
//
//	 //DISATTIVO RELE2:
//	 HAL_GPIO_WritePin(GPIOA, RELAY_K2_Pin, GPIO_PIN_RESET);
//
//}
//
//
//void testMAINLED()
//{
//
//	//printf ("INIT TEST MAINLED\n ");
//	for (char count=0; count<3;count++)
//	{
//		 HAL_Delay(500);
//
//
//		//LATLED: accendo alternativamente i colori Rosso, Verde e Blu ripetendo ciclicamente la sequenza
//		setPWM(htim3, TIM_CHANNEL_2, 255, 0); 	//BLUE oFF
//		setPWM(htim2, TIM_CHANNEL_2, 255, 255); 	//RED on
//
//		HAL_Delay(500);
//
//
//		 setPWM(htim2, TIM_CHANNEL_2, 255, 0); 		//RED off
//		 //	stopPWM(htim2, TIM_CHANNEL_2);			//RED off
//		 setPWM(htim3, TIM_CHANNEL_1, 255, 255); 	//GREEN on
//
//		 HAL_Delay(500);
//
//		 setPWM(htim3, TIM_CHANNEL_1, 255, 0); 		//GREEN off
//		 setPWM(htim3, TIM_CHANNEL_2, 255, 255); 	//BLUE on
//
//	}
//
//	//spengo tutto
//	setPWM(htim3, TIM_CHANNEL_2, 255, 0); 	//BLUE oFF
//
//}
//
//void testBUZZER()
//{
//	//printf ("INIT TEST BUZZER\n ");
//	for (char count=0; count<3;count++)
//	{
//		setPWM(htim1, TIM_CHANNEL_3, 32767, 16383); // Buzzer On 2.2KHz
//
//		 //HAL_Delay(500);
//		osDelay (500);
//
//		stopPWM(htim1, TIM_CHANNEL_3); 	// Buzzer Off
//
//		 //HAL_Delay(500);
//		 osDelay (500);
//	}
//
//}
//
//void testHAPTIC()
//{
//	//printf ("INIT TEST HAPTIC\n ");
//	for (char count=0; count<3;count++)
//	{
//
//		 setPWM(htim3, TIM_CHANNEL_4, 32767, 16383); // MOTOR On 2.2KHz
//
//		 //HAL_Delay(500);
//		osDelay (500);
//
//		 stopPWM(htim3, TIM_CHANNEL_4);	// MOTOR Off
//
//		 //HAL_Delay(500);
//		osDelay (500);
//
//	}
//
//}
//
//boolean_t testFLASH ()
//{
//	//const uint8_t SIZE=10;
//	uint8_t pWBuffer[10] = {0,1,2,3,4,5,6,7,8,9};
//	uint8_t pRBuffer[10] = {0};
//
//	boolean_t retval = FALSE;
//
//	if (EFLASH_Init())
//	{
//		//if (EFLASH_EraseSector(0)) 	  	//erase 1° settore (4KB)
//		//if (EFLASH_EraseBlock(0))   		//erase 1° blocco (64KB)
//		if (EFLASH_EraseChip())      		//CHIP ERASE
//		{
//
//			if (EFLASH_Write(0x00, pWBuffer, sizeof(pWBuffer)))
//			{
//				if( EFLASH_Read(0x00, pRBuffer, sizeof(pRBuffer)) )
//				{
//					//verifica
//					for( char i = 0 ; i < 10 ; i++)
//					{
//						if (pRBuffer[i]!=pWBuffer[i])
//						{
//							return FALSE;
//						}
//
//					}
//
//					if (EFLASH_EraseSector(0)) 	  	//erase 1° settore (4KB)
//						//verifica ok
//						retval=TRUE;
//				}
//			}
//		}
//	}
//
//	return retval;
//}
//
//
//// testACC
//boolean_t testACC (ACC_Measure_t* acc)
//{
//	ISM_Measure_t measure={0,0,0,0,0,0,0};
//
//	boolean_t retval = FALSE;
//
//	//IMM_ACC_ODR_416_HZ,IMM_ACC_RANGE_16G,IMM_GYRO_ODR_416_HZ,IMM_GYRO_RANGE_245_DPS
//	// AccSensitivity:0, GyroSensitivity:0
//	if (ISM_Init())
//	{
//
//		for( char i = 0 ; i < 5 ; i++)
//		{
//			// valori in m/s^2
//			ISM_GetLastMeasure(&measure);
//
//			acc->AccX+=measure.AccX;
//			acc->AccY+=measure.AccY;
//			acc->AccZ+=measure.AccZ;
//
//			//HAL_Delay(100);
//			osDelay (100);
//		}
//
//		acc->AccX=acc->AccX/5;
//		acc->AccY=acc->AccY/5;
//		acc->AccZ=acc->AccZ/5;
//
//		//converto m/s^2->g
//		acc->AccX=acc->AccX/ISM_G_TO_MS2;
//		acc->AccY=acc->AccY/ISM_G_TO_MS2;
//		acc->AccZ=acc->AccZ/ISM_G_TO_MS2;
//
//		//verifica ok
//		retval=TRUE;
//
//		ISM_DeInit();
//	}
//
//	return retval;
//}
//
//// scrivo il codice seriale all'indirizzo 0 della Flash
////-ritorno vero se sono riuscito a scrivere e leggere il codice seriale
//boolean_t writeSerialCode(char* serialCode)
//{
//	//const uint8_t SIZE=10;
//	//uint8_t serialCodeRead[50] = {0};
//
//	boolean_t retval = FALSE;
//
//	if (EFLASH_Init())
//	{
//		if (EFLASH_EraseSector(0)) 	  //erase 1° settore (4KB)
//			//EFLASH_EraseBlock(0);   //erase 1° blocco (64KB)
//			//EFLASH_EraseChip();      //CHIP ERASE
//		{
//
//			if (EFLASH_Write(0x00, serialCode, sizeof(serialCode)))
//			{
//				//serialCode[0]=0;
//				if( EFLASH_Read(0x00, serialCode, sizeof(serialCode)) )
//					//verifica ok
//					retval=TRUE;
//			}
//		}
//	}
//
//	return retval;
//
//}
//
////U|CAN / U|CAN|OK
//boolean_t testCAN (CAN_HandleTypeDef* pHandl)
//{
//	static const  int TIMEOUT_CAN_TEST= 3000U;
//	uint32_t tickstart;
//	boolean_t retval = FALSE;
//
//	//ENABLE CAN TRANSCEIVER
//	 HAL_GPIO_WritePin(GPIOA, CAN_SHDN_Pin, GPIO_PIN_RESET);
//
//	//CAN GIà INIZIALIZZATO, CONFIGURATO FILTRO RX e AVVIATO
//
//	  /*##-5- Configure Transmission process #####################################*/
//	  TxHeader.StdId = 0x321;
//	  TxHeader.ExtId = 0x01;
//	  TxHeader.RTR = CAN_RTR_DATA;
//	  TxHeader.IDE = CAN_ID_STD;
//	  TxHeader.DLC = 4;
//	  TxHeader.TransmitGlobalTime = DISABLE;
//
//	  /* Set the data to be transmitted */
//	  TxData[0] = 'P';
//	  TxData[1] = 'I';
//	  TxData[2] = 'N';
//	  TxData[3] = 'G';
//
//	  /* Start the Transmission process */
////	  if (HAL_CAN_AddTxMessage(pHandl, &TxHeader, TxData, &TxMailbox) != HAL_OK)
////	  {
////		  /* Transmission request Error */
////		  //Error_Handler();
////		  return FALSE;
////	  }
//	  HAL_Delay(10);
//
//	  /* Get tick */
//	  tickstart = HAL_GetTick();
//
//	  while (1)
//	  {
//		  	  /* If Got RX message */
//		  	 if (HAL_CAN_GetRxMessage(pHandl, CAN_RX_FIFO0, &RxHeader, RxData) == HAL_OK)
//		  	 {
//		  		// risposta arrivata - verifico
//				 if ((RxHeader.StdId == 0x321) && (RxHeader.IDE == CAN_ID_STD) && (RxHeader.DLC == 4))
//				 {
//					 // se arriva un PING rispondo con PONG
//					 if ((RxData[0]=='P') && (RxData[1]=='I') && (RxData[2]=='N') && (RxData[3]=='G'))
//					 {
//
//						 /* Set the data to be transmitted */
//						  TxData[0] = 'P';
//						  TxData[1] = 'O';
//						  TxData[2] = 'N';
//						  TxData[3] = 'G';
//
//						  /* Start the Transmission process */
//						  if (HAL_CAN_AddTxMessage(pHandl, &TxHeader, TxData, &TxMailbox) != HAL_OK)
//						  {
//							  /* Transmission request Error */
//							  Error_Handler();
//							  //return FALSE;
//						  }
//
//						 retval=TRUE;
//					 }
//
//				 }
//		  	 }
//	  }
//
//
//	 return retval;
//}
//


//// test connessione ble
////ritorno vero se il beacon segnalato sia stato trovato entro il timeout  (invio RSSI individuato).
//boolean_t TestConnectBLE(char* mac, char* timeOut,   char* rssi)
//{
//	//const uint8_t SIZE=10;
//	//uint8_t serialCodeRead[50] = {0};
//
//	boolean_t retval = FALSE;
///*
//	short unsigned int addr[6];
//	bd_addr connect_addr;
//
//	if (sscanf(mac,"%02hx:%02hx:%02hx:%02hx:%02hx:%02hx",&addr[5],&addr[4],&addr[3],&addr[2],&addr[1],&addr[0]) == 6)
//	{
//		for (i = 0; i < 6; i++)
//		{
//			connect_addr.addr[i] = addr[i];
//		}
//		action = action_connect;
//	}
//	else
//		return 0;
//*/
//	//LANCIO IL TASK DEL BLE
//	//osThreadDef(BGLIB_RxHandlerTask, BGLIB_RxHandler, osPriorityNormal, 0, 512);
//	//app.bleTask.Handle = osThreadCreate(osThread(BGLIB_RxHandlerTask), NULL);
//
//	//init BLE
//	//TO-DO
//
//
//	//Avvio scansione/connessione
//	//TO-DO
//
//	//BGLIB_DBG_ScanStart();
//	//ble_cmd_gap_discover(gap_discover_observation);
//	//ble_cmd_gap_connect_direct(&connect_addr, gap_address_type_public, 40, 60, 100,0);
//
//	// Message loop
////	while (state != state_finish)
////	{
////		if (read_message(UART_TIMEOUT) > 0) break;
////	}
//
//	//BGLIB_DBG_ScanStop();
//
//
//	return retval;
//
//}
