/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

typedef uint8_t       	uint8;  /*  8 bits */
typedef uint16_t 		uint16; /* 16 bits */
typedef uint32_t   		uint32; /* 32 bits */

typedef int8_t          int8;   /*  8 bits */
typedef int16_t			int16;  /* 16 bits */
typedef int32_t			int32;  /* 32 bits */

typedef volatile int8       vint8;  /*  8 bits */
typedef volatile int16      vint16; /* 16 bits */
typedef volatile int32      vint32; /* 32 bits */

typedef volatile uint8      vuint8;  /*  8 bits */
typedef volatile uint16     vuint16; /* 16 bits */
typedef volatile uint32     vuint32; /* 32 bits */

typedef int					boolean_t;

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LLED_RED_Pin GPIO_PIN_13
#define LLED_RED_GPIO_Port GPIOC
#define LPC_WAKE_Pin GPIO_PIN_0
#define LPC_WAKE_GPIO_Port GPIOC
#define LLED_GREEN_Pin GPIO_PIN_1
#define LLED_GREEN_GPIO_Port GPIOC
#define BLE_RST_Pin GPIO_PIN_2
#define BLE_RST_GPIO_Port GPIOC
#define LLED_BLUE_Pin GPIO_PIN_3
#define LLED_BLUE_GPIO_Port GPIOC
#define LPC_INT_Pin GPIO_PIN_0
#define LPC_INT_GPIO_Port GPIOA
#define CAN_SHDN_Pin GPIO_PIN_2
#define CAN_SHDN_GPIO_Port GPIOA
#define DW1000_RST_Pin GPIO_PIN_3
#define DW1000_RST_GPIO_Port GPIOA
#define DW1000_CS_Pin GPIO_PIN_4
#define DW1000_CS_GPIO_Port GPIOA
#define DW1000_SCK_Pin GPIO_PIN_5
#define DW1000_SCK_GPIO_Port GPIOA
#define DW1000_MISO_Pin GPIO_PIN_6
#define DW1000_MISO_GPIO_Port GPIOA
#define DW1000_MOSI_Pin GPIO_PIN_7
#define DW1000_MOSI_GPIO_Port GPIOA
#define DW1000_WUP_Pin GPIO_PIN_4
#define DW1000_WUP_GPIO_Port GPIOC
#define DW1000_IRQ_Pin GPIO_PIN_5
#define DW1000_IRQ_GPIO_Port GPIOC
#define CHGB_VOLT_Pin GPIO_PIN_0
#define CHGB_VOLT_GPIO_Port GPIOB
#define FLASH_CS_Pin GPIO_PIN_2
#define FLASH_CS_GPIO_Port GPIOB
#define IMU_CS_SPI2_Pin GPIO_PIN_12
#define IMU_CS_SPI2_GPIO_Port GPIOB
#define IMU_SCK_SPI2_Pin GPIO_PIN_13
#define IMU_SCK_SPI2_GPIO_Port GPIOB
#define IMU_MISO_SPI2_Pin GPIO_PIN_14
#define IMU_MISO_SPI2_GPIO_Port GPIOB
#define IMU_MOSI_SPI2_Pin GPIO_PIN_15
#define IMU_MOSI_SPI2_GPIO_Port GPIOB
#define PWM_ERM_TIM3_Pin GPIO_PIN_9
#define PWM_ERM_TIM3_GPIO_Port GPIOC
#define POWER_5V_ENABLE_Pin GPIO_PIN_9
#define POWER_5V_ENABLE_GPIO_Port GPIOA
#define PWM_BUZZER_TIM1_Pin GPIO_PIN_10
#define PWM_BUZZER_TIM1_GPIO_Port GPIOA
#define RELAY_K1_Pin GPIO_PIN_11
#define RELAY_K1_GPIO_Port GPIOA
#define RELAY_K2_Pin GPIO_PIN_12
#define RELAY_K2_GPIO_Port GPIOA
#define DEBUG_UART_TX_Pin GPIO_PIN_12
#define DEBUG_UART_TX_GPIO_Port GPIOC
#define DEBUG_UART_RX_Pin GPIO_PIN_2
#define DEBUG_UART_RX_GPIO_Port GPIOD
#define FLED_RED_TIM2_Pin GPIO_PIN_3
#define FLED_RED_TIM2_GPIO_Port GPIOB
#define FLED_GREEN_TIM3_Pin GPIO_PIN_4
#define FLED_GREEN_TIM3_GPIO_Port GPIOB
#define FLED_BLUE_TIM3_Pin GPIO_PIN_5
#define FLED_BLUE_TIM3_GPIO_Port GPIOB
#define BLE_IRQ_Pin GPIO_PIN_6
#define BLE_IRQ_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

#define UART4_RESET_Pin 		GPIO_PIN_2
#define UART4_RESET_GPIO_Port 	GPIOC
#define BLE_WAKE_Pin GPIO_PIN_6
#define BLE_WAKE_GPIO_Port GPIOB

#define DW1000_IRQ_IRQn            EXTI9_5_IRQn
#define DW1000_RST_IRQn            EXTI3_IRQn

/* operating mode */
#define NODEFUNCTION // The tag only responds to the poll
#define SWODEBUG	// abilta la printf su SWO e disabiluta il PWM TIM2 (FLEDRED)

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
