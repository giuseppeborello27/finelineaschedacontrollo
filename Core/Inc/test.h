#ifndef __TEST_H
#define __TEST_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx_hal.h"
#include <stdbool.h>

void CAN_RxHandler(void * arguments);

//enum CMD
//{
//
//		NOCOMD=0,
//
//		UWB,				//TO DO - U|UWB|TGT_ID
//		FLASH_M, 		//U|FLASH
//		ACC, 			//U|ACC
//		BLE, 				//TO DO - U|BLE|MAC|TO
//		SLEEP, 				//TO DO
//		SWITCHOFF, 			//TO DO
//		MOTION, 			//TO DO
//		SERIAL, 		//U|SERIAL|CODICE_SERIALE
//
//	#ifdef TAG_PERSONA
//		CHARGER,    		//TO DO
//		BUTTON1,  		//U|BUTTON|1
//		BUTTON2,  		//U|BUTTON|2
//		ANTITAMPER, 	//U|ANTITAMPER|OK
//		BUZZER, 		//U|BUZZER
//		HAPTIC,			//U|HAPTIC
//		MAINLED,		//U|MAINLED
//		LATLED 			//U|LATLED
//	#endif
//
//	#ifdef TAG_CARRELLO
//		CAN_BUS, 			//TO DO - U|CAN - U|CAN|OK
//		R  				//U|R|1 - U|R|2
//	#endif
//
//};

#endif /* __TEST_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
