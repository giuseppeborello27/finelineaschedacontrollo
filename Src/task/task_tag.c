/*!----------------------------------------------------------------------------
 * @file    task_tag.c
 * @brief   DecaWave Application Layer
 *          RTOS tag implementation
 *
 * @attention
 *
 * Copyright 2016-2017 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author
 *
 * DecaWave
 */

#include <util.h>
#include <circ_buf.h>
#include <task_tag.h>
#include "2ta.h"
#include "frameConfiguration.h"
#include "randomSlotAssignment.h"
#include "superFrameCounter.h"
#include "main.h" // solo per LLED
#include "driver_exclusion.h"
//#include <testDefines.h>
//-----------------------------------------------------------------------------

#define BLINK_PERIOD_MS            (500)    /* range init phase - blink sends period, ms */


#define DANGERAREA  12 	// area di pericolo in metri
#define HYSTERESIS	1	// metri
#define ALARMZONE	6 	// zona di allarme in metri




extern void send_to_pc_tag_location(twr_res_ext_t*);
extern float dwt_getrangebias(uint8 chan, float range, uint8 prf);
//-----------------------------------------------------------------------------

/* @brief    The software timer.
 *  It is signalling to the TwrBlinkTask to transmit a blink.
 *  Will be stopped on reception of Ranging Config response from a Node
 * */
//static void
//BlinkTimer_cb(void const *arg)
//{
//    if(app.blinkTask.Handle) //RTOS : blinkTask can be not started yet
//    {
//        if(osSignalSet(app.blinkTask.Handle, app.blinkTask.Signal) != osOK)
//        {
//            error_handler(1, _Err_Signal_Bad);
//        }
//    }
//    UNUSED(arg);
//}
//
//
///*
// * @brief
// *     The thread is initiating the transmission of the blink
// *     on reception of app.blinkTask.Signal
// *
// * */
//
//
//static void
//BlinkTask(void const * arg)
//{
//    twr_info_t     *p;
//
//    do{
//        osDelay(100);
//    }while(!(p = getTwrInfoPtr()));    //wait for initialisation of pTwrInfo
//
//
//    do {
//        osMutexRelease(app.blinkTask.MutexId);
//
//        osSignalWait(app.blinkTask.Signal, osWaitForever);
//
//        osMutexWait(app.blinkTask.MutexId, 0);    //we do not want the task can be deleted in the middle of operation
//
//        taskENTER_CRITICAL();
//        tag_wakeup_dw1000_blink_poll(p);
//        taskEXIT_CRITICAL();
//
//        initiator_send_blink(p);
//
//    }while(1);
//
//    UNUSED(arg);
//}
void TagActionTask(void const * arg)
{
	do
	{
		osMutexRelease(app.actionTask.MutexId);
		osSignalWait(app.actionTask.Signal, osWaitForever);
		osMutexWait(app.actionTask.MutexId, 0);

		taskENTER_CRITICAL();
		uint8_t AlarmTags = getAlarmActionAllTags(); //restituisce 1 se un solo tag si trova nello stato di Alarm, altrimenti 0
		uint8_t dangerTags = getDangerActionAllTags();//restituisce 1 se un solo tag si trova nello stato di danger, altrimenti 0

		taskEXIT_CRITICAL();

//		printf("AlarmTags %d \r\n",AlarmTags);
//		printf("dangerTags %d \r\n",dangerTags);
//		if (AlarmTags)
//		{
//			HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin,GPIO_PIN_RESET);
//			//HAL_GPIO_WritePin(RELAY_K1_GPIO_Port, RELAY_K1_Pin, GPIO_PIN_SET); // ON RELAY 1
//		}
//		else
//		{
//			HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin,GPIO_PIN_SET);
//			//HAL_GPIO_WritePin(RELAY_K1_GPIO_Port, RELAY_K1_Pin, GPIO_PIN_RESET);	// OFF RELAY 1
//		}
//		if (dangerTags)
//		{
//			HAL_GPIO_WritePin(LLED_GREEN_GPIO_Port, LLED_GREEN_Pin,GPIO_PIN_RESET);
//			//HAL_GPIO_WritePin(RELAY_K2_GPIO_Port, RELAY_K2_Pin, GPIO_PIN_SET); // ON RELAY 2
//		}
//		else
//		{
//			HAL_GPIO_WritePin(LLED_GREEN_GPIO_Port, LLED_GREEN_Pin,GPIO_PIN_SET);
//			//HAL_GPIO_WritePin(RELAY_K2_GPIO_Port, RELAY_K2_Pin, GPIO_PIN_RESET); // OFF RELAY 2
//		}
		osDelay(5);




	   	osThreadYield();
	}
	while(1);

	UNUSED(arg);
}



/*
 * @brief
 *     The thread is initiating the TWR sequence
 *  on reception of .signal.twrTxPoll
 *
 * */

void TagPollTask(void const * arg)
{    

    const frameConfig * frameConfiguration = GetFrameConfiguration();

    targetsContainer targetsContainer = Init2Ta(frameConfiguration);
    randomSlotsResult randomTau_ns;
    int32_t nextWakeUpPeriod_ns = 0;



    do {
//      uint32_t tStartFrame = getCurrentTime_ticks();
//
//      randomTau_ns = GetTwoRandomPollsTimes();
//
//      nextWakeUpPeriod_ns = randomTau_ns.first_of_two_tau_ns;
//
      Twota_waitFor(false, nextWakeUpPeriod_ns);
//
//      // first poll
//      Twr_with_target(&targetsContainer.firstTargetInfo);

      // WaitForFinishingComunication();
      // nextWakeUpPeriod_ns = 5000000; // 5ms
      // Twota_waitFor(false, nextWakeUpPeriod_ns);
    	osDelay(5);

      // printf("b \r\n");
      //  second poll
//      Twr_with_target(&targetsContainer.firstTargetInfo);
      // WaitForFinishingComunication();
      //  nextWakeUpPeriod_ns = 1000000; // 1ms
      // Twota_waitFor(false, nextWakeUpPeriod_ns);
//      osDelay(5);


       // third poll
//      Twr_with_target(&targetsContainer.firstTargetInfo);
//      osDelay(1);


      //  // fourth poll
      // Twr_with_target(&targetsContainer.firstTargetInfo);
//       osDelay(1);

      //  // fourth poll
      // Twr_with_target(&targetsContainer.firstTargetInfo);
      //  osSignalWait(2, osWaitForever);
      //  WaitForFinishingComunication();
      //  nextWakeUpPeriod_ns = 1000000;
      // Twota_waitFor(false, nextWakeUpPeriod_ns);

      //  Twr_with_target(&targetsContainer.firstTargetInfo);
      //  WaitForFinishingComunication();

//      HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin,GPIO_PIN_SET);

      //init_knownTagList();
      // printf("dange%d \r\n",dangerZone);
      // printf("appr %d \r\n",approachTime);


      // printf("approachTime %d \r\n",approachTime);
      // printf("withdrawalTime %d \r\n",withdrawalTime);


//      dangerZone = false;
//      deleteDList();
//
//      if (!receptionPoll)
//      nonReceptionTimeout = updateNonReceptionTimeout(nonReceptionTimeout);
//
//      receptionPoll = false;
////
////      // int32_t nextWakeUpPeriod_ns = 100000000;
//      nextWakeUpPeriod_ns = getRemainingSuperFrameTime(tStartFrame);
//
//
//      printf("REM %d \r\n",nextWakeUpPeriod_ns);
//       printf("c \r\n");


//      Twota_waitFor(true, nextWakeUpPeriod_ns);

      

      //printf("nextWakeUpPeriod_ns after:%d\r\n",nextWakeUpPeriod_ns);
      
      // count++;
      // if (count >= 1200) {
      //   while(1) {
      //     __NOP();
      //   }
      // }

    	osThreadYield();
    }while(1);

    UNUSED(arg);
}


/* @brief DW1000 RX : RTOS implementation
 *
 * */
static void
TagRxTask(void const * arg)
{
    error_e     ret;
    uint16_t    head, tail;

    twr_info_t  *ptwrInfo;


    do{
    	osDelay(10);
    }while(!(ptwrInfo = getTwrInfoPtr()));    //wait for initialisation of pTwrInfo

    int size = sizeof(ptwrInfo->rxPcktBuf.buf) / sizeof(ptwrInfo->rxPcktBuf.buf[0]);

    driverExclusionInit();

    printf("Init\n");

#ifdef NODEFUNCTION
    taskENTER_CRITICAL();
    /* Clear reception timeout to start next ranging process. */
    dwt_setrxtimeout(0);
    /* Activate reception immediately. */
    dwt_rxenable(DWT_START_RX_IMMEDIATE);
    taskEXIT_CRITICAL();
#endif
    /*-------------------------------------------------------------------------*/
    /* *List = Lista delle tag che si trovano all'interno dell'area di pericolo*/
    /*-------------------------------------------------------------------------*/
    initList();

    do {

        osMutexRelease(app.rxTask.MutexId);
        osSignalWait(app.rxTask.Signal, osWaitForever);
        osMutexWait(app.rxTask.MutexId, 0);

        taskENTER_CRITICAL();
        head = ptwrInfo->rxPcktBuf.head;
        tail = ptwrInfo->rxPcktBuf.tail;
        taskEXIT_CRITICAL();

        /* We are using circular buffer + Signal to safely deliver packets from ISR to APP */
        if(CIRC_CNT(head,tail,size) > 0)
        {
            rx_pckt_t       *prxPckt    = &ptwrInfo->rxPcktBuf.buf[tail];
            twr_res_ext_t   p;
            twr_res_p2p_t res;

            driverExclusionCheck(prxPckt);

            ret = twr_initiator_algorithm_rx(prxPckt, ptwrInfo); /**< Run bare twr_initiator_algorithm */

            taskENTER_CRITICAL();
            tail = (tail + 1) & (size-1);
            ptwrInfo->rxPcktBuf.tail = tail;
            taskEXIT_CRITICAL();


            if((app.pConfig->s.reportLevel) && (ret == _No_Err_Final))
            {
                int32_t tofi;
                int64_t     Rb, Da, Ra, Db ;

//                final_accel_t    *pFinalMsg;
                final_msg_accel_t	*pFinalMsg;

                pFinalMsg = &prxPckt->msg.finalMsg;

                uint64_t    tagPollTxTime;
                uint64_t    tagRespRxTime;
                uint64_t    tagFinalTxTime;

                uint64_t    nodeRespTxTime;
                uint64_t    nodePollRxTime;
                uint64_t    nodeFinalRxTime;

                float    RaRbxDaDb = 0;
                float    RbyDb = 0;
                float    RayDa = 0;

                TS2U64_MEMCPY(tagPollTxTime, pFinalMsg->final.pollTx_ts);
                TS2U64_MEMCPY(tagRespRxTime, pFinalMsg->final.responseRx_ts);
                TS2U64_MEMCPY(tagFinalTxTime, pFinalMsg->final.finalTx_ts);
                // printf("tagPollTxTime %d \r\n",(int32_t)tagPollTxTime);
                // printf("tagRespRxTime %d \r\n",(int32_t)tagRespRxTime);
                // printf("tagFinalTxTime %d \r\n",(int32_t)tagFinalTxTime);
                TS2U64_MEMCPY(nodePollRxTime, prxPckt->nodePollRx_ts);
                TS2U64_MEMCPY(nodeRespTxTime, prxPckt->nodeRespTx_ts);
                TS2U64_MEMCPY(nodeFinalRxTime, prxPckt->rxTimeStamp);
                // printf("nodePollRxTime %d \r\n",(int32_t)nodePollRxTime);
                // printf("nodeRespTxTime %d \r\n",(int32_t)nodeRespTxTime);
                // printf("nodeFinalRxTime %d \r\n",(int32_t)nodeFinalRxTime);

                Ra = (int64_t)((tagRespRxTime - tagPollTxTime) & MASK_40BIT);
                Db = (int64_t)((nodeRespTxTime - nodePollRxTime) & MASK_40BIT);

                Rb = (int64_t)((nodeFinalRxTime - nodeRespTxTime) & MASK_40BIT);
                Da = (int64_t)((tagFinalTxTime - tagRespRxTime) & MASK_40BIT);

                RaRbxDaDb = (((float)Ra))*(((float)Rb)) - (((float)Da))*(((float)Db));

                RbyDb = ((float)Rb + (float)Db);

                RayDa = ((float)Ra + (float)Da);

                tofi = (int32) ( RaRbxDaDb/(RbyDb + RayDa) );
                if(tofi <0)
                {
                	tofi = -tofi;
                }
                float range_m  = (float)tofi* (SPEED_OF_LIGHT/499.2e6f/128.0f);

                float dist_to_correct = range_m;
                dist_to_correct /= 1.51f;
                range_m = range_m - dwt_getrangebias(5, dist_to_correct, DWT_PRF_64M);



                uint16_t  currentTagId  = AR2U16(((final_msg_accel_t*)pFinalMsg)->mac.sourceAddr);
//                printf("addr16 %d \r\n",addr16);
                uint64_t        currentTagId64 = (uint64_t)currentTagId ;

                //driver exclusion update distance of the measured tag
                setTagDistance(range_m);
                //if the excluded tag is far then switch to normal mode
                if((getReceivedTagAddress() == getPedestrianTagAddress()) &&
                		(getTagStatus() == status_driver_exclusion_mode) &&
						(getTagDistance() > getDistanzaSgancio()))
                {
                	setTagStatus(status_normal_mode);
                	setPedestrianTagAddress(NO_TAG);
                	//printf("1\n");
                }

				if(range_m < 2000.0f) //Distanza valida
				{
					uint64_t tagDlist = get_tag64_from_DList(currentTagId64); // verifico se è presente nella lista Danger List
					uint64_t tagAlist = get_tag64_from_AList(currentTagId64); // verifico se è presente nella lista Alarm List


					//printf("Status %d\t Pedestrian %d\t Received %d\n", getTagStatus(), getPedestrianTagAddress(),getReceivedTagAddress());
					//If the tag is excluded then ignore the danger areas
					if((getPedestrianTagAddress() != getReceivedTagAddress()) || (getTagStatus() != status_driver_exclusion_mode))
					{



						if(!tagDlist && !tagAlist)
						{ // se non è presente nelle liste
						//					  printf("non è presente nella lista\r\n");
							if(range_m < DANGERAREA) // se la tag si trova all'interno dell'area di pericolo
							{
							//						  printf("si trova all'interno dell'area di pericolo\r\n");
							//
								if (range_m <= ALARMZONE)
								{
								//							printf("allarme aggiunto alla lista \r\n");
									// se si trova all'interno dell'area di allarme rallento il veicolo
									taskENTER_CRITICAL();
									deleteTagToList(currentTagId64,Dlist);		// Elimino la tag dalla Danger Lista
									addTagToList(currentTagId64,Alist); 		// Aggiungo la tag alla Alarm list
									addActiontoTagId(currentTagId64, ACTION_ALARMAREA, Alist);
									taskEXIT_CRITICAL();
									//HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin,GPIO_PIN_RESET);
								}
								else
								{	//altrimenti è nellarea di pericolo
								//							printf("segnalazione  aggiunto \r\n");
									taskENTER_CRITICAL();
								//							printf("ndanger  \r\n");
									deleteTagToList(currentTagId64,Alist);	// Elimino la tag dalla Alarm Lista
									addTagToList(currentTagId64,Dlist); 		// Aggiungo la tag alla Danger list
									addActiontoTagId(currentTagId64, ACTION_DANGERAREA, Dlist);
									taskEXIT_CRITICAL();
								//							HAL_GPIO_WritePin(LLED_GREEN_GPIO_Port, LLED_GREEN_Pin,GPIO_PIN_RESET);
								}

							}
							else	// se la tag si trova fuori dell'area di pericolo
							{
							//						  printf("sfuori dell'area di pericolo \r\n");
							}

						}
						else if (tagDlist)
						{// se è presente nella danger lista
							//					  printf("presente nella lista\r\n");
							if(range_m <= ALARMZONE) // la tag è entrata nella Alarm zone
							{
								//						  printf("dangToAlar  \r\n");
								deleteTagToList(currentTagId64,Dlist);		// Elimino la tag dalla Danger Lista
								addTagToList(currentTagId64,Alist); 		// Aggiungo la tag alla Alarm list
								addActiontoTagId(currentTagId64, ACTION_ALARMAREA, Alist);

							}
							//						  printf("si trova all'interno dell'area di pericolo\r\n");
							else if (range_m > ALARMZONE && range_m < DANGERAREA+HYSTERESIS) // la tag si trova all'interno della danger zone
							{
								//							printf("danger  \r\n");
								//							taskENTER_CRITICAL();
								// se si trova all'interno dell'area di allarme rallento il veicolo
								addActiontoTagId(currentTagId64, ACTION_DANGERAREA, Dlist);
								//							taskEXIT_CRITICAL();
								//							HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin,GPIO_PIN_RESET);
							}

						}
						else if (tagAlist)
						{

							if(range_m <= ALARMZONE+HYSTERESIS) // la tag si è nella Alarm zone
							{
							  //						  printf("alarm  \r\n");
								addActiontoTagId(currentTagId64, ACTION_ALARMAREA, Alist);

							}
							else if (range_m > ALARMZONE+HYSTERESIS && range_m < DANGERAREA) // La tag si è spostata nella danger zone
							{
								//						  printf("alarToDand  \r\n");
								deleteTagToList(currentTagId64,Alist);	// Elimino la tag dalla Alarm Lista
								addTagToList(currentTagId64,Dlist); 		// Aggiungo la tag alla Danger list
								addActiontoTagId(currentTagId64, ACTION_DANGERAREA, Dlist);
							}

						}

					}

							//                    uint16_t dist_cm = range_m*100.0;
							//                    printf("range %d \r\n",dist_cm);
				}
				else
				{
				  ret =  _Err_Range_Calculation;
				  //                  HAL_GPIO_TogglePin(LLED_RED_GPIO_Port, LLED_RED_Pin );
				}

            }



            /* ready to serve next raw reception */
            if( (ret != _NO_ERR) )
            {
                /* If the Node is performing a Tx, then the receiver will be enabled
                 * after the Tx automatically and twr_responder_algorithm_rx reports "_NO_ERR".
                 *
                 * Otherwise always re-enable the receiver : unknown frame, not expected tag,
                 * final message, _Err_DelayedTX_Late, etc.
                 * */
//            	HAL_GPIO_TogglePin(LLED_RED_GPIO_Port, LLED_RED_Pin);
                taskENTER_CRITICAL();
                /* Clear reception timeout to start next ranging process. */
                    dwt_setrxtimeout(0);
                    /* Activate reception immediately. */
                    dwt_rxenable(DWT_START_RX_IMMEDIATE);
                taskEXIT_CRITICAL();

            }


            // /* Report previous range/coordinates/offset back to UART/USB */
            // if((app.pConfig->s.reportLevel) && (ret == _No_Err_Response))
            // {
            //     send_to_pc_tag_location(&p);
            // }
        }

        osThreadYield();
    }while(1);

    UNUSED(arg);
}


/* @brief Setup TWR tasks and timers for discovery phase.
 *         - blinking timer
 *         - blinking task
 *          - twr polling task
 *         - rx task
 * Only setup, do not start.
 * */
static void tag_setup_tasks(void)
{


    /* This will be the main poll thread for the Tag after discovery completed
     * Do not reduce the stack size for this thread.
     * */
    osThreadDef(pollTask, TagPollTask, osPriorityAboveNormal, 0, 128);
    osMutexDef(pollMutex);

    osThreadDef(actionTask, TagActionTask, osPriorityAboveNormal, 0, 128);
    osMutexDef(actionMutex);

    /* rxThread is passing signal from RX IRQ to an actual two-way ranging algorithm.
     * It awaiting of Rx Signal from RX IRQ ISR and decides what to do next in TWR exchange process.
     * Do not reduce the stack size for this thread.
     * */
    osThreadDef(rxTask, TagRxTask, osPriorityRealtime, 0, 384);
    osMutexDef(rxMutex);



    app.pollTask.Handle     = osThreadCreate(osThread(pollTask), NULL);
    app.pollTask.MutexId    = osMutexCreate(osMutex(pollMutex));

    app.actionTask.Handle     = osThreadCreate(osThread(actionTask), NULL);
    app.actionTask.MutexId    = osMutexCreate(osMutex(actionMutex));

    app.rxTask.Handle       = osThreadCreate(osThread(rxTask), NULL);
    app.rxTask.MutexId      = osMutexCreate(osMutex(rxMutex));

    if( \
    	(app.pollTask.Handle == NULL)   ||\
        (app.rxTask.Handle == NULL)   /*||\
        (app.imuTask.Handle == NULL)*/
        )
    {
        error_handler(1, _Err_Create_Task_Bad);
    }
}

//-----------------------------------------------------------------------------

/* @brief
 *      Stop and delete blink timer and blink timer Task
 * */
void blink_timer_delete(void)
{
    taskENTER_CRITICAL();

    if(app.blinkTmr.Handle)
    {
        osTimerStop(app.blinkTmr.Handle);

        if (osTimerDelete(app.blinkTmr.Handle) == osOK)
        {
            app.blinkTmr.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Timer);
        }
    }

    taskEXIT_CRITICAL();
}

void blink_task_delete(void)
{
    if(app.blinkTask.Handle)
    {
        osMutexWait(app.blinkTask.MutexId, osWaitForever);
        taskENTER_CRITICAL();
        osMutexRelease(app.blinkTask.MutexId);
        if(osThreadTerminate(app.blinkTask.Handle) == osOK)
        {
            osMutexDelete(app.blinkTask.MutexId);
            app.blinkTask.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Task);
        }
        taskEXIT_CRITICAL();
    }
}

//-----------------------------------------------------------------------------

/* @brief
 *      Kill all task and timers related to TWR if any
 *      DW1000's RX and IRQ shall be switched off before task termination,
 *      that IRQ will not produce unexpected Signal
 * */
void tag_terminate_tasks(void)
{
    HAL_RTCEx_DeactivateWakeUpTimer(&hrtc);

    blink_timer_delete();

    blink_task_delete();

    if(app.imuTask.Handle)
    {
        osMutexWait(app.imuTask.MutexId, osWaitForever);
        taskENTER_CRITICAL();
        osMutexRelease(app.imuTask.MutexId);
        if(osThreadTerminate(app.imuTask.Handle) == osOK)
        {
            osMutexDelete(app.imuTask.MutexId);
            app.imuTask.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Task);
        }
        taskEXIT_CRITICAL();
    }

    if(app.rxTask.Handle)
    {
        osMutexWait(app.rxTask.MutexId, osWaitForever);
        taskENTER_CRITICAL();
        osMutexRelease(app.rxTask.MutexId);
        if(osThreadTerminate(app.rxTask.Handle) == osOK)
        {
            osMutexDelete(app.rxTask.MutexId);
            app.rxTask.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Task);
        }
        taskEXIT_CRITICAL();
    }

    if(app.pollTask.Handle)
    {
        osMutexWait(app.pollTask.MutexId, osWaitForever);
        taskENTER_CRITICAL();
        osMutexRelease(app.pollTask.MutexId);
        if(osThreadTerminate(app.pollTask.Handle) == osOK)
        {
            osMutexDelete(app.pollTask.MutexId);
            app.pollTask.Handle = NULL;
        }
        else
        {
            error_handler(1, _ERR_Cannot_Delete_Task);
        }
        taskEXIT_CRITICAL();
    }

    tag_process_terminate();    //de-allocate Tag RAM Resources
}


/* @fn         tag_helper
 * @brief     this is a service function which starts the Tag
 * top-level application
 *
 * */
void tag_helper(void const *argument)
{
    error_e     tmp;

    port_disable_wake_init_dw();

    taskENTER_CRITICAL();    /**< When the app will setup RTOS tasks, then if task has a higher priority,
                                 the kernel will start it immediately, thus we need to stop the scheduler.*/

    /* "RTOS-independent" part : initialisation of two-way ranging process */
    tmp = tag_process_init();

    if( tmp != _NO_ERR)
    {
        error_handler(1, tmp);
    }

    tag_setup_tasks();        /**< "RTOS-based" : setup all RTOS tasks. */

    HAL_Delay(100); //Start Task

    tag_process_start();


    taskEXIT_CRITICAL();    /**< all RTOS tasks can be scheduled */

}


