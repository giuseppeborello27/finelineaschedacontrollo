/*
 *  @file    task_flush.h
 *  @brief
 *
 * @attention
 *
 * Copyright 2017 (c) Decawave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author Decawave
 */

#ifndef __FLUSH_TASK__H__
#define __FLUSH_TASK__H__ 1

#ifdef __cplusplus
 extern "C" {
#endif

#include "port.h"

#define FLUSH_PERIOD_DEFAULT_MS    50

void FlushTask(void const * argument);
void reset_FlushTask(void);
void set_FlushPeriod(int ms);

#ifdef __cplusplus
}
#endif

#endif /* __FLUSH_TASK__H__ */
