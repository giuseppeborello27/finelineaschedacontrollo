/*
 * driver_exclusion.h
 *
 *  Created on: 28 ott 2020
 *      Author: Giovambattista Astorino
 */

#ifndef INC_DRIVER_EXCLUSION_H_
#define INC_DRIVER_EXCLUSION_H_

#include "port.h"
#include "tag.h"
#include "util.h"
#include <stdio.h>

#define DRIVER_EXCLUSION_TIMEOUT	(3000) //ms //default value TODO read from app

#define DISENGAGE_DISTANCE		(3.0) //m //default value TODO read from app

#define EXCLUSION_DISTANCE		(1.0) //m //default value TODO read from app

#define NO_TAG					(0)

typedef enum {
    status_normal_mode,             //tag is in normale mode
    status_driver_exclusion_mode,    //tag is excluded
}tagStatus;

typedef struct
{
	tagStatus tag_status;
	uint16_t pedestrian_tag_address; //excluded tag address
	uint16_t received_tag_address;
	uint32_t last_received_message;
	float tag_distance;

} driverExclusionData;


typedef struct
{
	uint32_t timeout_esclusione;
	float distanza_di_sgancio;
	float distanza_di_esclusione;

} configurableParameter;


//initialization function
void driverExclusionInit();

//driver exclusion handler
void driverExclusionCheck(rx_pckt_t *prxPckt);


tagStatus getTagStatus();
void setTagStatus(tagStatus input);

uint16_t getPedestrianTagAddress();
void setPedestrianTagAddress(uint16_t input);

uint16_t getReceivedTagAddress();
void setReceivedTagAddress(uint16_t input);

uint32_t getLastReceivedMessage();
void setLastReceivedMessage(uint32_t input);

uint32_t getTimeoutEsclusione();
void setTimeoutEsclusione(uint32_t input);

float getDistanzaSgancio();
void setDistanzaSgancio(float input);

float getDistanzaEsclusione();
void setDistanzaEsclusione(float input);

float getTagDistance();
void setTagDistance(float input);


#endif /* INC_DRIVER_EXCLUSION_H_ */
