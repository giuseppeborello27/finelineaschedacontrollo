/*
 * driver_exclusion.c
 *
 *  Created on: 28 ott 2020
 *      Author: Giovambattista Astorino
 */


#include "driver_exclusion.h"
#include "cmsis_os.h"
#include "stdio.h"

static driverExclusionData driver_exclusion_data;
static configurableParameter configurable_parameter;

//initialization function
void driverExclusionInit()
{
	driver_exclusion_data.tag_status = status_normal_mode;
	driver_exclusion_data.pedestrian_tag_address = 0;
	driver_exclusion_data.last_received_message = 0;
	driver_exclusion_data.tag_distance = 0.0;
	configurable_parameter.distanza_di_esclusione = EXCLUSION_DISTANCE;
	configurable_parameter.distanza_di_sgancio = DISENGAGE_DISTANCE;
	configurable_parameter.timeout_esclusione = DRIVER_EXCLUSION_TIMEOUT;

}


void driverExclusionCheck(rx_pckt_t *prxPckt)
{
    fcode_e fcode = ((std_msg_ss_t*)(&prxPckt->msg.stdMsg))->messageData[0];
    //uint16_t destAddress = AR2U16(((resp_pdoa_msg_t*)&(prxPckt->msg.stdMsg))->mac.destAddr);
    uint16_t destAddress = AR2U16(((resp_pdoa_msg_t*)&(prxPckt->msg.stdMsg))->mac.sourceAddr);
    //uint16_t destAddress = AR2U16(((poll_msg_t*)&(pTwrInfo->rxPcktBuf.buf[tail].msg.stdMsg))->mac.sourceAddr);

    setReceivedTagAddress(destAddress);


    //if there are no exclusion message from tag for X seconds
    //then switch to normal mode
    if(((xTaskGetTickCount() - getLastReceivedMessage())> getTimeoutEsclusione()) &&
    		(getTagStatus() == status_driver_exclusion_mode))
    {
    	setTagStatus(status_normal_mode);
    	setPedestrianTagAddress(NO_TAG);
    }


	//If it is an exclusion message or poll
	if((fcode == Driver_exclusion_fcode) || (fcode == Poll_driver_exclusion))
	{
		//printf("Fcode %d from Address %d\n", fcode, destAddress);

		//Message is from excluded pedestrian tag
		if(destAddress == getPedestrianTagAddress())
		{
			//printf("Exclusion from excluded\n");

			setLastReceivedMessage(xTaskGetTickCount());
		}
		else
		{//message is not from excluded pedestrian tag

			//printf("Exclusion from not excluded\n");

			//There are no excluded pedestrian tag
			if(getPedestrianTagAddress() == NO_TAG)
			{
				//printf("No excluded tag");
				//if the tag is in the exclusion area
				if(getTagDistance() < getDistanzaEsclusione())
				{
					printf("New excluded tag is current tag");
					//add tag to excluded tag and update timestamp
					setTagStatus(status_driver_exclusion_mode);
					setLastReceivedMessage(xTaskGetTickCount());
					setPedestrianTagAddress(destAddress);

				}

			}
			else
			{
				//printf("Excluded tag != current tag");
			}
		}

	}


}

tagStatus getTagStatus()
{
	taskENTER_CRITICAL();
	tagStatus result = driver_exclusion_data.tag_status;
	taskEXIT_CRITICAL();

	return result;

}

void setTagStatus(tagStatus input)
{
	taskENTER_CRITICAL();
	driver_exclusion_data.tag_status = input;
	taskEXIT_CRITICAL();
}


uint16_t getPedestrianTagAddress()
{
	taskENTER_CRITICAL();
	uint16_t result = driver_exclusion_data.pedestrian_tag_address;
	taskEXIT_CRITICAL();

	return result;

}

void setPedestrianTagAddress(uint16_t input)
{
	taskENTER_CRITICAL();
	driver_exclusion_data.pedestrian_tag_address = input;
	taskEXIT_CRITICAL();
}


uint16_t getReceivedTagAddress()
{
	taskENTER_CRITICAL();
	uint16_t result = driver_exclusion_data.received_tag_address;
	taskEXIT_CRITICAL();

	return result;

}

void setReceivedTagAddress(uint16_t input)
{
	taskENTER_CRITICAL();
	driver_exclusion_data.received_tag_address = input;
	taskEXIT_CRITICAL();
}


uint32_t getLastReceivedMessage()
{
	taskENTER_CRITICAL();
	uint32_t result = driver_exclusion_data.last_received_message;
	taskEXIT_CRITICAL();

	return result;

}

void setLastReceivedMessage(uint32_t input)
{
	taskENTER_CRITICAL();
	driver_exclusion_data.last_received_message = input;
	taskEXIT_CRITICAL();
}


uint32_t getTimeoutEsclusione()
{
	taskENTER_CRITICAL();
	uint32_t result = configurable_parameter.timeout_esclusione;
	taskEXIT_CRITICAL();

	return result;

}

void setTimeoutEsclusione(uint32_t input)
{
	taskENTER_CRITICAL();
	configurable_parameter.timeout_esclusione = input;
	taskEXIT_CRITICAL();
}


float getDistanzaSgancio()
{
	taskENTER_CRITICAL();
	float result = configurable_parameter.distanza_di_sgancio;
	taskEXIT_CRITICAL();

	return result;

}

void setDistanzaSgancio(float input)
{
	taskENTER_CRITICAL();
	configurable_parameter.distanza_di_sgancio = input;
	taskEXIT_CRITICAL();
}


float getDistanzaEsclusione()
{
	taskENTER_CRITICAL();
	float result = configurable_parameter.timeout_esclusione;
	taskEXIT_CRITICAL();

	return result;
}

void setDistanzaEsclusione(float input)
{
	taskENTER_CRITICAL();
	configurable_parameter.timeout_esclusione = input;
	taskEXIT_CRITICAL();
}

float getTagDistance()
{
	taskENTER_CRITICAL();
	float result = driver_exclusion_data.tag_distance;
	taskEXIT_CRITICAL();

	return result;
}

void setTagDistance(float input)
{
	taskENTER_CRITICAL();
	driver_exclusion_data.tag_distance = input;
	taskEXIT_CRITICAL();
}

