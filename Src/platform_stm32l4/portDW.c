/*
 * @file portDW.c
 *
 * @brief  Application platform-dependent functions for Decawave application are collected here
 * @author Decawave
 *
 * @attention Copyright 2016 (c) DecaWave Ltd, Dublin, Ireland.
 *            All rights reserved.
 *
 */
#include <port.h>
#include "main.h"

#define USART_RX_DMA_IRQn    DMA1_Channel5_IRQn

/**/
#define LED_ERROR_Port  LLED_RED_GPIO_Port
#define LED_ERROR_Pin   LLED_RED_Pin

/**/
static volatile uint32_t signalResetDone;    /**< this is bare-metal signal */

//-----------------------------------------------------------------------------
/*
 * The standard malloc/free used in sscanf/sprintf
 * we want them to be replaced with FreeRTOS's implementation
 * */
//_PTR _calloc_r(struct _reent *re, size_t num, size_t size) {
//    return pvPortMalloc(num*size);
//}
//
//_PTR _malloc_r(struct _reent *re, size_t size) {
//    return pvPortMalloc(size);
//}
//
//_VOID _free_r(struct _reent *re, _PTR ptr) {
//    vPortFree(ptr);
//}


/****************************************************************************//**
 *
 *                                 UART RX section
 *
 *******************************************************************************/

/* @fn      port_uart_rx_init()
 * @brief   UART needs to know how many character needs to be received.
 * 			As this is unknown, we set of reception of single character.
 *        	see HAL_UART_RxCpltCallback for RX callback operation.
 * */
//void port_uart_rx_init(void)
//{
//    app.uartRx.head = app.uartRx.tail = 0;
//
//	if (huart5.gState == HAL_UART_STATE_BUSY_TX_RX)
//    {
//		huart5.State = HAL_UART_STATE_BUSY_TX;
//    }
//    else
//    {
//      huart5.State = HAL_UART_STATE_READY;
//    }
//
//    /* RX uses UART_IRQ (IT) mode; TX will use DMA_IRQ+UART_IRQ */
//    if (HAL_UART_Receive_IT(&huart5, &app.uartRx.tmpRx, 1) != HAL_OK)
//    {
//        error_handler(0, _ERR_UART_RX);
//    }
//}
//
//
///* @fn      HAL_UART_RxCpltCallback
// * 			ISR level function
// * @brief   on reception of UART RX char save it to the buffer.
// * 			UART_RX is working in IT mode, but UART_TX is working with DMA mode
// * */
//void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
//{
//	uint8_t	data;
//
//	data = app.uartRx.tmpRx;
//
//	/* ReStart USART RX listening for next char */
//	if (HAL_UART_Receive_IT(&huart5, &app.uartRx.tmpRx, 1) != HAL_OK)
//    {
//        error_handler(0, _ERR_UART_RxCplt);
//    }
//
//	/* Add received data to the uart Rx circular buffer */
//    if(CIRC_SPACE( app.uartRx.head, app.uartRx.tail, sizeof(app.uartRx.buf)) > 0)
//    {
//		app.uartRx.buf[app.uartRx.head] = data;
//		app.uartRx.head = (app.uartRx.head + 1) & (sizeof(app.uartRx.buf)-1);
//    }
//    else
//    {
//        error_handler(0, _ERR_UART_RxCplt_Overflow);
//    }
//
//	if(app.ctrlTask.Handle)                                   	//RTOS : ctrlTask could be not started yet
//	{
//		osSignalSet(app.ctrlTask.Handle, app.ctrlTask.Signal);  //signal to the ctrl thread : UART data ready
//	}
//
//}
//
//
///* @fn        HAL_UART_TxCpltCallback
// * @brief      on complete of UART TX release
// * */
//void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
//{
//    /* Check if a tx/rx Process is ongoing or not */
//    if(huart->State == HAL_UART_STATE_BUSY_TX_RX)
//    {
//      huart->State = HAL_UART_STATE_BUSY_RX;
//    }
//    else
//    {
//      huart->State = HAL_UART_STATE_READY;
//    }
//}


/****************************************************************************//**
 *
 *                                 USB RX section
 *
 *******************************************************************************/
/* nothing */

/****************************************************************************//**
 *
 */

/* @fn        error_handler(block, code)
 * @brief     visually indicates something went wrong
 * @parm    if block is 1 then block execution
 * */
void error_handler(int block, error_e err)
{
    app.lastErrorCode = err;

    if(app.pConfig->s.debugEn)
    {
        if(block)
        {
            /* Flash Error Led*/
            while(block)
            {
                for(int i = err; i>0; i--)
                {
                    HAL_IWDG_Refresh(&hiwdg);
                    HAL_GPIO_WritePin(LED_ERROR_Port, LED_ERROR_Pin , GPIO_PIN_SET);
                    Sleep(250);
//                    HAL_GPIO_WritePin(LED_ERROR_Port, LED_ERROR_Pin , GPIO_PIN_RESET);
                    Sleep(250);
                }
                HAL_IWDG_Refresh(&hiwdg);
                Sleep(5000);
                HAL_IWDG_Refresh(&hiwdg);
                Sleep(5000);
                HAL_IWDG_Refresh(&hiwdg);
            }
        }
    }
}



/****************************************************************************//**
 *
 *                                 IRQ section
 *
 *******************************************************************************/

/* @brief     manually configuring and enabling of EXTI priority
 * */
void init_dw1000_irq(void)
{
    /* EXTI15_10_IRQn interrupt configuration for DW_A DW_B */
    HAL_NVIC_SetPriority(DW1000_IRQ_IRQn,configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY, 0);
    enable_dw1000_irq();
}

/* @fn        process_dwRSTn_irq
 * @brief    call-back to signal to the APP that the DW_RESET pin is went high
 *
 * */
static void dw_rst_pin_irq_cb(void)
{
    signalResetDone = 1;
}


void enable_dw1000_irq(void)
{
    HAL_NVIC_EnableIRQ(DW1000_IRQ_IRQn);
}


__INLINE void disable_dw1000_irq(void)
{
    HAL_NVIC_DisableIRQ(DW1000_IRQ_IRQn);
}


__STATIC_INLINE int
check_IRQ_enabled(IRQn_Type IRQn)
{
    return ((   NVIC->ISER[((uint32_t)(IRQn) >> 5)]
             & (uint32_t)0x01 << (IRQn & (uint8_t)0x1F)  ) ? 1 : 0) ;
}

/* @fn         HAL_GPIO_EXTI_Callback
 * @brief      EXTI line detection callback from HAL layer
 * @param      GPIO_Pin: Specifies the port pin connected to corresponding EXTI line.
 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{

    switch (GPIO_Pin)
    {
        case DW1000_RST_Pin :
        { /* bare-metal signal */
            dw_rst_pin_irq_cb();
            break;
        }

        case DW1000_IRQ_Pin :
        {
            set_dw_spi_fast_rate(DW_A);

            while(HAL_GPIO_ReadPin(DW1000_IRQ_GPIO_Port, DW1000_IRQ_Pin) == GPIO_PIN_SET)
            {
                dwt_isr();
            }

            if(app.DwCanSleep)
            {
                dwt_entersleep();
            }

            break;
        }

        default:
            break;
    }
}

/* @fn deca_mutex_on()
 * @brief     Disables IRQ from DW IRQ lines
 *             Returns the IRQ state before disable, this value is used to re-enable in decamutexoff call
 *             This is called at the beginning of a critical section
 */
decaIrqStatus_t deca_mutex_on(void)
{
    decaIrqStatus_t s  ;

    s = check_IRQ_enabled(DW1000_IRQ_IRQn);

    if(s)
    {
        HAL_NVIC_DisableIRQ(DW1000_IRQ_IRQn); //disable the external interrupt line
        __ASM volatile ("dsb 0xF":::"memory");    //CMSIS __DSB;
        __ASM volatile ("isb 0xF":::"memory");    //CMSIS __ISB;
    }

    return s ;   // return state before disable, value is used to re-enable in decamutexoff call
}

/* @fn deca_mutex_off(s)
 *
 * @brief restores IRQ enable state as saved by decamutexon
 *           This is called at the end of a critical section
 */
void deca_mutex_off(decaIrqStatus_t s)
{
    if(s)
    {
        HAL_NVIC_EnableIRQ(DW1000_IRQ_IRQn);
    }
}



/****************************************************************************//**
 *
 *                                 END OF IRQ section
 *
 *******************************************************************************/

/* @fn        reset_DW1000
 * @brief    DW_RESET pin on DW1000 has 2 functions
 *             In general it is output, but it also can be used to reset the digital
 *             part of DW1000 by driving this pin low.
 *             Note, the DW_RESET pin should not be driven high externally.
 * */
void reset_DW1000(void)
{
    GPIO_InitTypeDef     GPIO_InitStruct;

    GPIO_InitStruct.Pin = DW1000_RST_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_PULLDOWN;
    HAL_GPIO_Init(DW1000_RST_GPIO_Port, &GPIO_InitStruct);

    HAL_GPIO_WritePin(DW1000_RST_GPIO_Port, DW1000_RST_Pin, RESET);

    Sleep(1);

    GPIO_InitStruct.Pin = DW1000_RST_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(DW1000_RST_GPIO_Port, &GPIO_InitStruct);
}

/* @fn        port_wakeup_dw1000
 * @brief    "slow" waking up of DW1000 using DW_CS only
 * */
void port_wakeup_dw1000(dw_name_e chip)
{
    wakeup_dw1000(chip);
}


/* @fn        setup_DW1000_rst_pin_irq
 * @brief    setup the DW_RESET pin mode
 *             0 - output Open collector mode
 *             !0 - input mode with connected EXTI0 IRQ
 * */
static void setup_DW1000_rst_pin_irq(int enable)
{
    GPIO_InitTypeDef GPIO_InitStruct;

    if(enable)
    {
        /* Enable Reset Pin for use as IRQ for interrupt
         * this will be used to determine the chip in the Idle mode
         * */
        GPIO_InitStruct.Pin  = DW1000_RST_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
        HAL_GPIO_Init(DW1000_RST_GPIO_Port, &GPIO_InitStruct);

        HAL_NVIC_SetPriority(EXTI3_IRQn,4, 0); //below RTOS priority
        HAL_NVIC_EnableIRQ(EXTI3_IRQn);


    }
    else
    {
        HAL_NVIC_DisableIRQ(EXTI3_IRQn);

        //put the pin back to tri-state ... as analog input or input floating
        GPIO_InitStruct.Pin  = DW1000_RST_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
		HAL_GPIO_Init(DW1000_RST_GPIO_Port, &GPIO_InitStruct);
		HAL_GPIO_WritePin(DW1000_RST_GPIO_Port, DW1000_RST_Pin, GPIO_PIN_SET);
    }
}


/* @fn      usleep
 * @brief precise usleep() delay
 * */
#pragma GCC optimize ("O0")
void usleep(__useconds_t usec)
{
    int i;
#pragma GCC ivdep
    for(i=0;i<usec;i++)
    {
        __NOP();
        __NOP();
        __NOP();
        __NOP();
        __NOP();
        __NOP();
    }
}


#define WAKEUP_TMR_MS    (3)
/* @fn      port_wakeup_dw1000_fast
 * @brief   waking up of DW1000 using DW_CS and DW_RESET pins.
 *          The DW_RESET signalling that the DW1000 is in the INIT state.
 *          the total fast wakeup takes ~2.2ms and depends on crystal startup time
 * */
error_e port_wakeup_dw1000_fast(void)
{
    uint32_t x = 0;
    uint32_t timestamp = portGetTickCount();    //protection
    error_e  ret = _Err_Timeout;

    setup_DW1000_rst_pin_irq(0);    //disable RSTn IRQ
    signalResetDone = 0;            //signalResetDone connected to the process_dwRSTn_irq() callback
    setup_DW1000_rst_pin_irq(1);    //enable RSTn Rising IRQ

    spi_handle_t *pSpi = get_spi_handler();

    //set CS pins = 0
    HAL_GPIO_WritePin(pSpi->csPort, pSpi->csPin, GPIO_PIN_RESET);

    //need to poll to check when the DW1000 is in the IDLE. The CPLL interrupt is not reliable.
    //when RSTn goes high the DW1000 is in INIT, it will enter IDLE after PLL lock (in 5 us)
    while((HAL_GetTick() - timestamp) < WAKEUP_TMR_MS)
    {
        x++;     //when DW1000 will switch to an IDLE state RSTn pin will be high
        if(signalResetDone == 1)
        {
            ret = _NO_ERR;
            break;
        }
        Sleep(1);
    }

    setup_DW1000_rst_pin_irq(0);         //disable RSTn IRQ

    //set CS pins = 1
    HAL_GPIO_WritePin(pSpi->csPort, pSpi->csPin, GPIO_PIN_SET);

    //it takes ~35us in total for the DW1000 to lock the PLL, download AON and go to IDLE state
    //usleep(35);

    //MYMOD:BUG DECAWAVE
	usleep(1000);

	//TODO: TO TEST ONLY
	//uint32 status = dwt_read32bitreg(0x0F); // Read status register low 32bits

    return ret;
}


/* @fn         port_stop_all_UWB(s)
 *
 * @brief     stop UWB activity
 */
void port_stop_all_UWB(void)
{
    decaIrqStatus_t s = deca_mutex_on();

    set_dw_spi_slow_rate(DW_A);

    dwt_forcetrxoff();

    dwt_setinterrupt(DWT_INT_TFRS | DWT_INT_RFCG | DWT_INT_ARFE | DWT_INT_RFSL |\
                       DWT_INT_SFDT | DWT_INT_RPHE | DWT_INT_RFCE | DWT_INT_RFTO | DWT_INT_RXPTO, 0);

    dwt_softreset();

    dwt_setcallbacks(NULL, NULL,  NULL, NULL);

    deca_mutex_off(s);
}


/**
 *  @brief     Bare-metal level
 *          initialise master/slave DW1000 (check if can talk to device and wake up and reset)
 */
static error_e
port_init_device(dw_name_e device)
{
    set_dw_spi_slow_rate(device);

    //this is called here to wake up the device (i.e. if it was in sleep mode before the restart)
    uint32   devID0 = dwt_readdevid() ;

    if(DWT_DEVICE_ID != devID0) //if the read of device ID fails, the DW1000 could be asleep
    {
        port_wakeup_dw1000(device);

        devID0 = dwt_readdevid();
        // SPI not working or Unsupported Device ID
        if(DWT_DEVICE_ID != devID0)
            return(_ERR_DEVID);
    }
    //clear the sleep bit in case it is set - so that after the hard reset below the DW does not go into sleep
    dwt_softreset();

    return (_NO_ERR);
}

/*
 * @brief this function
 * 1. disables irq from DW1000 chip
 * 2. sets the default DW1000 chip structures pointer pDwA
 * 3. executes a slow wakeup of DW1000
 * 4. sets slow SPI speed to DW1000
 * 5. read devID
 * 6. on successfull reading of devID executes the dwt_softreset()
 * */
void port_disable_wake_init_dw(void)
{
    disable_dw1000_irq();             /**< disable NVIC IRQ until we configure the device */

    taskENTER_CRITICAL();

    pDwA = &dw_chip_A;

    //this is called here to wake up the device (i.e. if it was in sleep mode before the restart)
    port_wakeup_dw1000(DW_A);

    if( (port_init_device(DW_A) != _NO_ERR))
    {
        error_handler(1,  _ERR_INIT);
    }

    taskEXIT_CRITICAL();
}
/* @fn         start_timer(uint32 *p_timestamp)
 * @brief     save system timestamp (in CLOCKS_PER_SEC)
 * @parm     p_timestamp pointer on current system timestamp
 */
void start_timer(volatile uint32_t * p_timestamp)
{
    *p_timestamp = HAL_GetTick();
}


/* @fn         check_timer(uint32 timestamp , uint32 time)
 * @brief     check if time from current timestamp over expectation
 * @param     [in] timestamp - current timestamp
 * @param     [in] time - time expectation (in CLOCKS_PER_SEC)
 * @return     true - time is over
 *             false - time is not over yet
 */
bool check_timer(uint32_t timestamp, uint32_t time)
{
    uint32_t time_passing;
    uint32_t temp_tick_time = HAL_GetTick();

    if (temp_tick_time >= timestamp)
    {
        time_passing = temp_tick_time - timestamp;
    }
    else
    {
        time_passing = 0xffffffffUL - timestamp + temp_tick_time;
    }

    if (time_passing >= time)
    {
        return (true);
    }

    return (false);
}


/* @brief    Sleep
 *             -DDEBUG defined in Makefile prevents __WFI
 */
void Sleep( volatile uint32_t dwMs )
{
    uint32_t dwStart ;
    start_timer(&dwStart);
    while(check_timer(dwStart,dwMs)==false)
    {
        #ifndef DEBUG
//        __WFI();
        #endif
    }
}



/**/

#if (configCHECK_FOR_STACK_OVERFLOW > 0)

void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName )
{
    error_handler(1, _ERR_Stack_Overflow);
}
#endif
/* END of portDW.c */
