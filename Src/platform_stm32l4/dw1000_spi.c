/*
 * @file    dw1000_spi.c
 * @brief    SPI functions to DW1000 chip's from STM32.
 *
 * Pure "HAL" SPI Driver implementation is too slow.
 * It takes 38-42us to read the SYS_STATUS register (5bytes SPI),
 * independent of the realization (Poll, IT, DMA).
 *
 * We use the combination of HAL and LL drivers:
 * HAL is used to initialise SPI and DMA,
 * and LL is used thereafter in the operation.
 *
 * With LL driver it takes ~15us for the reading of SYS_STATUS (DMA or IT mode).
 * Still slow, but this is the best we could achieve in STM32 for short transactions.
 *
 * @author Decawave Software
 *
 * @attention Copyright 2017 (c) DecaWave Ltd, Dublin, Ireland.
 *            All rights reserved.
 *
 */


#include <port.h>
#include "deca_device_api.h"
#include "main.h"

//-----------------------------------------------------------------------------
#define SPI_BUFFLEN        (32)

static  uint8_t spiA_TmpBuffer[SPI_BUFFLEN];

//-----------------------------------------------------------------------------
#include "stm32l4xx_ll_dma.h"
#include "stm32l4xx_ll_spi.h"
#include "stm32l4xx_ll_bus.h"

//------------------------------------------------------------------------------
static spi_handle_t spiA_handler
=
{//SPI1 clocked from 80MHz
    .phspi          = &hspi1,
    .prescaler_slow = SPI_BAUDRATEPRESCALER_32,
    .prescaler_fast = SPI_BAUDRATEPRESCALER_8,
    .dmaRx          = DMA1,
    .channelRx      = LL_DMA_CHANNEL_2,
    .dmaTx          = DMA1,
    .channelTx      = LL_DMA_CHANNEL_3,
    .csPort         = DW1000_CS_GPIO_Port,
    .csPin          = DW1000_CS_Pin,
    .Lock           = HAL_UNLOCKED,
    .TxComplete     = 0,
    .RxComplete     = 0,
    .pBuf           = spiA_TmpBuffer,
};

//------------------------------------------------------------------------------
// DW chip description
const dw_t dw_chip_A
=
{
    .irqPin     = DW1000_IRQ_Pin,
    .irqPort    = DW1000_IRQ_GPIO_Port,
    .irqN       = DW1000_IRQ_IRQn,
    .rstPin     = DW1000_RST_Pin,
    .rstPort    = DW1000_RST_GPIO_Port,
    .rstIrqN    = DW1000_RST_IRQn,
    .wakeUpPin  = DW1000_WUP_Pin,
    .wakeUpPort = DW1000_WUP_GPIO_Port,
    .pSpi       = &spiA_handler
};


static spi_handle_t * pgSpiHandler = &spiA_handler;

const dw_t *pDwA = &dw_chip_A;

//------------------------------------------------------------------------------
spi_handle_t * get_spi_handler(void)
{
    return(pgSpiHandler);
}

__STATIC_INLINE
void set_SPI_master(void)
{
    while(pgSpiHandler->Lock);    //wait the completion of the last transaction
    pgSpiHandler = pDwA->pSpi;
}

//-----------------------------------------------------------------------------

/* @fn         wakeup_DECA(void)
 * @brief     wakes up both DW1000 chip using configured CS pins
 *             assuming hspi are configured before
 * */
void wakeup_dw1000(dw_name_e chip)
{
    spi_handle_t *p = pDwA->pSpi;

    if(chip == DW_A)
    {
        //set CS pins = 0
		HAL_GPIO_WritePin(p->csPort, p->csPin, GPIO_PIN_RESET);
        Sleep(1);
		//release CS pins
        HAL_GPIO_WritePin(p->csPort, p->csPin, GPIO_PIN_SET);
    }
    Sleep(7);

}


/* @fn        set_dw_spi_X_slowrate
 *             sets pgSpiHandler
 * */
void set_dw_spi_slow_rate(dw_name_e chip)
{
    while(pgSpiHandler->Lock);    //wait the completion of the last transaction

    if(chip == DW_A)
    {
        set_SPI_master();
    }

    pgSpiHandler->phspi->Init.BaudRatePrescaler = pgSpiHandler->prescaler_slow;
    HAL_SPI_Init(pgSpiHandler->phspi);

}

/* @fn      set_dw_spi_X_fastrate
 * @brief   set 16MHz
 *          note: hspi1 is clocked from 32 MHz
 * */
void set_dw_spi_fast_rate(dw_name_e chip)
{
    while(pgSpiHandler->Lock);    //wait the completion of the last transaction

    if(chip == DW_A)
    {
        set_SPI_master();
    }

    pgSpiHandler->phspi->Init.BaudRatePrescaler = pgSpiHandler->prescaler_fast;
    HAL_SPI_Init(pgSpiHandler->phspi);
}


/*
 * */
__STATIC_INLINE void
open_spi(spi_handle_t * p)
{
    if( (p->phspi->Instance != SPI1))
    {
        error_handler(1, _ERR_SPI_DMA);
    }

    /* Disable DMA channels by default */
    LL_DMA_DisableChannel(p->dmaRx, p->channelRx);
    LL_DMA_DisableChannel(p->dmaTx, p->channelTx);

    //LL_SPI_Enable(p->phspi->Instance);

    /* Configure spi DMA transfer interrupts */
    /* Enable DMA RX Interrupt */
    LL_SPI_EnableDMAReq_RX(p->phspi->Instance);
    LL_DMA_EnableChannel(p->phspi->Instance,p->channelRx);
    /* Enable DMA TX Interrupt */
    LL_SPI_EnableDMAReq_TX(p->phspi->Instance);
    LL_DMA_EnableChannel(p->phspi->Instance,p->channelTx);

    /* Enable DMA interrupts complete/error */
    LL_DMA_EnableIT_TC(p->dmaRx, p->channelRx);
    LL_DMA_EnableIT_TE(p->dmaRx, p->channelRx);
    LL_DMA_EnableIT_TC(p->dmaTx, p->channelTx);
    LL_DMA_EnableIT_TE(p->dmaTx, p->channelTx);
}

/*
 * */
void close_spi(spi_handle_t* p)
{
    LL_SPI_DisableDMAReq_TX(p->phspi->Instance);
    LL_SPI_DisableDMAReq_RX(p->phspi->Instance);
    LL_SPI_Disable(p->phspi->Instance);

    p->TxComplete = 0;
    p->RxComplete = 0;
}


/*
 * @brief blocking waiting of end of dma/spi transaction
 * */
__STATIC_INLINE void
dmaWaitEndOfTxRx(spi_handle_t * p)
{
    if( (p->phspi->Instance != SPI1))
    {
        error_handler(1, _ERR_SPI_DMA);
    }

    while (p->TxComplete != 1);
    LL_DMA_DisableChannel(p->dmaTx, p->channelTx);      /* Disable DMA Tx Channel */

    while (p->RxComplete != 1);
    LL_DMA_DisableChannel(p->dmaRx, p->channelRx);      /* Disable DMA Rx Channel */
}


/**
  * @brief  This function Activate SPI1
  * @param  None
  * @retval None
  */
__STATIC_INLINE void
Activate_SPI(spi_handle_t* p)
{
    if( (p->phspi->Instance != SPI1))
    {
        error_handler(1, _ERR_SPI_DMA);
    }

    /* Enable SPI */
    LL_SPI_Enable(p->phspi->Instance);

    /* Enable DMA Channels */
    LL_DMA_EnableChannel(p->dmaRx, p->channelRx);
    LL_DMA_EnableChannel(p->dmaTx, p->channelTx);
}


/**
  * @brief This function configures the DMA Channels for SPI
  * @note  This function is used to :
  *        1. Configure the DMA Rx Channel
  *        2. Configure the DMA Tx Channel
  *
  *        DMA's clock and NVIC interrupts are pre-configured
  *
  * @param   None
  * @retval  None
  */
__STATIC_INLINE void
Configure_DMA(spi_handle_t *p, const uint8_t *txBuffer, uint8_t *rxBuffer, uint32_t len)
{
    if( (p->phspi->Instance != SPI1))
    {
        error_handler(1, _ERR_SPI_DMA);
    }
    LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_DMA1);

    uint32_t periph_addr = LL_SPI_DMA_GetRegAddr(p->phspi->Instance);


    /* (1) Configure the DMA RX */
    LL_DMA_ConfigTransfer(p->dmaRx, p->channelRx,
                        LL_DMA_DIRECTION_PERIPH_TO_MEMORY | LL_DMA_PRIORITY_HIGH | LL_DMA_MODE_NORMAL |
                        LL_DMA_PERIPH_NOINCREMENT | LL_DMA_MEMORY_INCREMENT |
                        LL_DMA_PDATAALIGN_BYTE | LL_DMA_MDATAALIGN_BYTE);

    LL_DMA_ConfigAddresses(p->dmaRx, p->channelRx, periph_addr, (uint32_t)rxBuffer,
                        LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
                        //LL_DMA_GetDataTransferDirection(p->dma_rx, p->channel_rx));
    LL_DMA_SetDataLength(p->dmaRx, p->channelRx, len);
    LL_DMA_SetPeriphRequest(p->dmaRx, p->channelRx, LL_DMA_REQUEST_1);

    /* (2) Configure the DMA TX */
    LL_DMA_ConfigTransfer(p->dmaTx, p->channelTx,
                        LL_DMA_DIRECTION_MEMORY_TO_PERIPH | LL_DMA_PRIORITY_HIGH | LL_DMA_MODE_NORMAL |
                        LL_DMA_PERIPH_NOINCREMENT | LL_DMA_MEMORY_INCREMENT |
                        LL_DMA_PDATAALIGN_BYTE | LL_DMA_MDATAALIGN_BYTE);

    LL_DMA_ConfigAddresses(p->dmaTx, p->channelTx, (uint32_t)txBuffer, periph_addr,
                       LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
                        //LL_DMA_GetDataTransferDirection(p->dmaTx, p->channelTx));
    LL_DMA_SetDataLength(p->dmaTx, p->channelTx, len);
    LL_DMA_SetPeriphRequest(p->dmaTx, p->channelTx, LL_DMA_REQUEST_1);
}


//-----------------------------------------------------------------------------

/*
 * */
__STATIC_INLINE
int readfromspi_uni(uint16_t headerLength,
                    const uint8_t *headerBuffer,
                    uint32_t readlength,
                    uint8_t *readBuffer,
                    spi_handle_t *pSpiHandler)
{
    while(pSpiHandler->Lock == HAL_LOCKED);

    __HAL_LOCK(pSpiHandler);    //"return HAL_BUSY;" if locked

    open_spi(pSpiHandler);

    uint32_t tmpLen, n = 0;

    HAL_GPIO_WritePin(pSpiHandler->csPort, pSpiHandler->csPin, GPIO_PIN_RESET);

    do {
        tmpLen = MIN(readlength+headerLength, SPI_BUFFLEN);

        Configure_DMA (pSpiHandler, headerBuffer, pSpiHandler->pBuf, tmpLen);

        pSpiHandler->TxComplete = 0;
        pSpiHandler->RxComplete = 0;
        __ISB();
		__DSB();

        Activate_SPI(pSpiHandler);

        dmaWaitEndOfTxRx(pSpiHandler);

        for(int i = 0; i < tmpLen-headerLength; i++)
        {
        	readBuffer[n + i] = pSpiHandler->pBuf[headerLength + i];
        }

        readlength  -= (tmpLen-headerLength);
        n          	+= (tmpLen-headerLength);
        headerLength = 0;                    //after first transmission of the header

    } while (readlength > 0);

    HAL_GPIO_WritePin(pSpiHandler->csPort, pSpiHandler->csPin, GPIO_PIN_SET);

    close_spi(pSpiHandler);

    __HAL_UNLOCK(pSpiHandler);

    return _NO_ERR;
}

/* @brief Note,
 *		  The Tag applicaiton is not meet strict timing requirements
 *  	  non-dma veersion of this function is used.
 * */
__STATIC_INLINE
int writetospi_uni( uint16_t headerLength, const uint8_t *headerBuffer,
                    uint32_t bodyLength,   const uint8_t *bodyBuffer,
                    spi_handle_t *pSpiHandler)
{
#if (1)
    uint8_t tmp_buf[SPI_BUFFLEN];

    assert_param(headerLength < SPI_BUFFLEN );

    /* Blocking: Check whether the previous transfer has been finished */
    while(pSpiHandler->Lock == HAL_LOCKED);

    __HAL_LOCK(pSpiHandler);    //"return HAL_BUSY;" if locked

    open_spi(pSpiHandler);

    uint32_t tmpLen, n;

    for(int i = 0; i < headerLength; i++)
    {
        pSpiHandler->pBuf[i] =  headerBuffer[i];
    }

    n = 0;

    HAL_GPIO_WritePin(pSpiHandler->csPort, pSpiHandler->csPin, GPIO_PIN_RESET);

    do {
        tmpLen = MIN(bodyLength + headerLength, SPI_BUFFLEN);

        Configure_DMA ( pSpiHandler, pSpiHandler->pBuf, tmp_buf, tmpLen);

        for(int i = 0; i < tmpLen - headerLength; i++)
        {
            pSpiHandler->pBuf[headerLength + i] =  bodyBuffer[n + i];
        }

        pSpiHandler->TxComplete = 0;
        pSpiHandler->RxComplete = 0;
        __ISB();
		__DSB();

        Activate_SPI(pSpiHandler);

        dmaWaitEndOfTxRx(pSpiHandler);

        bodyLength -= (tmpLen-headerLength);
        n            += (tmpLen-headerLength);
        headerLength= 0;

    } while (bodyLength > 0);

    HAL_GPIO_WritePin(pSpiHandler->csPort, pSpiHandler->csPin, GPIO_PIN_SET);

    close_spi(pSpiHandler);

    __HAL_UNLOCK(pSpiHandler);


#else

    /* Blocking: Check whether the previous transfer has been finished */
    while(pSpiHandler->Lock == HAL_LOCKED);

    __HAL_LOCK(pSpiHandler);    //"return HAL_BUSY;" if locked

    int i;

    HAL_GPIO_WritePin(pSpiHandler->csPort, pSpiHandler->csPin, GPIO_PIN_RESET);

    /* Send header */
    for(i=0; i<headerLength; i++)
    {
        if (HAL_SPI_Transmit(pSpiHandler->phspi, &headerBuffer[i], 1, HAL_MAX_DELAY)) ////No timeout
        {
            error_handler(0,0); /**< Show transfer error in transmission process / non-blocking */
        }
    }

    for(i=0; i<bodyLength; i++)
    {
        /* Send data */
        if (HAL_SPI_Transmit(pSpiHandler->phspi, &bodyBuffer[i], 1, HAL_MAX_DELAY)) ////No timeout
        {
            error_handler(0,0); /**< Show transfer error in transmission process / non-blocking */
        }
    }

    HAL_GPIO_WritePin(pSpiHandler->csPort, pSpiHandler->csPin, GPIO_PIN_SET);

    __HAL_UNLOCK(pSpiHandler);

#endif

    return _NO_ERR;
}



//-----------------------------------------------------------------------------

// reads from pSpiHandler
__INLINE
int readfromspi(uint16_t headerLength, const uint8_t *headerBuffer,
                uint32_t readlength,   uint8_t *readBuffer)
{

    return readfromspi_uni(headerLength, headerBuffer,
                            readlength, readBuffer, pgSpiHandler);
}

// writes to pSpiHandler
__INLINE
int writetospi(uint16_t headerLength, const uint8_t *headerBuffer,
               uint32_t bodyLength,   const uint8_t *bodyBuffer)
{
    return writetospi_uni(headerLength, headerBuffer,
                            bodyLength, bodyBuffer, pgSpiHandler);
}


//-----------------------------------------------------------------------------
// Callbacks for IRQ
/**
  * @brief  Function called from DMA1 IRQ Handler when Rx transfer is completed
  * @param  None
  * @retval None
  */
#pragma GCC optimize ("O0")
void DMA1_ReceiveComplete_Callback(void)
{
    /* signal that the DMA Rx transfer completed */
    spiA_handler.RxComplete = 1;
}

/**
  * @brief  Function called from DMA1 IRQ Handler when Tx transfer is completed
  * @param  None
  * @retval None
  */
#pragma GCC optimize ("O0")
void DMA1_TransmitComplete_Callback(void)
{
    /* signal that the DMA Tx transfer completed */
    spiA_handler.TxComplete = 1;
}


void SPI1_TransferError_Callback(void)
{
    spiA_handler.RxComplete = 1;
    spiA_handler.TxComplete = 1;
    error_handler(1, _ERR_SPI_DMA);
}


/****************************************************************************//**
 *
 *                                 END OF SPI section
 *
 *******************************************************************************/

