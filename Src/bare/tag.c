/*!----------------------------------------------------------------------------
 * @file    tag.c
 * @brief    DecaWave Application Layer
 *             TWR functions collection
 *
 * @attention
 *
 * Copyright 2016-2017 (c) DecaWave Ltd, Dublin, Ireland.
 *
 * All rights reserved.
 *
 * @author
 *
 * DecaWave
 */

/* Includes */
#include <circ_buf.h>
#include <error.h>
#include "deca_regs.h"
#include "math.h"

#include "deca_device_api.h"

#include "cmsis_os.h"
#include "assert.h"
#include <limits.h>
#include <port.h>
#include <tag.h>
#include <util.h>
#include <uwb_frames.h>

#include <tagManager.h>
#include <powerGainSettings.h>
#include "targetManager.h"
#include "superFrameCounter.h"
#include <stdlib.h>
#include "main.h"

#include "driver_exclusion.h"
//-----------------------------------------------------------------------------
// Definitions
#define SAFE_TXDATA        /*see start_tx()*/

#define TWR_ENTER_CRITICAL()    taskENTER_CRITICAL()
#define TWR_EXIT_CRITICAL()     taskEXIT_CRITICAL()

/* Two parameters will be sent to the Tag in the Ranging Config message:
 * First, is the ~delay after Poll_Tx, when Tag needs to go to Rx, and the second is
 * the ~delay between Poll_Tx's and Final_Tx's R-marks.
 * Tag hardware shall be capable to meet the requested timings.
 * Below defined limits, used to specify Tag's maximum HW capability.
 *
 * That limits depend on the MCU speed, code optimization, latency, introduced by
 * the application architecture, especially number of reading/writing from/to DW1000 and RTOS latencies.
 * Timings can be improved more (decreased) by placing a ranging-dependent
 * functionality below RTOS, (so app will not have a delay, introduced by RTOS itself).
 * However, current realization is not optimized for that.
 * */
#define MIN_RESPONSE_CAPABILITY_US          (100)   /**< time when Tag can be ready to receive the Respose after its Poll */
#define MIN_POLL_TX_FINAL_TX_CAPABILITY_US  (1500)  /**< time for tag to transmit Poll_TX and then Final_TX) */


/* For best PDOA performance the clock offset between PDOA node and the tag
 * should be >2ppm.
 * In case Tag's crystal is very off, it also will be trimmed to stay in the [2 .. 4]ppm range,
 * as defined below:
 * */
#define TARGET_XTAL_OFFSET_VALUE_PPHM_MIN   (200)
#define TARGET_XTAL_OFFSET_VALUE_PPHM_MAX   (400)
#define AVG_TRIM_PER_PPHM                   (32.f/4800.f) /* Trimimg per 100 ppm*/

#define SNIFF_ON_TIME 15
#define SNIFF_OFF_TIME 1
#define RX_RELAX_TIMEOUT_SY        (50)    /**< relaxed RX Timeout in sequential TWR process exchange */

//-----------------------------------------------------------------------------

/* const txSpectrumConfig for channel 5 spectrum config */
static uint8_t    startFrame = 0;
static const struct
{
    uint8_t     PGdelay;

    //TX POWER
    //31:24     BOOST_0.125ms_PWR
    //23:16     BOOST_0.25ms_PWR-TX_SHR_PWR
    //15:8      BOOST_0.5ms_PWR-TX_PHR_PWR
    //7:0       DEFAULT_PWR-TX_DATA_PWR
    uint32_t    txPwr[2]; //
}txSpectrumConfig
=
{//Channel 5
    0xc0,   //PG_DELAY
    {
        0x0E082848, //16M prf power
        0x25456585  //64M prf power
    }
};

//-----------------------------------------------------------------------------
// TWR structure holds all TWR data
#define TAG_STATIC_TWRINFO
#ifdef TAG_STATIC_TWRINFO
//static ("safe") implementation
static twr_info_t    TwrInfo;

#else
//dynamic allocation of TwrInfo
static twr_info_t    *pTwrInfo = NULL;

#define TAG_MALLOC     pvPortMalloc
#define TAG_FREE    vPortFree

#endif

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Local functions prototypes

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// Implementation


//-----------------------------------------------------------------------------
// Support section

/*
 * @brief     get pointer to the twrInfo structure
 * */
twr_info_t *
getTwrInfoPtr(void)
{
#ifdef TAG_STATIC_TWRINFO
    return (&TwrInfo);
#else
    return (pTwrInfo);
#endif
}


/*
 * @brief   ISR level (need to be protected if called from APP level)
 *          low-level configuration for DW1000
 *
 *          if called from app, shall be performed with DW IRQ off &
 *          TWR_ENTER_CRITICAL(); / TWR_EXIT_CRITICAL();
 *
 *
 * @note
 * */
static void
rxtx_tag_configure(rxtx_tag_configure_t *p)
{
    assert(p->pdwCfg->chan == 5);            /**< This project supports only CH5 + DWT_PRF_64M */

    dwt_txconfig_t  dtmp;
    uint32_t        power;

    dwt_configure(p->pdwCfg);    /**< Configure the Physical Channel parameters (PLEN, PRF, etc) */

    /* configure power */
    power = txSpectrumConfig.txPwr[p->pdwCfg->prf - DWT_PRF_16M];

    init_powerGainSettings(); // config power tx

    if(app.pConfig->s.smartTxEn == 0)
    {
        power = get_powerGainSettings(); // acap
        power = (power & 0xff) ;
        power |= (power << 8) + (power << 16) + (power << 24);
        dwt_setsmarttxpower(0);
    }
    else
    {
        dwt_setsmarttxpower(1);
    }

    dtmp.power = power;
    dtmp.PGdly = txSpectrumConfig.PGdelay ;

    dwt_configuretxrf(&dtmp);

    /* set antenna delays */
    dwt_setrxantennadelay(p->rxAntDelay);
    dwt_settxantennadelay(p->txAntDelay);

    dwt_setdblrxbuffmode (0);    /**< dblBuf is not used in TWR */
    dwt_setrxaftertxdelay(0);    /**< no any delays set by default : part of config of receiver on Tx sending */
    dwt_setrxtimeout     (0);    /**< no any delays set by default : part of config of receiver on Tx sending */
    //dwt_enableframefilter(p->frameFilter);

    //dwt_setpanid(p->panId);

    /*patch for preamble length 64 */
    if(p->pdwCfg->txPreambLength == DWT_PLEN_64)
    {
        set_dw_spi_slow_rate(DW_A);
        dwt_loadopsettabfromotp(DWT_OPSET_64LEN);
        set_dw_spi_fast_rate(DW_A);
    }

    dwt_setaddress16(p->shortadd);

}


/*
 * @brief   ISR level (need to be protected if called from APP level)
 * @param   twr_info_t *pTwrInfo has two members
 *          xtaltrim - current trimmed value
 *          clkOffset_pphm -
 *
 * @note    This changes the DW1000 system clock and shall be performed
 *          when DW1000 is not in active Send/Receive state.
 * */
void trim_tag_proc(twr_info_t *pTwrInfo)
{
    unsigned tmp = abs(pTwrInfo->clkOffset_pphm);

    if(tmp > TARGET_XTAL_OFFSET_VALUE_PPHM_MAX ||
       tmp < TARGET_XTAL_OFFSET_VALUE_PPHM_MIN)
    {
        int8_t tmp8 = pTwrInfo->xtaltrim;
        tmp8 -= (int8_t)(((TARGET_XTAL_OFFSET_VALUE_PPHM_MAX + TARGET_XTAL_OFFSET_VALUE_PPHM_MIN)/2 + pTwrInfo->clkOffset_pphm) * AVG_TRIM_PER_PPHM);
        pTwrInfo->xtaltrim = (uint8_t)(FS_XTALT_MASK & tmp8);

        /* Configure new Crystal Offset value */
        dwt_setxtaltrim(pTwrInfo->xtaltrim);
    }
}


// #if (DIAG_READ_SUPPORT==1)
// /*
//  * @brief    ISR layer
//  *     read full diagnostic data form the received frame from the two DW1000s
//  *     offset 0 / 1
//  * */
// static int
// read_full_diagnostics(rx_pckt_t *prxPckt,
//                         uint32   status )
// {
//     uint16_t     fpIndex;
//     diag_v5_t    *p = &prxPckt->diagnostics;

//     p->header = DWT_DIAGNOSTIC_LOG_REV_5;

//     memcpy(p->r0F, (uint8_t*) &status, 4);                      //copy 4bytes of status (saved on entry to ISR)
//     dwt_readfromdevice(RX_FINFO_ID, 4, 5, (uint8_t*)(p+5) );    //read MSB from status and 4byte frame info
//     dwt_readfromdevice(RX_FQUAL_ID, 0, 17,(uint8_t *)(p->r12)); //read 17 bytes of diagnostic data from 0x12,13,14

//     memcpy((uint8_t*)p->r15, prxPckt->rxTimeStamp, TS_40B_SIZE);//copy TS
//     dwt_readfromdevice(RX_TIME_ID, RX_TIME_FP_INDEX_OFFSET, 9, (uint8_t *)(p->r15 + 5)); //2FP, 2Diag, 5TSraw

//     // Calculate the First Path Index ((LDE0 + LDE1 << 8) / 64)
//     fpIndex = (*((uint8_t*)(p+32)) >> 6) + (*((uint8_t*)(p+33)) << 2);

//     fpIndex = fpIndex*4 + 1;                 //get location in the accumulator

//     //printf("%d FP index %02x %02x %i %i\n", offset, *((uint8_t*)(p+32)), *((uint8_t*)(p+33)), fpIndex, (fpIndex-1)>>2);
//     //Read CIR for the First Path + 3 More samples (4*4 = 16)
//     dwt_readaccdata(p->r25, 17, fpIndex-1); //read 1 extra as first will be dummy byte
//     dwt_readfromdevice(LDE_IF_ID, LDE_PPINDX_OFFSET, 2, p->r2E);
//     dwt_readfromdevice(DRX_CONF_ID, 0x28,            4, p->r27);
//     dwt_readfromdevice(LDE_IF_ID, LDE_PPAMPL_OFFSET, 2, p->r2E2);

//     return (int) fpIndex ;
// }
// #endif

/**
 * @brief   ISR layer
 *          Transmit packet
 * */
static error_e
tx_start(tx_pckt_t * pTxPckt)
{
    error_e ret = _NO_ERR;
    uint8_t  txFlag = 0;

    dwt_forcetrxoff();    //Stop the Receiver and Write Control and Data

#ifdef SAFE_TXDATA
    dwt_writetxdata(pTxPckt->psduLen, (uint8_t *) &pTxPckt->msg.stdMsg, 0);
#endif

    dwt_writetxfctrl(pTxPckt->psduLen, 0, 1);

    //Setup for delayed Transmit
    if(pTxPckt->delayedTxTimeH_sy != 0UL)
    {
        dwt_setdelayedtrxtime(pTxPckt->delayedTxTimeH_sy) ;
    }

    if(/*(pTxPckt->delayedRxTime_sy !=0) && */(pTxPckt->txFlag & DWT_RESPONSE_EXPECTED))
    {
        dwt_setrxaftertxdelay(pTxPckt->delayedRxTime_sy);
        dwt_setrxtimeout(pTxPckt->delayedRxTimeout_sy);
    }

    // Begin delayed TX of frame
    txFlag = (pTxPckt->delayedTxTimeH_sy != 0UL) | (pTxPckt->txFlag);

    if(dwt_starttx(txFlag) != DWT_SUCCESS)
    {
        ret = _Err_DelayedTX_Late;
    }

#ifndef SAFE_TXDATA
    /* while transmitting of the preamble we can fill the data path
     * to save time : this is not "safe" approach and
     * additional check to be done to use this feature.
     * TODO: left for future : do not use this feature
     * */
    if (ret == _NO_ERR)
    {
        dwt_writetxdata(pTxPckt->psduLen, (uint8_t *)  &pTxPckt->msg.stdMsg, 0);
    }
#endif

    return (ret);
}

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
//    DW1000 callbacks section :
//    if RTOS, the preemption priority of the dwt_isr() shall be such, that
//    allows signal to the thread.

/*
 * @brief   This is a special function, which starts the SAR sampling.
 *          This shall be started in order to update temperature of the chip.
 *      Note: the reading of temperature will be available ~1mS after start of this function.
 * */
static void
start_tempvbat_sar(void)
{
    uint8_t wr_buf[1];
    // These writes should be single writes and in sequence
    wr_buf[0] = 0x80; // Enable TLD Bias
    dwt_writetodevice(RF_CONF_ID,0x11,1,wr_buf);

    wr_buf[0] = 0x0A; // Enable TLD Bias and ADC Bias
    dwt_writetodevice(RF_CONF_ID,0x12,1,wr_buf);

    wr_buf[0] = 0x0f; // Enable Outputs (only after Biases are up and running)
    dwt_writetodevice(RF_CONF_ID,0x12,1,wr_buf);    //

    // Reading All SAR inputs
    wr_buf[0] = 0x00;
    dwt_writetodevice(TX_CAL_ID, TC_SARL_SAR_C,1,wr_buf);
    wr_buf[0] = 0x01; // Set SAR enable
    dwt_writetodevice(TX_CAL_ID, TC_SARL_SAR_C,1,wr_buf);
}
static void

resp_tx_cb_setup_receivers(twr_info_t *pTwrInfo)
{
//------------------------------------------------------------------------------
//
// Description of the ranging protocol.
//
//           |< ------------------------------- tag_pollTxFinalTx_us --------------------------------->|
//TAGTX      |                                                                                         |
//   PollTxSFD   PollTxEnd                                                                    FinalTxSFD   FinalTxEnd
//_i---------|---i___________________________________________________________________________i---------|---i___
//           |
//           |   |<--- tag_replyDly_us --->|                                                 |
//TAGRX         |                   RespRxStart                                                 |
//_______________________________________..i---------------i___________________________________________________
//
//           |                             |                                                 |
//NODERX     |                             |                                                 |
//CHIP MASTER|                             |           |<-(2)- dwt_setdelayedtrxtime for M ->|
//           |                             |           |                                     |
//    PollRxTs                             |           |                                      FinalRxSFD
//-----------|---i___________________________________________________________________________i---------|---i___
//           |
//           |<--[1]-- delayedTxTimeH_sy (tmp64) ----->|
//           |                                         |
//NODETX                                        respTxTs
//_________________________________________i-----------|---i___________________________________________________
//
//           |                                         |
//NODERX     |                                         |
//CHIP SLAVE's DEVTIME TIME is synchronized and close to MASTER's dev_time. use Master's dev_time to activate SLAVE's receiver
//the differences between M&S dev_time is << delayed_RX time, so we should never experience fail in starting of M/S receivers.
//           |                                         |<-[2]- dwt_setdelayedtrxtime for B ->|
//    PollRxTs                                  RespTxTs                                     |
//           |                                         |                                      FinalRxSFD
//___________________________________________________________________________________________i---------|---i___
//
//[1] is calculating in send_response()
//[2] is calculating in TX_IRQ callback
//------------------------------------------------------------------------------

    //[2] startup receivers.
    uint32_t    tmp;
    uint16_t    timeout;
    uint64_t    anchorRespTxTime;

    TS2U64_MEMCPY(anchorRespTxTime, pTwrInfo->nodeRespTx_ts);

    tmp  = 3500;//  1800-Prima verisone 3500-seconda versione   //configured time when Tag will transmit Final

    tmp -=  1000;//682-Prima verisone 1000-seconda versione       //configured time when Node was transmitting Response (now)
    tmp -= pTwrInfo->msg_time .poll     .phrAndData_us; //length of data part of the poll
    tmp -= pTwrInfo->msg_time .response .preamble_us;   //length of response's preamble
    tmp -= pTwrInfo->msg_time .final    .preamble_us;   //length of final's preamble

    tmp = (uint32_t) (util_us_to_dev_time(tmp) >>8);

    tmp += (uint32_t)(anchorRespTxTime >>8);
    tmp &= 0xFFFFFFFE;                                              //This is the time when to enable the receiver

    timeout = pTwrInfo->msg_time .final .sy + RX_RELAX_TIMEOUT_SY;  //timeout for reception of Final msg

    dwt_setdelayedtrxtime(tmp);         //set delayed RX for Master : waiting the Final
    dwt_setrxtimeout(timeout);
    dwt_rxenable(DWT_START_RX_DELAYED); //start delayed Rx : if late, then the RX will be enabled immediately.
    start_tempvbat_sar();               //start sampling of a temperature on the Master chip: on reception of Final read the value.
}
/* @brief    ISR layer
 *             Real-time TWR application Tx callback
 *            to be called from dwt_isr()
 * */
static void
twr_tx_tag_cb(const dwt_cb_data_t *txd)
{

    twr_info_t *    pTwrInfo = getTwrInfoPtr();

    if(!pTwrInfo)
    {
        return;
    }

    uint32_t tmp = (uint32_t)((hrtc.Instance->SSR) & RTC_SSR_SS);    // MCU RTC time : this will be HW timestamped

    // Store the Tx Time Stamp of the transmitted packet
    switch(pTwrInfo->txState)
    {
    case Twr_Tx_Blink_Sent :                    //tag
        pTwrInfo->blinkRtcTimeStamp = tmp;
        dwt_readtxtimestamp(pTwrInfo->blinkTx_ts);
        break;
    case Twr_Tx_Poll_Sent :                     //tag
        pTwrInfo->pollRtcTimeStamp = tmp;
        dwt_readtxtimestamp(pTwrInfo->pollTx_ts);
//        HAL_GPIO_TogglePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin );
        break;
    case Twr_Tx_Resp_Sent :                        //responder (tag)
        pTwrInfo->respRtcTimeStamp = tmp;
        dwt_readtxtimestamp(pTwrInfo->nodeRespTx_ts);
        resp_tx_cb_setup_receivers(pTwrInfo);    //tag additional algorithm for receivers

        HAL_GPIO_WritePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin, GPIO_PIN_SET);
//        HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin, GPIO_PIN_RESET);
        break;
    case Twr_Tx_Final_Sent :                    //tag
        pTwrInfo->finalRtcTimeStamp = tmp;
        dwt_readtxtimestamp(pTwrInfo->finalTx_ts);
        trim_tag_proc(pTwrInfo);                //Trim Tag's offset automatically WRT to the PDOA-Node
        app.DwCanSleep = 0;
//        HAL_GPIO_TogglePin(LLED_GREEN_GPIO_Port, LLED_GREEN_Pin );
        // SignalFinishedSecondPollCommunication();
        break;
    default :
        break;
    }
}

/* @brief     ISR layer
 *             TWR application Rx callback
 *             to be called from dwt_isr() as an Rx call-back
 * */
static void
twr_rx_cb(const dwt_cb_data_t *rxd)
{

    twr_info_t     *pTwrInfo = getTwrInfoPtr();

    if(!pTwrInfo)
    {
        return;
    }

    uint16_t head = pTwrInfo->rxPcktBuf.head;
    uint16_t tail = pTwrInfo->rxPcktBuf.tail;
    uint16_t size = sizeof(pTwrInfo->rxPcktBuf.buf) / sizeof(pTwrInfo->rxPcktBuf.buf[0]);

    if(CIRC_SPACE(head,tail,size) <= 0)
    {
        return;
    }

    rx_pckt_t *p = &pTwrInfo->rxPcktBuf.buf[head];

    p->rxRtcTimeStamp = (uint32_t)((hrtc.Instance->SSR) & RTC_SSR_SS);    // MCU RTC timestamp

    dwt_readrxtimestamp(p->rxTimeStamp);    //Raw Rx TimeStamp

    p->rxDataLen = MIN(rxd->datalength, sizeof(twr_msg_t));

    dwt_readrxdata((uint8_t *)&p->msg.stdMsg, p->rxDataLen, 0); //Raw message

#if (DIAG_READ_SUPPORT == 1)
    read_full_diagnostics(p, rxd->status);
#endif

    if(app.rxTask.Handle != NULL) // RTOS : rxTask can be not started
    {
        head = (head + 1) & (size-1);
        pTwrInfo->rxPcktBuf.head = head;    //IRQ : do not need to protect
        if(osSignalSet(app.rxTask.Handle,app.rxTask.Signal) != osOK)
        {

            error_handler(1, _Err_Signal_Bad);
        }
    }

    //SignalFinishedSecondPollCommunication();

}

/*
 * @brief    ISR layer
 *
 * */
static void
twr_rx_timeout_cb(const dwt_cb_data_t *rxd)
{
//	HAL_GPIO_WritePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin, GPIO_PIN_SET);
//	HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin, GPIO_PIN_SET);
//	HAL_GPIO_WritePin(LLED_GREEN_GPIO_Port, LLED_GREEN_Pin, GPIO_PIN_SET);

//    twr_info_t     *pTwrInfo = getTwrInfoPtr();

//    dwt_forcetrxoff() ;
	/* Clear reception timeout to start next ranging process. */
	dwt_setrxtimeout(0);
	/* Activate reception immediately. */
	dwt_rxenable(DWT_START_RX_IMMEDIATE);

//    if(!pTwrInfo)
//    {
//    	return;
//    }
//
//    if(pTwrInfo->txState == Twr_Tx_Poll_Sent)
//    {
//        pTwrInfo->faultyRangesCnt++;
//    }




//range aborted
}


static void
twr_rx_error_cb(const dwt_cb_data_t *rxd)
{
    twr_rx_timeout_cb(rxd);
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------

/* @brief     app layer
 *     RTOS independent application layer function.
 *     initialising of TWR from scratch.
 *    This MUST be executed in protected mode.
 *
 *     This will setup the process of:
 *     1. broadcast blink / wait for Ranging Config response;
 *     2. receive setup parameters from Ranging Config;
 *     3. if version of Ranging Config is not compatible, keep blinking;
 *     4. otherwise setup slot, new panID, framefiltering, address, twr timings;
 *     6. switch off blinking timer and switch on precise WUP timer;
 *     5. range to the Node addr from MAC of Ranging Config
 * */
error_e tag_process_init(void)
{
#ifdef TAG_STATIC_TWRINFO
    twr_info_t *pTwrInfo = &TwrInfo;
#else

    pTwrInfo = TAG_MALLOC(sizeof(twr_info_t));

    if(!pTwrInfo)
    {
        return(_ERR_Cannot_Alloc_Memory);
    }

#endif

    /* switch off receiver's rxTimeOut, RxAfterTxDelay, delayedRxTime,
     * autoRxEnable, dblBufferMode and autoACK,
     * clear all initial counters, etc.
     * */
    memset(pTwrInfo, 0 , sizeof(twr_info_t));

    /* Tag will receive its configuration, such as
     * panID, tagAddr, node0Addr and TWR delays:
     * pollTx2FinalTxDelay_us and response rx delay from Ranging Config message.
     *
     * But the reception timeouts calculated based on known length of
     * Ranging Config and Response packets.
     * */
      pTwrInfo->eui16     = GetTagId();

    { //pre-calculate all possible two-way ranging frame timings
        dwt_config_t *pCfg = &app.pConfig->dwt_config;    //dwt_config : holds node's UWB mode

        msg_t msg;

        msg.prf             = pCfg->prf;            //Deca define: e.g. DWT_PRF_64M
        msg.dataRate        = pCfg->dataRate;       //Deca define: e.g. DWT_BR_6M8
        msg.txPreambLength  = pCfg->txPreambLength; //Deca define: e.g. DWT_PLEN_128


        msg.msg_len = sizeof(rng_cfg_msg_t);
        calculate_msg_time(&msg, &pTwrInfo->msg_time.ranging_config);

        msg.msg_len = sizeof(poll_msg_t);
        calculate_msg_time(&msg, &pTwrInfo->msg_time.poll);

        // msg.msg_len = sizeof(resp_ext_msg_t);
        // calculate_msg_time(&msg, &pTwrInfo->msg_time.response);

        msg.msg_len = sizeof(resp_pdoa_msg_t);
        calculate_msg_time(&msg, &pTwrInfo->msg_time.response);

        msg.msg_len = sizeof(final_msg_accel_t);
        calculate_msg_time(&msg, &pTwrInfo->msg_time.final);
    }

    /* dwt_xx calls in app level Must be in protected mode (DW1000 IRQ disabled) */
//    disable_dw1000_irq();

//    set_dw_spi_slow_rate(DW_A);
    reset_DW1000(); /* Target specific drive of RSTn line into DW1000 low for a period. */
    TWR_ENTER_CRITICAL();

    port_stop_all_UWB();    /**< switch off all UWB and set callbacks to NULL */

    if (dwt_initialise(DWT_LOADUCODE) != DWT_SUCCESS)
    {
        return (_ERR_INIT);   // device initialise has failed
    }

    set_dw_spi_fast_rate(DW_A);

    /* Configure receiver's UWB mode, set power and antenna delays for TWR mode */
    rxtx_tag_configure_t p;
    p.pdwCfg      = &app.pConfig->dwt_config;
    p.frameFilter = DWT_FF_NOTYPE_EN;    //DWT_FF_DATA_EN
    p.txAntDelay  = app.pConfig->s.ant_tx_a;
    p.rxAntDelay  = app.pConfig->s.ant_rx_a;
    p.panId       = 0x5555;//PanID : does not matter : DWT_FF_NOTYPE_EN : will be reconfigured on reception of RI message
    p.shortadd    = 0xAAAA;//ShortAddr : does not matter : DWT_FF_NOTYPE_EN : will be reconfigured on reception of RI message
    rxtx_tag_configure(&p);

    // dwt_setleds(3) ;            /**< DEBUG I/O 2&3 : configure the GPIOs which control the LEDs on HW */
    // dwt_setlnapamode(1,1);        /**< DEBUG I/O 5&6 : configure TX/RX states to output on GPIOs */

    dwt_setcallbacks(twr_tx_tag_cb, twr_rx_cb, twr_rx_timeout_cb, twr_rx_error_cb);

//    dwt_configuresleep(DWT_PRESRV_SLEEP | DWT_CONFIG, DWT_WAKE_CS | DWT_SLP_EN);

    dwt_setinterrupt( DWT_INT_TFRS | DWT_INT_RFCG |
                     (DWT_INT_ARFE | DWT_INT_RFSL | DWT_INT_SFDT |
                      DWT_INT_RPHE | DWT_INT_RFCE | DWT_INT_RFTO /*| DWT_INT_RXPTO*/), 1);


    init_dw1000_irq();            /**< manually init EXTI DW1000 lines IRQs */

    /* configure non-zero initial values */
    pTwrInfo->env.responseRxTo_sy  = 2 * pTwrInfo->msg_time.response.sy;    //to be able receive long rng_cfg_upd_msg_t relax Response RX timeout time
    pTwrInfo->seqNum    = (uint8_t)(0xff*rand()/RAND_MAX);
    pTwrInfo->eui64     = ((uint64_t)dwt_getlotid()<<32) + dwt_getpartid(); //valid after dwt_initialise()
    pTwrInfo->xtaltrim = dwt_getxtaltrim();                             //valid after dwt_initialise()


    TWR_EXIT_CRITICAL();


    return (_NO_ERR);
}


/*
 * */
void tag_process_start(void)
{
    enable_dw1000_irq();         /**< enable DW1000 IRQ to start  */
}


/* @brief   app level
 *          RTOS-independent application level function.
 *          deinitialize the pTwrInfo structure.
 *  This must be executed in protected mode.
 *
 * */
void tag_process_terminate(void)
{
    TWR_ENTER_CRITICAL();

    twr_info_t     *pTwrInfo = getTwrInfoPtr();

    if (pTwrInfo)
    {
#ifndef TAG_STATIC_TWRINFO
        TAG_FREE(pTwrInfo);
#endif
    }

    TWR_EXIT_CRITICAL();
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------

/* @brief
 *          TWR : DISCOVERY PHASE
 *          Tag sends blinks, searching for a Ranging Config message
 *
 *          application layer function
 */
// error_e    initiator_send_blink(twr_info_t *p)
// {
//     error_e       ret;
//     tx_pckt_t     txPckt;

//     memset(&txPckt, 0, sizeof(txPckt));

//     blink_msg_t *pTxMsg = &txPckt.msg.blinkMsg;

//     // TWR : PHASE : Initiator Sends Blink to Responder
//     //txPckt.psduLen              = sizeof(blink_msg_t);
//     txPckt.delayedTxTimeH_sy    = 0;
//     txPckt.txFlag               = ( DWT_START_TX_IMMEDIATE | DWT_RESPONSE_EXPECTED );
//     txPckt.delayedRxTime_sy     = (uint32_t)util_us_to_sy(app.pConfig->s.rcDelay_us);  //activate receiver this time SY after Blink Tx
//     txPckt.delayedRxTimeout_sy  = (uint32_t)util_us_to_sy(app.pConfig->s.rcRxTo_us);     //receiver will be active this time, SY

//     pTxMsg->frameCtrl[0]        = Head_Msg_BLINK;
//     pTxMsg->seqNum              = p->seqNum;
//     memcpy(&pTxMsg->tagID, &p->eui64, sizeof(pTxMsg->tagID));

//     p->seqNum++;
//     p->txState                  = Twr_Tx_Blink_Sent;

//     TWR_ENTER_CRITICAL();

//     ret = tx_start(&txPckt);

//     TWR_EXIT_CRITICAL();

//     if( ret != _NO_ERR)
//     {
//         p->lateTX++;
//     }

//     return (ret);
// }


/* @brief
 *          TWR: RANGING PHASE
 *          Initiator sends Poll to the Responder
 *
 *          application layer function
 */
error_e    initiator_send_poll(twr_info_t *p)
{
    error_e      ret;
    tx_pckt_t    txPckt;

    poll_msg_t   *pTxMsg  = &txPckt.msg.pollMsg;

    /* Construct TxPckt packet: Send Poll immediate and configure delayed wait for the Response */
    txPckt.psduLen              = sizeof(poll_msg_t);
    txPckt.delayedTxTimeH_sy    = 0;
    txPckt.txFlag               = ( DWT_START_TX_IMMEDIATE | DWT_RESPONSE_EXPECTED );

    txPckt.delayedRxTime_sy     = p->env.delayRx_sy;        //environment, received from Ranging Init
    txPckt.delayedRxTimeout_sy  = p->env.responseRxTo_sy;   //this is calculated locally based on UWB configuration

    /* Construct TX Final UWB message */

    /* See IEEE frame header description */
    pTxMsg->mac.frameCtrl[0]    = Head_Msg_STD;
    pTxMsg->mac.frameCtrl[1]    = Frame_Ctrl_SS;
    pTxMsg->mac.panID[0]        = p->env.panID & 0xff;
    pTxMsg->mac.panID[1]        = (p->env.panID >> 8) & 0xff;
    pTxMsg->mac.destAddr[0]     = p->env.nodeAddr[0];
    pTxMsg->mac.destAddr[1]     = p->env.nodeAddr[1];
    pTxMsg->mac.sourceAddr[0]   = p->env.tagAddr[0];
    pTxMsg->mac.sourceAddr[1]   = p->env.tagAddr[1];
    pTxMsg->mac.seqNum          = p->seqNum;

    /* Data */
    pTxMsg->poll.fCode          = Twr_Fcode_Tag_Poll;
    // pTxMsg->poll.rNum           = p->rangeNum;
    pTxMsg->poll.rNum           = GetSuperFrameNumber();

    /* Transmit over the air */
    p->txState                  = Twr_Tx_Poll_Sent;
    p->seqNum++;
    // p->rangeNum++;

    POLL_ENTER_CRITICAL();

    ret = tx_start(&txPckt);

    POLL_EXIT_CRITICAL();

    if( ret != _NO_ERR)
    {
        p->lateTX++;
    }

    return (ret);
}

/* @brief     Part of twr_initiator_algorithm_tx.
 *
 * Initiator wakes DW1000 chip to send Blink or Poll message.
 *
 * */
void tag_wakeup_dw1000_blink_poll(twr_info_t *p)
{
    set_dw_spi_fast_rate(DW_A);

    p->stationary = p->stationary_imu;
    app.DwCanSleep = 0;

    if(port_wakeup_dw1000_fast() != _NO_ERR)
    {
        error_handler(0,  _ERR_DEVID);
    }

    /* Below needs to be restored on the wakeup */
//    dwt_setleds(3) ;            /**< DEBUG I/O 2&3 : configure the GPIOs which control the LEDs on HW */
//    dwt_setlnapamode(1,1);      /**< DEBUG I/O 5&6 : configure TX/RX states to output on GPIOs */
    dwt_setrxantennadelay(app.pConfig->s.ant_rx_a);
    dwt_settxantennadelay(app.pConfig->s.ant_tx_a); //Tx antenna delay is not preserved during sleep
}

/* @brief     Part of twr_initiator_algorithm_tx.
 *
 * Initiator sends POLL message.
 *
 * */
void initiator_wake_and_send_poll(twr_info_t *p)
{

//    POLL_ENTER_CRITICAL();
//    tag_wakeup_dw1000_blink_poll(p);
//    POLL_EXIT_CRITICAL();

    initiator_send_poll(p);
}
/**
 * @brief   This function constructs the data part of Response Msg.
 *          It is called after node receives a Poll from a tag.
 *          Mac header for the message should be created separately.
 */
static void
prepare_response_msg(resp_tag_t         *pResp,
                     tag_addr_slot_t    *tag,
                     twr_info_t         *p,
                     uint32             uTimeStamp)
{
    int32_t    tmp;


    //driver exclusion
    //if the tag is excluded and it is the received tag then send exclusion message
    if((getTagStatus() == status_driver_exclusion_mode) &&
    		(getReceivedTagAddress() == getPedestrianTagAddress()))
    {
    	pResp->fCode = Driver_exclusion_fcode;
    }
    else
    {
    	pResp->fCode = Twr_Fcode_Resp_Ext;
    }


//     uint8_t slotArrayIndex = (uint8_t)(tag->slot-1);
    // {//slot period correction for the Tag
    //     int32_t tagSleepCorrection_us = calc_slot_correction_middle_slot_algorithm(p, slotArrayIndex, uTimeStamp);
    //   //  printf("tagSleepCorrection_us %d slotArrayIndex %d uTimeStamp % d\r\n", tagSleepCorrection_us, slotArrayIndex, uTimeStamp);
    //     U32TOAR_MEMCPY(pResp->slotCorr_us , tagSleepCorrection_us);
    // }
    pResp->rNum = p->rangeNum;//p->result[slotArrayIndex].rangeNum;

    /* Send back to the tag the previous X, Y & clockOffset */
    // acap: we send the distance instead of x >> tmp = (int32_t)(p->result[tag->slot].x_cm);
//    tmp = (int32_t)(p->result[slotArrayIndex].dist_cm);
    pResp->x_cm[0] = 0; //(uint8_t)(tmp &0xFF);
    pResp->x_cm[1] = 0; //(uint8_t)(tmp>>8 &0xFF);

    // tmp = (int32_t)(p->result[slotArrayIndex].y_cm);
    pResp->y_cm[0] = 0; //(uint8_t)(tmp &0xFF);
    pResp->y_cm[1] = 0; //(uint8_t)(tmp>>8 &0xFF);

    //tmp = (int32_t)(p->result[slotArrayIndex].clockOffset_pphm);
    pResp->clkOffset_pphm[0] = 0; //(uint8_t)(tmp &0xFF);
    pResp->clkOffset_pphm[1] = 0; //(uint8_t)(tmp>>8 &0xFF);
}
/* @brief   APP level
 *          part of Real-time TWR algorithm implementation (Responder)
 *
 *          if called from ISR level, then remove
 *          TWR_ENTER_CRITICAL() and TWR_EXIT_CRITICAL() around tx_start()
 *
 *          Note:
 *          Shall be called with guarantee that the DWT_IRQ will not happen
 *
 * @return  _NO_ERR for no errors / error_e otherwise
 * */
error_e node_send_response(rx_pckt_t *pRxPckt, twr_info_t *p) //static error_e node_send_response(rx_pckt_t *pRxPckt, twr_info_t *p)
{
    error_e     ret;
    uint32_t    tmp;
    uint64_t    tmp64;

    p->env.nodeAddr[0]     = pRxPckt->msg.pollMsg.mac.sourceAddr[0];
    p->env.nodeAddr[1]     = pRxPckt->msg.pollMsg.mac.sourceAddr[1];
    /* Construct the response tx packet to the tag */
    tx_pckt_t       TxPckt;
    resp_pdoa_msg_t  *pTxMsg = &TxPckt.msg.respMsg;

    pTxMsg->mac.frameCtrl[0]    = Head_Msg_STD;
    pTxMsg->mac.frameCtrl[1]    = Frame_Ctrl_SS;
    pTxMsg->mac.panID[0]        = p->env.panID & 0xff;
    pTxMsg->mac.panID[1]        = (p->env.panID >> 8) & 0xff;
    //pTxMsg->mac.destAddr[0]     = pRxPckt->tag->addrShort[0];
    //pTxMsg->mac.destAddr[1]     = pRxPckt->tag->addrShort[1];
    pTxMsg->mac.destAddr[0]     = pRxPckt->msg.pollMsg.mac.sourceAddr[0];
    pTxMsg->mac.destAddr[1]     = pRxPckt->msg.pollMsg.mac.sourceAddr[1];

    // printf("%d \r\n", pTxMsg->mac.destAddr[0]);
    // printf("%d \r\n", pTxMsg->mac.destAddr[1]);
    pTxMsg->mac.sourceAddr[0]   = p->euiShort[0];
    pTxMsg->mac.sourceAddr[1]   = p->euiShort[1];
    // printf("%d \r\n", pTxMsg->mac.sourceAddr[0]);
    // printf("%d \r\n", pTxMsg->mac.sourceAddr[1]);
    pTxMsg->mac.seqNum          = p->seqNum;

    prepare_response_msg(&pTxMsg->resp, pRxPckt->tag, p, pRxPckt->rxRtcTimeStamp);

//------------------------------------------------------------------------------
//
// Description of the ranging protocol.
//
//           |< ------------------------------- tag_pollTxFinalTx_us --------------------------------->|
//TAGTX      |                                                                                         |
//   PollTxSFD   PollTxEnd                                                                   FinalTxSFD    FinalTxEnd
//_i---------|---i___________________________________________________________________________i---------|---i___
//           |
//           |   |<--- tag_replyDly_us --->|                                                 |
//TAGRX         |                   RespRxStart                                                 |
//_______________________________________..i---------------i___________________________________________________
//
//           |                             |                                                 |
//NODERX     |                             |                                                 |
//CHIP MASTER|                             |           |<-(2)- dwt_setdelayedtrxtime for M ->|
//           |                             |           |                                     |
//    PollRxTs                             |           |                                      FinalRxSFD
//-----------|---i___________________________________________________________________________i---------|---i___
//           |
//           |<--[1]-- delayedTxTimeH_sy (tmp64) ----->|
//           |                                         |
//NODETX                                        respTxTs
//_________________________________________i-----------|---i___________________________________________________
//
//           |                                         |
//           |                                         |
//CHIP SLAVE : DEVTIME TIME is synchronized and close to MASTER's dev_time. use Master's dev_time to activate SLAVE's receiver
//the differences between M&S dev_time is << delayed_RX time, so we should never experience fail in starting of M/S receivers.
//           |                                         |<-[2]- dwt_setdelayedtrxtime for S ->|
//    PollRxTs                                  RespTxTs                                     |
//           |                                         |
//___________________________________________________________________________________________i---------|---i___
//
//[1] is calculating in node_send_response()
//[2] is calculating in TX_IRQ callback: twr_tx_node_cb()
//------------------------------------------------------------------------------

    /* [1] configure TX devtime when R-marker of Response will be transmitted */

    // DW1000 will adjust the transmission of SFD of the response packet exactly
    // to PollRx + tag_replyDly + length of PLEN of msg

    tmp  = 1000;// 682-Prima verisone 975-seconda versione    //tag_replyDly_us is a SYSTEM configuration value of the delay
                                                  //after completion of a Tag PollTx, when the Tag will turn on its receiver
                                                    //and will wait for the Response from the Node.
    //printf("tmp %d \r\n",tmp);
    tmp += p->msg_time.poll.phrAndData_us;   //pre-calculated length of Poll packet
//    printf("1 %d \r\n",tmp);
    tmp += p->msg_time.response.preamble_us; //pre-calculated length of Response packet
//    printf("2 %d \r\n",tmp);

    /* Adjust the Response transmission time with respect to the last system TimeStamp */
    TS2U64_MEMCPY(tmp64, p->nodePollRx_ts);


    tmp64 += util_us_to_dev_time(tmp);
    tmp64 &= MASK_TXDTS;

    TxPckt.delayedTxTimeH_sy    = tmp64 >>8;    //at this time the Node will transmit the Response's TimeStamp
                                                //in the DWT_START_TX_DELAYED mode.

    /*Write all timestamps in the resp message*/

    TS2TS_UWB_MEMCPY( pTxMsg->resp.pollRx_ts ,     p->nodePollRx_ts);           // Embed Poll Rx time to the resp message

     U642TS_UWB_MEMCPY(pTxMsg->resp.responseTx_ts,   (tmp64 + app.pConfig->s.ant_tx_a));          // Embed Calculated resp TX time to the resp message

        TxPckt.psduLen              = sizeof(resp_pdoa_msg_t);
    TxPckt.txFlag               = ( DWT_START_TX_DELAYED ); //RX will be set in the dwt_isr() : twr_tx_node_cb()
    TxPckt.delayedRxTime_sy     = 0;
    TxPckt.delayedRxTimeout_sy  = 0;

    p->seqNum++;
    p->txState                  = Twr_Tx_Resp_Sent;         //indicate to TX_IRQ that the Response was sent

    TWR_ENTER_CRITICAL();

    ret = tx_start(&TxPckt);

    TWR_EXIT_CRITICAL();
    HAL_GPIO_WritePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin, GPIO_PIN_SET);
    //after a good tx_start(), DW_A and DW_B will be configured for reception in the TX_IRQ callback [2].
    //if the tx_start() failed transmit delayed, the DW_A receiver shall be re-enabled in the control application.

    return (ret);
}
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// The most real-time section of TWR algorithm

/* @brief    app layer
 *             Real-time TWR algorithm implementation (Initiator)
 *
 *             prefer to be called from application UWB Rx,
 *            but can be called from ISR, i.e. twr_rx_cb() directly.
 *             if called from ISR layer, then revise/remove
 *             TWR_ENTER_CRITICAL()
 *             TWR_EXIT_CRITICAL()
 *
 * @return     _NO_ERR for no errors
 * */
error_e twr_initiator_algorithm_rx(rx_pckt_t *pRxPckt, twr_info_t *pTwrInfo)
{
    error_e     ret   = _Err_Not_Twr_Frame;
    fcode_e     fcode = Twr_Fcode_Not_Defined;

    std_msg_t   *pMsg = &pRxPckt->msg.stdMsg;
    uint16_t destAddress ;
    if(pMsg->mac.frameCtrl[0] == Head_Msg_STD || pMsg->mac.frameCtrl[0] == Head_Msg_STD1)
    {
        /* Only SS and LS MAC headers supported in current application */
        switch (pMsg->mac.frameCtrl[1] & Frame_Ctrl_MASK)
        {
        case Frame_Ctrl_SS:
            if((pRxPckt->rxDataLen == sizeof(resp_ext_msg_t)) /*||    /* Response (with Coordinates extension) */
               /*(pRxPckt->rxDataLen == sizeof(rng_cfg_upd_msg_t))*/)   /* Ranging Config (update) */
            {
                fcode = ((std_msg_ss_t*)pMsg)->messageData[0] ;
                destAddress = AR2U16(((resp_ext_msg_t*)pMsg)->mac.destAddr);
                //printf("%d \r\n",fcode);
            }
            else if((pRxPckt->rxDataLen == sizeof(poll_msg_t)) ||        /* Poll */
               (pRxPckt->rxDataLen == sizeof(final_msg_accel_t)))   /* Final extended */
            {
                fcode = ((std_msg_ss_t*)pMsg)->messageData[0] ;
                //printf("%d \r\n",fcode);
            }
            break;
        case Frame_Ctrl_LS:
            if(pRxPckt->rxDataLen == sizeof(rng_cfg_msg_t))         /* Ranging Config (discovery) */
            {
                fcode = ((std_msg_ls_t*)pMsg)->messageData[0] ;
            }
            break;
        default:
            fcode = Twr_Fcode_Not_Defined ;
            break;
        }
    }

    uint16_t thisTagId = GetTagId();
    //printf("1 \r\n");
    switch (fcode)
    {
    	case Poll_driver_exclusion:
        case  Twr_Fcode_Tag_Poll :
        {

        	HAL_GPIO_WritePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin, GPIO_PIN_RESET);


        	uint8_t currentPollrNum = ((poll_msg_t*)pMsg)->mac.seqNum;

        	//GB
        	destAddress = AR2U16(((resp_ext_msg_t*)pMsg)->mac.destAddr);
        	if(destAddress == thisTagId)
        	{
        		pTwrInfo->pDestTag = pRxPckt->tag;          // TODO:in maniera provvisioria rispondo a tutti   /* current tag we are ranging to*/

				TS2TS_MEMCPY(pTwrInfo->nodePollRx_ts, pRxPckt->rxTimeStamp);   /* node's received time of poll message */

				pTwrInfo->pollRtcTimeStamp = pRxPckt->rxRtcTimeStamp;
				pTwrInfo->rangeNum = (uint16_t)currentPollrNum;

				ret = node_send_response(pRxPckt, pTwrInfo);

				if(ret == _NO_ERR)
				{
		//   		HAL_GPIO_WritePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin, GPIO_PIN_SET);
		//          ret = _No_Err_Response;

				}
				else
				{
		//            printf("N \r\n");
		//        	  HAL_GPIO_WritePin(LLED_BLUE_GPIO_Port, LLED_BLUE_Pin, GPIO_PIN_SET);
					  memset(pTwrInfo->nodePollRx_ts, 0, sizeof(pTwrInfo->nodePollRx_ts));    /* received time of poll message */
					  memset(pTwrInfo->nodeRespTx_ts, 0, sizeof(pTwrInfo->nodeRespTx_ts));    /* node's response tx time */

				}
        	}
			else
			{
//				ret = _Err_PollOfOwnVehicleTag;
//				ret = _Err_CannotRespondToIdentifiedPoll;
				ret =_Err_Unknown_Tag;
			}
        	//GB/
          break;
        }
        case Twr_Fcode_Rng_Config :
            /* Initiator received Range Init message from a Responder in discovery process.
             * 1. setup Timing parameters in accordance to Range Init message;
             * 2. configure MCU RTC WKUP timer to expire on next slot time
             * */
            ret = initiator_received_ranging_config(pRxPckt, pTwrInfo);

            if(ret == _NO_ERR)
            {
                ret = _No_Err_Ranging_Config;
            }
            SetCurrentTargetResponseStatus(0,0,UNKNOWN_PACKET);
            break;

    	case Driver_exclusion_fcode:
        case Twr_Fcode_Resp_Ext:
            /* Initiator received a Response message.
             * If the message from our PDOA node:
             * 1. Send delayed Final_TX;
             * 2. Adjust the Wakeup timer for next Poll.
             * */
            //TODO: fix the address

            if(destAddress == thisTagId)
            {

                // update destination node for the final messagge
                pTwrInfo->env.nodeAddr[0] =  ((resp_ext_msg_t*)pMsg)->mac.sourceAddr[0];
                pTwrInfo->env.nodeAddr[1] = ((resp_ext_msg_t*)pMsg)->mac.sourceAddr[1];
                //printf("response rnum : %d\r\n",((resp_ext_msg_t*)pMsg)->resp.rNum);
                uint16_t dist_cm = (uint16_t)AR2U16(((resp_ext_msg_t*)pMsg)->resp.x_cm);
                //SetCurrentTargetResponseStatus(dist_cm, AR2U16(pTwrInfo->env.nodeAddr),RECEIVED_A_RESPONSE_FOR_THIS_TAG);
                //(1)
                // update destination node for the final messagge
                // pTwrInfo->env.nodeAddr[0] =  ((resp_ext_msg_t*)pMsg)->mac.sourceAddr[0];
                // pTwrInfo->env.nodeAddr[1] = ((resp_ext_msg_t*)pMsg)->mac.sourceAddr[1];

                ret = initiator_received_response(pRxPckt, pTwrInfo, ((resp_ext_msg_t*)pMsg)->resp.rNum);
                //pTxMsg->mac.destAddr[0]     = p->env.nodeAddr[0];
               // pTxMsg->mac.destAddr[1]     = p->env.nodeAddr[1];
                if(ret == _NO_ERR)
                {
                    ret = _No_Err_Response;
                }

                //(2)
//                TWR_ENTER_CRITICAL();
//
//
//                int32_t         slotCorr_ns;
//                uint32_t        nextWakeUpPeriod_ns, rtcNow, tmpNow;
//
//                // casting received bytes to the int,
//                // this is a signed correction wrt (gRtcSFrameZeroCnt + expected_slot) coming from the Node
//                pTwrInfo->env.slotCorr_ns = 1000 * (int32_t)AR2U32(((resp_ext_msg_t*)pMsg)->resp.slotCorr_us);
//
//                slotCorr_ns = pTwrInfo->env.slotCorr_ns;
//                //SetCurreSlotCorrection(slotCorr_ns);
//                // printf("slotCorr_ns %d\r\n", slotCorr_ns/1000);
//
//                //(A)start of calculation
//                rtcNow = (uint32_t)((hrtc.Instance->SSR) & RTC_SSR_SS);
//
//                tmpNow = (pTwrInfo->gRtcSFrameZeroCnt > rtcNow)?
//                         (pTwrInfo->gRtcSFrameZeroCnt - rtcNow):
//                         (pTwrInfo->gRtcSFrameZeroCnt - rtcNow + 32768);
//
//                //Next RTC_Wakeup of Tag will be aligned to exact slot expected by the Node's:
//                // nextWakeUpPeriod_ns  = pTwrInfo->env.sframePeriod_ns
//                //                     -(WKUP_RESOLUTION_NS * tmpNow)
//                //                     -(slotCorr_ns);
//                // if(nextWakeUpPeriod_ns < (pTwrInfo->env.sframePeriod_ns >>1))
//                // {
//                //     nextWakeUpPeriod_ns += (pTwrInfo->env.sframePeriod_ns);
//                // }
//                //acap: we need send the slot correction to 2ta
//                //twr_configure_rtc_wakeup_ns(nextWakeUpPeriod_ns);
//
//                TWR_EXIT_CRITICAL();
            }
            else {
              //etCurrentTargetResponseStatus(0,0,RESPONSE_ADDRESSED_TO_ANOTHER_TAG_ID);
            }
            break;

          case Twr_Fcode_Tag_Accel_Final :
          {

//              HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin, GPIO_PIN_SET);
//              HAL_GPIO_WritePin(LLED_GREEN_GPIO_Port, LLED_GREEN_Pin, GPIO_PIN_RESET);
              TS2TS_MEMCPY(pRxPckt->nodePollRx_ts, pTwrInfo->nodePollRx_ts);
              TS2TS_MEMCPY(pRxPckt->nodeRespTx_ts, pTwrInfo->nodeRespTx_ts);
              ret = _No_Err_Final;

            /* Responder (Node) received the FINAL from Tag,
             * 1. if Tag is that one, currently the node is ranging to :
             *       read registers, necessary for ranging & phase difference and report this to upper application
             *       to calculate the result
             * 2. otherwise ignore
             * */
             //printf("final \r\n");
            // uint16_t        addr16;
            // ret = _Err_Unknown_Tag;

            // addr16 = AR2U16( ((final_msg_accel_t*)pMsg)->mac.sourceAddr );

            // uint16_t destAddress = AR2U16(((final_msg_accel_t*)pMsg)->mac.destAddr);
            // //printf("final. source: %d, destAddress:%d\r\n",addr16,destAddress);
            // // if this packet is not addressed to this host return
            // if(!(destAddress == GetHostId())) {
            //     ret = _Err_known_Tag_msg_to_other_host;
            //     break;
            // }

            // pRxPckt->tag = get_tag16_from_knownTagList( addr16 );

            // if (pRxPckt->tag && (pRxPckt->tag == pTwrInfo->pDestTag))
            // {
            //     pRxPckt->tag->lastReceived_PollRNum_InFinal =  ((poll_msg_t*)pMsg)->poll.rNum;
            //     pRxPckt->tag->nodeSFnumber_of_lastReceivedFinal =  GetCurrentLocalSuperFrameCounter();
            //     node_received_final(pRxPckt, pTwrInfo);
            //     ret = _No_Err_Final;
            // }

            // register received packet diagnostic
            //ReadPowerDiagnostic(&(pRxPckt->receivedPowerDiagnostic));
            break;
          }

        default:
            // SetCurrentTargetResponseStatus(0,0,UNKNOWN_PACKET);

            /* Initiator received unknown data / reset initiator
             * */
            ret = _Err_Not_Twr_Frame;
            break;
    }

    return (ret);
}


/* @brief check on-the air configuration that it capable
 *           to work on the current hardware
 * */
error_e check_air_config_correct(uint8_t ver, uint16_t delayRx_sy, uint16_t pollTxToFinalTx_us)
{
    error_e ret = _NO_ERR;

    if (ver != RC_VERSION_PDOA)
    {//This Tag knows only one version of Ranging Config format: RC_VERSION_PDOA
        ret = _Err_RC_Version_Unknown;
    }
    else if ((delayRx_sy < MIN_RESPONSE_CAPABILITY_US) ||\
        (pollTxToFinalTx_us < MIN_POLL_TX_FINAL_TX_CAPABILITY_US))
    {//This Tag hardware is too slow and cannot range with parameters, supplied by the Node.
        ret = _Err_Non_Compatible_TWR_Parameters;
    }

    return ret;
}

error_e initialize_secondCommunication(twr_info_t *pTwrInfo, uint16_t targetID) {
  error_e ret = _NO_ERR;

    rng_cfg_t *pRxMsg;

      /* obtain Node address and PanID from the MAC of Ranging Config */
    // if(targetID>0) { // 0x50b9
    //   pTwrInfo->env.nodeAddr[1] = (targetID >> 8) & 0x00FF;
    //   pTwrInfo->env.nodeAddr[0] = (targetID & 0x00FF);
    // }
    // else { // anonymous
      pTwrInfo->env.nodeAddr[0] = 0;//pRxPckt->msg.rngCfgMsg.mac.sourceAddr[0];
      pTwrInfo->env.nodeAddr[1] = 0;//pRxPckt->msg.rngCfgMsg.mac.sourceAddr[1];
    // }
    ret = _NO_ERR;

  return (ret);
}

error_e initialize_pollBasedResgistration(twr_info_t *pTwrInfo) {
  error_e ret = _NO_ERR;

    rng_cfg_t *pRxMsg;

    /* obtain Node address and PanID from the MAC of Ranging Config */
    pTwrInfo->env.panID       = 57034;//AR2U16(pRxPckt->msg.rngCfgMsg.mac.panID);
    //pTwrInfo->env.nodeAddr[0] = pRxPckt->msg.rngCfgMsg.mac.sourceAddr[0];
    //pTwrInfo->env.nodeAddr[1] = pRxPckt->msg.rngCfgMsg.mac.sourceAddr[1];

      /* obtain Node address and PanID from the MAC of Ranging Config */
    pTwrInfo->env.nodeAddr[0] = 0;//pRxPckt->msg.rngCfgMsg.mac.sourceAddr[0];
    pTwrInfo->env.nodeAddr[1] = 0;//pRxPckt->msg.rngCfgMsg.mac.sourceAddr[1];


    // ret = check_air_config_correct(pRxMsg->version,
    //                                AR2U16(pRxMsg->delayRx_us),
    //                                AR2U16(pRxMsg->pollTxToFinalTx_us));
    ret = _NO_ERR;

    if( ret == _NO_ERR)
    {
        // if(app.blinkTmr.Handle)
        // {
        //     osTimerStop(app.blinkTmr.Handle);    //stop blink timer : RTOS timer
        // }

        TWR_ENTER_CRITICAL();

        HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);

        /* configure environment parameters from Ranging Config message */
        pTwrInfo->env.version    = 3;// pRxMsg->version;

        uint16_t tagId = GetTagId();
        pTwrInfo->env.tagAddr[0] = tagId & 0xFF; // 3;
        pTwrInfo->env.tagAddr[1] = (tagId>>8) & 0xFF; //0;
        // pTwrInfo->env.tagAddr[0] = 2;//pRxMsg->tagAddr[0];
        // pTwrInfo->env.tagAddr[1] = 2;//pRxMsg->tagAddr[1];

        pTwrInfo->env.sframePeriod_ns       = 100000000;//1000000 * (uint32_t)(AR2U16(pRxMsg->sframePeriod_ms));
        pTwrInfo->env.pollTx2FinalTxDelay32 = 223641600;//115015680-Prima verisone 223641600-seconda versione
        pTwrInfo->env.delayRx_sy            = 975;//682-Prima verisone 975-seconda versione
        pTwrInfo->env.pollMultFast          = 1;//AR2U16(pRxMsg->pollMultFast);
        pTwrInfo->env.pollMultSlow          = 100;//AR2U16(pRxMsg->pollMultSlow);
        pTwrInfo->env.mode                  = 0;//AR2U16(pRxMsg->mode);
        pTwrInfo->env.slotCorr_ns           = 0;//1000 * (int32_t)AR2U32(pRxMsg->slotCorr_us);              //next wakeup correction to fit the slot

        rxtx_tag_configure_t p;
        p.pdwCfg = &app.pConfig->dwt_config;
        p.frameFilter = DWT_FF_NOTYPE_EN;    //DWT_FF_DATA_EN
        p.txAntDelay  = app.pConfig->s.ant_tx_a;
        p.rxAntDelay  = app.pConfig->s.ant_rx_a;
        p.panId       = pTwrInfo->env.panID;
        p.shortadd    = AR2U16(pTwrInfo->env.tagAddr);

        /* Setup configured panID, FrameFiltering and antenna delays */
        rxtx_tag_configure(&p); // TODO sistemare drivers decawave


        memset(pTwrInfo->nodePollRx_ts, 0, sizeof(pTwrInfo->nodePollRx_ts));    /* received time of poll message */
        memset(pTwrInfo->nodeRespTx_ts, 0, sizeof(pTwrInfo->nodePollRx_ts));    /* anchor's response tx time */

        /* Setup New High resolution RTC Wakup Timer : */
        uint32_t    tmp;
        uint32_t    rtcNow = (uint32_t)((hrtc.Instance->SSR) & RTC_SSR_SS);

        tmp = pTwrInfo->env.sframePeriod_ns;
//                - (WKUP_RESOLUTION_NS * (0x7FFF& (pTwrInfo->blinkRtcTimeStamp - rtcNow)))
//                - 1000*pTwrInfo->msg_time.poll.us
//                - pTwrInfo->env.slotCorr_ns;

		twr_configure_rtc_wakeup_ns(tmp);

        /* configure the RTC Wakup timer with highest RTOS priority.
         * This timer is counting a Super Frame period and signals to poll thread.
         * */
		HAL_NVIC_SetPriority(RTC_WKUP_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY, 0);
        HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);

        TWR_EXIT_CRITICAL();

    }

    return (ret);
}
/* @brief     Part of twr_initiator_algorithm_rx
 *
 * Initiator in discovery phase received RANGING CONFIG message from a Responder.
 * 1. Check that Tag understand and can comply to requested Ranging Config parameters;
 * 2. Setup Range Times as defined in the Ranging Config message;
 * 3. Switch off the Blink Timer.
 * 4. Configure & Start the Poll Timer.
 *
 * @parm    *rx_packet, *control structure
 *
 * @return error code
 * */
error_e initiator_received_ranging_config(rx_pckt_t *pRxPckt, twr_info_t *pTwrInfo)
{
    error_e ret = _NO_ERR;

    rng_cfg_t *pRxMsg;

    if(pRxPckt->rxDataLen == sizeof(rng_cfg_msg_t))
    {
        /* obtain Node address and PanID from the MAC of Ranging Config */
        pTwrInfo->env.panID       = 57034;//AR2U16(pRxPckt->msg.rngCfgMsg.mac.panID);
        pTwrInfo->env.nodeAddr[0] = pRxPckt->msg.rngCfgMsg.mac.sourceAddr[0];
        pTwrInfo->env.nodeAddr[1] = pRxPckt->msg.rngCfgMsg.mac.sourceAddr[1];

        pRxMsg = &pRxPckt->msg.rngCfgMsg.rngCfg;
    }
    // else if (pRxPckt->rxDataLen == sizeof(rng_cfg_upd_msg_t))
    // {
    //     /* update */
    //     pRxMsg = &pRxPckt->msg.rngCfgUpdMsg.rngCfg;
    // }
    else
    {
        return ret;
    }

    ret = check_air_config_correct(pRxMsg->version,
                                   AR2U16(pRxMsg->delayRx_us),
                                   AR2U16(pRxMsg->pollTxToFinalTx_us));

    if( ret == _NO_ERR)
    {
        if(app.blinkTmr.Handle)
        {
            osTimerStop(app.blinkTmr.Handle);    //stop blink timer : RTOS timer
        }

        TWR_ENTER_CRITICAL();

        HAL_NVIC_DisableIRQ(RTC_WKUP_IRQn);

        /* configure environment parameters from Ranging Config message */
        pTwrInfo->env.version    = 3;// pRxMsg->version;

        pTwrInfo->env.tagAddr[0] = pRxMsg->tagAddr[0];
        pTwrInfo->env.tagAddr[1] = pRxMsg->tagAddr[1];

        pTwrInfo->env.sframePeriod_ns       = 100000000;//1000000 * (uint32_t)(AR2U16(pRxMsg->sframePeriod_ms));
        pTwrInfo->env.pollTx2FinalTxDelay32 = 115015680;// (uint32_t)util_us_to_dev_time(AR2U16(pRxMsg->pollTxToFinalTx_us));//Time from SFD of Poll to SFD of Final.
        pTwrInfo->env.delayRx_sy            = 682;//(uint32_t)util_us_to_sy(AR2U16(pRxMsg->delayRx_us));      //time after Poll Tx completed to activate the RX
        pTwrInfo->env.pollMultFast          = 1;//AR2U16(pRxMsg->pollMultFast);
        pTwrInfo->env.pollMultSlow          = 100;//AR2U16(pRxMsg->pollMultSlow);
        pTwrInfo->env.mode                  = 0;//AR2U16(pRxMsg->mode);
        pTwrInfo->env.slotCorr_ns           = 1000 * (int32_t)AR2U32(pRxMsg->slotCorr_us);              //next wakeup correction to fit the slot

        rxtx_tag_configure_t p;
        p.pdwCfg = &app.pConfig->dwt_config;
        p.frameFilter = DWT_FF_NOTYPE_EN;    //DWT_FF_DATA_EN
        p.txAntDelay  = app.pConfig->s.ant_tx_a;
        p.rxAntDelay  = app.pConfig->s.ant_rx_a;
        p.panId       = pTwrInfo->env.panID;
        p.shortadd    = AR2U16(pTwrInfo->env.tagAddr);

        /* Setup configured panID, FrameFiltering and antenna delays */
        rxtx_tag_configure(&p);

        /* Setup New High resolution RTC Wakup Timer : */
        uint32_t    tmp;
        uint32_t    rtcNow = (uint32_t)((hrtc.Instance->SSR) & RTC_SSR_SS);

        tmp = pTwrInfo->env.sframePeriod_ns
                - (WKUP_RESOLUTION_NS * (0x7FFF& (pTwrInfo->blinkRtcTimeStamp - rtcNow)))
                - 1000*pTwrInfo->msg_time.poll.us
                - pTwrInfo->env.slotCorr_ns;

        twr_configure_rtc_wakeup_ns(tmp);

        /* configure the RTC Wakup timer with highest RTOS priority.
         * This timer is counting a Super Frame period and signals to poll thread.
         * */
        HAL_NVIC_SetPriority(RTC_WKUP_IRQn, configLIBRARY_MAX_SYSCALL_INTERRUPT_PRIORITY, 0);
		HAL_NVIC_EnableIRQ(RTC_WKUP_IRQn);

        TWR_EXIT_CRITICAL();

    }

    return (ret);
}

/* @brief     Part of twr_initiator_algorithm_rx.
 *
 * Initiator received the RESPONSE message.
 * 1. setup and send delayed Final_TX according to control structure
 * 2. if a use case : setup the delayed receive for reception of REPORT TOF
 *
 * @parm    *rx_packet, *control structure
 *
 * @return delayed TxPool error code
 * */
error_e initiator_received_response(rx_pckt_t *prxPckt, twr_info_t *p, uint8_t receivedRnum)
{
    error_e     ret;
    uint64_t    calcFinalTx64 ;

    tx_pckt_t   TxPckt;                 /**< allocate space for Tx Packet */

    final_msg_accel_t      *pTxMsg      = &TxPckt.msg.finalMsg;

    /* Construct TxPckt packet: no reception after Final transmission */
    TxPckt.psduLen                = sizeof(final_msg_accel_t);
    TxPckt.txFlag                 = ( DWT_START_TX_DELAYED );
    TxPckt.delayedRxTime_sy       = 0;
    TxPckt.delayedRxTimeout_sy    = 0;

    /* Calculate timings for transmission of Final Packet */
    // Load Poll Tx time
    TS2U64_MEMCPY(calcFinalTx64, p->pollTx_ts);

    // Add delay from Ranging Config message: PollTx2FinalTx
    calcFinalTx64 = MASK_TXDTS & ((uint64_t)(calcFinalTx64 + p->env.pollTx2FinalTxDelay32));

    // DW1000 will adjust the transmission of Final TxPacket SFD exactly at PollTx+PollTx2FinalTx+AntTxDelay
    TxPckt.delayedTxTimeH_sy    = calcFinalTx64 >>8;

    // Calculate Time Final message will be sent and embed this number to the Final message data.
    // Sending time will be delayedReplyTime, snapped to ~125MHz or ~250MHz boundary by
    // zeroing its low 9 bits, and then having the TX antenna delay added.
    // Getting antenna delay from the config and add it to the Calculated TX Time
    calcFinalTx64 = MASK_40BIT & (uint64_t)(calcFinalTx64 + app.pConfig->s.ant_tx_a);

    /* Construct TX Final UWB message */

    /* See IEEE frame header description */
    pTxMsg->mac.frameCtrl[0]    = Head_Msg_STD;
    pTxMsg->mac.frameCtrl[1]    = Frame_Ctrl_SS;
    pTxMsg->mac.panID[0]        = p->env.panID & 0xff;
    pTxMsg->mac.panID[1]        = (p->env.panID >> 8) & 0xff;
    pTxMsg->mac.destAddr[0]     = p->env.nodeAddr[0];
    pTxMsg->mac.destAddr[1]     = p->env.nodeAddr[1];
    pTxMsg->mac.sourceAddr[0]   = p->env.tagAddr[0];
    pTxMsg->mac.sourceAddr[1]   = p->env.tagAddr[1];
    pTxMsg->mac.seqNum          = p->seqNum;

    /* Data */
    pTxMsg->final.fCode       = (uint8_t)Twr_Fcode_Tag_Accel_Final;
    pTxMsg->final.rNum        = receivedRnum;//(uint8_t)p->rangeNum;
    TS2TS_UWB_MEMCPY( pTxMsg->final.pollTx_ts,     p->pollTx_ts);           // Embed Poll Tx time to the Final message
    TS2TS_UWB_MEMCPY( pTxMsg->final.responseRx_ts, prxPckt->rxTimeStamp);   // Embed Response Rx time to the Final message
    U642TS_UWB_MEMCPY(pTxMsg->final.finalTx_ts,    calcFinalTx64);          // Embed Calculated Final TX time to the Final message
    pTxMsg->final.flag        = p->stationary;
    pTxMsg->final.acc_x[0]    = p->acc.acc_x[0];
    pTxMsg->final.acc_x[1]    = p->acc.acc_x[1];
    pTxMsg->final.acc_y[0]    = p->acc.acc_y[0];
    pTxMsg->final.acc_y[1]    = p->acc.acc_y[1];
    pTxMsg->final.acc_z[0]    = p->acc.acc_z[0];
    pTxMsg->final.acc_z[1]    = p->acc.acc_z[1];

    /* Transmit over the air */
    p->txState  = Twr_Tx_Final_Sent;    //indicate to TX ISR that the response has been sent
    p->seqNum++;

    TWR_ENTER_CRITICAL();

    ret = tx_start(&TxPckt);

    TWR_EXIT_CRITICAL();

    if( ret != _NO_ERR)
    {
        p->lateTX++;
    }

    return (ret);
}

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
/*
 * @brief    setup RTC Wakeup timer
 *             wait_ns is awaiting time in ns
 * */
void twr_configure_rtc_wakeup_ns(uint32_t wait_ns)
{
//	HAL_GPIO_TogglePin(LLED_RED_GPIO_Port, LLED_RED_Pin);

    wait_ns = (uint32_t)(wait_ns/ WKUP_RESOLUTION_NS);
    if (HAL_RTCEx_SetWakeUpTimer_IT(&hrtc, wait_ns, RTC_WAKEUPCLOCK_RTCCLK_DIV2) != HAL_OK)
    {
//    	HAL_GPIO_WritePin(LLED_RED_GPIO_Port, LLED_RED_Pin,GPIO_PIN_RESET);
    	error_handler(1,_Err_Configure_WKUP_Timer);

    }

}

/* @brief HAL RTC Wakeup timer callback
 *
 *         Wakeup Timer has started with RTOS highest priority.
 *         This timer sends signal to the Poll Thread to start Tx_Poll.
 * */
void HAL_RTCEx_WakeUpTimerEventCallback(RTC_HandleTypeDef *phrtc)
{

static int    count = 0;


#ifndef NODEFUNCTION

    twr_info_t     *pTwrInfo = getTwrInfoPtr();


    if(pTwrInfo)    //RTOS : pTwrInfo can be not allocated yet
    {
        pTwrInfo->gRtcSFrameZeroCnt = (uint32_t)((hrtc.Instance->SSR) & RTC_SSR_SS);

        if(count == 0)
        {
            twr_configure_rtc_wakeup_ns(pTwrInfo->env.sframePeriod_ns);
        }

        if(pTwrInfo->stationary && !pTwrInfo->stationary_imu )
        {
            pTwrInfo->stationary = false;
            count = pTwrInfo->env.pollMultFast;
        }

        count++;

        if( (pTwrInfo->stationary && (count >= pTwrInfo->env.pollMultSlow)) ||
            (!pTwrInfo->stationary && (count >= pTwrInfo->env.pollMultFast)) )
        {
            if(app.pollTask.Handle)    //RTOS : pollTask can be not started yet
            {

            	osMutexWait(app.pollTask.MutexId, osWaitForever);

            	if(osSignalSet(app.pollTask.Handle, app.pollTask.Signal) != osOK)
                {
                    error_handler(1, _Err_Signal_Bad);
                }
            }
            count = 0;
        }
    }
#endif


    			if(app.actionTask.Handle)    //RTOS : actionTask can be not started yet
                {

                	osMutexWait(app.actionTask.MutexId, osWaitForever);

                	if(osSignalSet(app.actionTask.Handle, app.actionTask.Signal) != osOK)
                    {
                        error_handler(1, _Err_Signal_Bad);
                    }
                }

}


//-----------------------------------------------------------------------------
