/*
 * @file    config.c
 * @brief   supports EEPROM and bss configuration sections:
 *
 *          defaultFConfig : section in EEPROM, where default parameters are saved and is not re-writable.
 *                 FConfig : section in EEPROM, where current parameters are saved and this is re-writable.
 *               bssConfig : section in RAM, which is representing FConfig - working set of RAM parameters.
 *
 *    application on startup shall init_bssConfig() : this will copy FConfig to bssConfig
 *    Accessing to variables is by pointer get_pbssConfig();
 *
 *    if application wants to re-write FConfig, use save_bssConfig(*newRamParametersBlock);
 *
 * NOTE: The code is very MCU dependent and this one is for STM32L151CC : it used DATA EEPROM
 *
 * Linker script shall define sections as follow:
 *
 * ".default_config" @0x08080000
 * ".fconfig"        @0x08080200
 *
 * @author Decawave Software
 *
 * @attention Copyright 2017 (c) DecaWave Ltd, Dublin, Ireland.
 *            All rights reserved.
 *
 */

#include <config.h>
#include <stdint.h>
#include <string.h>
#include <version.h>

#include <stm32l4xx.h>

#include "deca_device_api.h"
#include "deca_version.h"
#include "config.h"

//------------------------------------------------------------------------------

/* Section ".default_config" is defined in a linker file */
const param_block_t defaultFConfig __section(".default_config") = DEFAULT_CONFIG;


/* Section ".fconfig" is defined in a linker file */
const param_block_t FConfig __section(".fconfig") = DEFAULT_CONFIG;

/* run-time parameters block.
 *
 * This is the RAM image of the FConfig .
 *
 * Accessible from application by app.pConfig pointer after init_bssConfig()
 *
 * */
static param_block_t bssConfig __section(".bss") __aligned(0x100);


//------------------------------------------------------------------------------

/* MACRO for WRITE
 *
 * NOTE: The code is very MCU dependent: check MCU NVM and EEPROM organization
 * in curremnt
 * Linker script shall define pages
 * ".default_config" @0x08080000
 * ".fconfig"        @0x08080200
 *
 * */

#define FCONFIG_START_ADDR   ((uint32_t)0x08080200)
#define FCONFIG_END_ADDR     (FCONFIG_START_ADDR + FCONFIG_SIZE)

#if defined(STM32F3)
#define FCONFIG_PAGES_TO_BE_PROTECTED     (OB_WRP_PAGES2TO3)
#endif
//------------------------------------------------------------------------------
// Implementation

/*
 * @brief get pointer to run-time bss param_block_t block
 *
 * */
param_block_t *get_pbssConfig(void)
{
    return(&bssConfig);
}


/* @fn       load_bssConfig
 * @brief copy parameters from NVM to RAM structure.
 *           should be executed when usb connection initialized
 *
 *           assumes that memory model in the MCU of .text and .bss are the same
 * */
void load_bssConfig(void)
{
    memcpy(&bssConfig, &FConfig, sizeof(bssConfig));
}


/* @fn      restore_bssConfig
 * @brief   copy parameters from default NVM section to RAM structure.
 *
 *          assumes that memory model in the MCU of .text and .bss are the same
 * */
void restore_bssConfig(void)
{
    memcpy(&bssConfig, &defaultFConfig, sizeof(bssConfig));
}


/* @brief    save pNewRamParametersBlock to FConfig
 * @return  _NO_ERR for success and error_e code otherwise
 * */
//error_e save_bssConfig(param_block_t * pNewRamParametersBlock)
//{
//#if defined(STM32L4)
//    uint32_t                *pbuf;
//    volatile uint32_t       Address;
//    HAL_StatusTypeDef       status;
//
//    HAL_FLASHEx_DATAEEPROM_Unlock();         /**< Unlock the Flash to enable the flash control register access */
//
//    status = FLASH_WaitForLastOperation(FLASH_TIMEOUT_VALUE);
//
////    Address = FCONFIG_START_ADDR;
////    while (Address < FCONFIG_END_ADDR)
////    {
////        status = HAL_FLASHEx_DATAEEPROM_Erase(FLASH_TYPEERASEDATA_WORD, Address);
////        if(status !=HAL_OK)
////        {
////            error_handler(0, status);
////        }
////        Address += 4;
////    }
//
//    HAL_FLASHEx_DATAEEPROM_EnableFixedTimeProgram();
//
//    /* PROGRAM the FCONFIG */
//    pbuf = (uint32_t *) pNewRamParametersBlock;
//    Address = FCONFIG_START_ADDR;
//
//    while (Address < FCONFIG_END_ADDR)
//    {
//        status = HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, Address, *pbuf);
//
//        if (status != HAL_OK)
//        {
//          HAL_FLASHEx_DATAEEPROM_Lock();
//          return(_Err_Flash_Error);     /**< Error occurred while writing data in Flash memory */
//        }
//
//        Address += 4;
//        pbuf++;
//    }
//
//    /* Check the correctness of written data */
//    pbuf = (uint32_t *) pNewRamParametersBlock;
//    Address = FCONFIG_START_ADDR;
//
//    while (Address < FCONFIG_END_ADDR)
//    {
//        if((*(volatile uint32_t*) Address) != *pbuf)
//        {
//            HAL_FLASHEx_DATAEEPROM_Lock();
//            return(_Err_Flash_Verify);  /**< Error occurred while verifying */
//        }
//        Address += 4;
//        pbuf++;
//    }
//
//    HAL_FLASHEx_DATAEEPROM_Lock();   /**< Lock the Flash to disable the flash control register access */
//
//#endif
//
//    return (_NO_ERR);
//}

